package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.LeaveDetailResponse;
import com.hashworks.lms.dto.response.UserLeaveDetailsResponse;
import com.hashworks.lms.model.*;
import com.hashworks.lms.repository.*;
import com.hashworks.lms.service.IUserLeavesAuditService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserLeavesAuditService implements IUserLeavesAuditService {


    @Autowired
    UserLeavesAuditRepository userLeavesAuditRepository;


    @Autowired
    PolicyDetailMasterRepository policyDetailMasterRepository;

    @Autowired
    LeaveApplicationRepository leaveApplicationRepository;


    @Override
    public Map <String, Object> getUserLeaveDetails(UserLeaveDetailsRequest userLeaveDetailsRequest) {
        try {

            UserLeaveDetailsResponse userLeaveDetailsResponse = new UserLeaveDetailsResponse();
            userLeaveDetailsResponse.setUserId( userLeaveDetailsRequest.getUserId() );

            List<PolicyDetailMaster> policyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterId( userLeaveDetailsRequest.getPolicyMasterId() );
            List<LeaveDetailResponse> leaveDetailResponses = new ArrayList <LeaveDetailResponse>();

            for (PolicyDetailMaster policyDetailMaster:policyDetailMasters ) {

                LeaveDetailResponse leaveDetailResponse = new LeaveDetailResponse();
                List<Object[]> userLeaveDetails = userLeavesAuditRepository.getLeavesCreditedConsumedAvailable(userLeaveDetailsRequest.getUserId(),policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId());

                for(Object[] userLeaveDetail : userLeaveDetails){
                        leaveDetailResponse.setLeaveTypeId( policyDetailMaster.getLeaveType().getId() );
                        leaveDetailResponse.setLeaveType( policyDetailMaster.getLeaveType().getName() );
                        leaveDetailResponse.setCredited( (Double) userLeaveDetail[0] );
                        leaveDetailResponse.setConsumed( (Double) userLeaveDetail[1] );
                        leaveDetailResponse.setAvailable( (Double) userLeaveDetail[2] );

                        leaveDetailResponse.setApplied( (leaveApplicationRepository.getLeavesCountBasedOnStatus( userLeaveDetailsRequest.getUserId(), "pending", policyDetailMaster.getPolicyMaster().getId(), policyDetailMaster.getLeaveType().getId() )) );
                        leaveDetailResponse.setApproved( leaveApplicationRepository.getLeavesCountBasedOnStatus( userLeaveDetailsRequest.getUserId(), "approved", policyDetailMaster.getPolicyMaster().getId(), policyDetailMaster.getLeaveType().getId() ) );
                    }

                leaveDetailResponses.add( leaveDetailResponse );
            }
            userLeaveDetailsResponse.setLeaveDetailResponses( leaveDetailResponses );
            
            
//            select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = 1 and policy_master_id = 2 and leave_type_id = 1 ;


            return ResponseJsonUtilNew.getSuccessResponseMessage( "User leaves Details Fetched Successfully", userLeaveDetailsResponse );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    //Above and below apis are same.This needs to be sent as List.

//    @Override
//    public UserLeaveDetailsResponse getUserLeaveDetailsNew(UserLeaveDetailsRequest userLeaveDetailsRequest) {
//        try {
//
//            UserLeaveDetailsResponse userLeaveDetailsResponse = new UserLeaveDetailsResponse();
//            userLeaveDetailsResponse.setUserId( userLeaveDetailsRequest.getUserId() );
//
//            List<PolicyDetailMaster> policyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterId( userLeaveDetailsRequest.getPolicyMasterId() );
//            List<LeaveDetailResponse> leaveDetailResponses = new ArrayList <LeaveDetailResponse>();
//
//            for (PolicyDetailMaster policyDetailMaster:policyDetailMasters ) {
//
//                LeaveDetailResponse leaveDetailResponse = new LeaveDetailResponse();
//                List<Object[]> userLeaveDetails = userLeavesAuditRepository.getLeavesCreditedConsumedAvailable(userLeaveDetailsRequest.getUserId(),policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId());
//
//                for(Object[] userLeaveDetail : userLeaveDetails){
//                    leaveDetailResponse.setLeaveTypeId( policyDetailMaster.getLeaveType().getId() );
//                    leaveDetailResponse.setLeaveType( policyDetailMaster.getLeaveType().getName() );
//                    leaveDetailResponse.setCredited( (Double) userLeaveDetail[0] );
//                    leaveDetailResponse.setConsumed( (Double) userLeaveDetail[1] );
//                    leaveDetailResponse.setAvailable( (Double) userLeaveDetail[2] );
//
//                    leaveDetailResponse.setApplied((leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "pending",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId())) );
//                    leaveDetailResponse.setApproved( leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "approved",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId()) );
//                }
//
//                leaveDetailResponses.add( leaveDetailResponse );
//            }
//            userLeaveDetailsResponse.setLeaveDetailResponses( leaveDetailResponses );
//
//
////            select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = 1 and policy_master_id = 2 and leave_type_id = 1 ;
//
//            return userLeaveDetailsResponse;
////            return ResponseJsonUtilNew.getSuccessResponseMessage( "User leaves Details Fetched Successfully", userLeaveDetailsResponse );
//        } catch (Exception e) {
//            return null;
//        }
//    }



    @Override
    public UserLeaveDetailsResponse getUserLeaveDetailsNew(UserLeaveDetailsRequest userLeaveDetailsRequest) {
        try {

            UserLeaveDetailsResponse userLeaveDetailsResponse = new UserLeaveDetailsResponse();
            userLeaveDetailsResponse.setUserId( userLeaveDetailsRequest.getUserId() );

            List<PolicyDetailMaster> policyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterId( userLeaveDetailsRequest.getPolicyMasterId() );
            List<LeaveDetailResponse> leaveDetailResponses = new ArrayList <LeaveDetailResponse>();

            for (PolicyDetailMaster policyDetailMaster:policyDetailMasters ) {

                LeaveDetailResponse leaveDetailResponse = new LeaveDetailResponse();
                List<Object[]> userLeaveDetails = userLeavesAuditRepository.getLeavesCreditedConsumedAvailable(userLeaveDetailsRequest.getUserId(),policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId());

                for(Object[] userLeaveDetail : userLeaveDetails){

                    leaveDetailResponse.setLeaveTypeId( policyDetailMaster.getLeaveType().getId() );
                    leaveDetailResponse.setLeaveType( policyDetailMaster.getLeaveType().getName() );
                    leaveDetailResponse.setCredited( (userLeaveDetail[0]!= null) ? ( (Double) userLeaveDetail[0] ) : 0.0 );
                    leaveDetailResponse.setConsumed( (userLeaveDetail[1]!= null) ? ( (Double) userLeaveDetail[1] ) : 0.0 );
                    leaveDetailResponse.setAvailable( (userLeaveDetail[2]!= null) ? ( (Double) userLeaveDetail[2] ) : 0.0 );
                    Double pendingLeaveCount = (leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "pending",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId()));
                    Double approvedLeaveCount = ( leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "approved",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId()) );
                   leaveDetailResponse.setApplied( (pendingLeaveCount != null) ? pendingLeaveCount : 0.0 );
                   leaveDetailResponse.setApproved( (approvedLeaveCount != null) ? approvedLeaveCount : 0.0 );

                }

                leaveDetailResponses.add( leaveDetailResponse );
            }
            userLeaveDetailsResponse.setLeaveDetailResponses( leaveDetailResponses );


//            select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = 1 and policy_master_id = 2 and leave_type_id = 1 ;

            return userLeaveDetailsResponse;
//            return ResponseJsonUtilNew.getSuccessResponseMessage( "User leaves Details Fetched Successfully", userLeaveDetailsResponse );
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    public UserLeaveDetailsResponse getUserLeaveDetailsNew2(UserLeaveDetailsRequest userLeaveDetailsRequest,GetEmployeeDetailsRequest getEmployeeDetailsRequest) {
        try {

            UserLeaveDetailsResponse userLeaveDetailsResponse = new UserLeaveDetailsResponse();
            userLeaveDetailsResponse.setUserId( userLeaveDetailsRequest.getUserId() );

            List<PolicyDetailMaster> policyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterId( userLeaveDetailsRequest.getPolicyMasterId() );
            List<LeaveDetailResponse> leaveDetailResponses = new ArrayList <LeaveDetailResponse>();

            for (PolicyDetailMaster policyDetailMaster:policyDetailMasters ) {

                LeaveDetailResponse leaveDetailResponse = new LeaveDetailResponse();
                List<Object[]> userLeaveDetails = userLeavesAuditRepository.getLeavesCreditedConsumedAvailable2(userLeaveDetailsRequest.getUserId(),policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId(),getEmployeeDetailsRequest.getStartDate(),getEmployeeDetailsRequest.getEndDate());

                for(Object[] userLeaveDetail : userLeaveDetails){

                    leaveDetailResponse.setLeaveTypeId( policyDetailMaster.getLeaveType().getId() );
                    leaveDetailResponse.setLeaveType( policyDetailMaster.getLeaveType().getName() );
                    leaveDetailResponse.setCredited( (userLeaveDetail[0]!= null) ? ( (Double) userLeaveDetail[0] ) : 0.0 );
                    leaveDetailResponse.setConsumed( (userLeaveDetail[1]!= null) ? ( (Double) userLeaveDetail[1] ) : 0.0 );
                    leaveDetailResponse.setAvailable( (userLeaveDetail[2]!= null) ? ( (Double) userLeaveDetail[2] ) : 0.0 );
                    Double pendingLeaveCount = (leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "pending",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId()));
                    Double approvedLeaveCount = ( leaveApplicationRepository.getLeavesCountBasedOnStatus(userLeaveDetailsRequest.getUserId(), "approved",policyDetailMaster.getPolicyMaster().getId(),policyDetailMaster.getLeaveType().getId()) );
                    leaveDetailResponse.setApplied( (pendingLeaveCount != null) ? pendingLeaveCount : 0.0 );
                    leaveDetailResponse.setApproved( (approvedLeaveCount != null) ? approvedLeaveCount : 0.0 );

                }

                leaveDetailResponses.add( leaveDetailResponse );
            }
            userLeaveDetailsResponse.setLeaveDetailResponses( leaveDetailResponses );


//            select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = 1 and policy_master_id = 2 and leave_type_id = 1 ;

            return userLeaveDetailsResponse;
//            return ResponseJsonUtilNew.getSuccessResponseMessage( "User leaves Details Fetched Successfully", userLeaveDetailsResponse );
        } catch (Exception e) {
            return null;
        }
    }
}





    //working code - earlier version - end point

//
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    PolicyMasterRepository policyMasterRepository;
//
//    @Autowired
//    LeaveTypeRepository leaveTypeRepository;
//
//    @Autowired
//    LeaveApplicationRepository leaveApplicationRepository;
//
//    @Autowired
//    EmployeeReportingManagerRepository employeeReportingManagerRepository;
//
//    @Autowired
//    ReportingManagerRepository reportingManagerRepository;
//
//    @Autowired
//    NotificationService notificationService;
//
//    @Autowired
//    UserLeavesAuditRepository userLeavesAuditRepository;
//
////    @Autowired
////    UserLeaveBalanceRepository userLeaveBalanceRepository;
//
//    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
//    public  Map<String,Object> applyForLeave(LeaveApplicationSaveRequest leaveApplicationRequest)
//    {
//
//        try {
//            HashMap <String, Object> map = new HashMap <>();
//            LeaveApplication leaveApplication = new LeaveApplication();
//            User user = userRepository.findOne(leaveApplicationRequest.getUserId());
//            PolicyMaster policyMaster = policyMasterRepository.findOne(leaveApplicationRequest.getPolicyMasterId());
//            LeaveType leaveType = leaveTypeRepository.findOne(leaveApplicationRequest.getLeaveTypeId());
//            ReportingManager reportingManager = reportingManagerRepository.findOne(leaveApplicationRequest.getReportingManagerId());
//
//            if(user != null && policyMaster != null && leaveType != null && reportingManager != null){
//                leaveApplication.setUser(user);
//                leaveApplication.setPolicyMaster(policyMaster);
//                leaveApplication.setLeaveType(leaveType);
//                leaveApplication.setReportingManager(reportingManager);
//                leaveApplication.setStatus("pending");
//                leaveApplication.setCreatedDate(new Date());
//                leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
//                leaveApplication.setEmployeeCommentSubmission(leaveApplicationRequest.getEmployeeCommentSubmission());
//                leaveApplication.setLeaveStartDate(leaveApplicationRequest.getStartDate());
//                leaveApplication.setLeaveEndDate(leaveApplicationRequest.getEndDate());
//                leaveApplication.setAppliedNoOfDays(leaveApplicationRequest.getNoOfDays());
//                leaveApplication.setEmployee(userRepository.findByUserId(leaveApplicationRequest.getUserId()));
//
//                // Email Notification
//
////                EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
////
////                List<String> emailIds = new ArrayList<String>();
////                String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
////                emailIds.add(emailId1);
////                EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
////                if(employeeReportingManager2 != null)
////                {
////                    String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
////                    emailIds.add(emailId2);
////                }
////
////                notificationService.sendNotification(user.getUsername(),emailIds,"Leave Application","Leave Application Submitted Successfully");
//
//                leaveApplicationRepository.save(leaveApplication);
//                return ResponseJsonUtilNew.postSuccessResponseMessage( "Leave Application Submitted Successfully!!!" );
//            }else{
//                return ResponseJsonUtilNew.entityNotFoundMessage( "User/PolicyMaster/LeaveType/ReportingManager doesn't exist" );
//            }
//
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//
//
//    }
//
//    public Map<String,Object> viewLeavesStatus(LeaveStatusRequest leaveStatusRequest)
//    {
//        try {
//            List<LeaveApplication> leaves = leaveApplicationRepository.findByUserId(leaveStatusRequest.getId());
//            List<ViewLeaveStatusResponse> leaveResponse =new ArrayList<>();
//
//            for (Iterator<LeaveApplication> iter = leaves.iterator(); iter.hasNext(); ) {
//                LeaveApplication leaveApplication = iter.next();
//                ViewLeaveStatusResponse viewLeaveStatusResponse = new ViewLeaveStatusResponse();
//                viewLeaveStatusResponse.setUserId(leaveApplication.getUser().getId());
//                viewLeaveStatusResponse.setCreatedBy(leaveApplication.getCreatedBy());
//                viewLeaveStatusResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//                viewLeaveStatusResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//                viewLeaveStatusResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//                viewLeaveStatusResponse.setStartDate(leaveApplication.getLeaveStartDate());
//                viewLeaveStatusResponse.setEndDate(leaveApplication.getLeaveEndDate());
//                viewLeaveStatusResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//                viewLeaveStatusResponse.setStatus(leaveApplication.getStatus());
//                viewLeaveStatusResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//                viewLeaveStatusResponse.setLeaveApplicationId(leaveApplication.getId());
//                viewLeaveStatusResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//                leaveResponse.add(viewLeaveStatusResponse);
//            }
//
//            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//
//    }
//
//    public Map<String,Object> viewLeavesByManager(ManagerLeaveViewRequest managerLeaveViewRequest)
//    {
//        try{
//            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerId(managerLeaveViewRequest.getManagerId());
//
//            List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();
//
//            for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
//                LeaveApplication leaveApplication = iter.next();
//                ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
//                viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
//                viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
//                viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
//                viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//                viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//                viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//                viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
//                viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
//                viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//                viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
//                viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//                viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
//                viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//                leaveResponse.add(viewLeavesByManagerResponse);
//            }
//
//
//            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//    public  Map<String,Object> viewLeavesByManagerByDate(ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest)
//    {
//        try{
//            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerIdByDate(managerLeaveViewByDateRequest.getManagerId(),managerLeaveViewByDateRequest.getStartDate(),managerLeaveViewByDateRequest.getEndDate());
//
//            List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();
//
//            for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
//                LeaveApplication leaveApplication = iter.next();
//                ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
//                viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
//                viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
//                viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
//                viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//                viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//                viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//                viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
//                viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
//                viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//                viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
//                viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//                viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
//                viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//                leaveResponse.add(viewLeavesByManagerResponse);
//            }
//
//
//            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//
//    public Map<String,Object> approveLeaves(ApproveLeavesRequest approveLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveLeavesRequest.getLeaveApplicationId());
//            // if(leaveApplication.getReportingManager().getId() == approveLeavesRequest.getReportingManagerId())
//            //  {
//            leaveApplication.setStatus("approved");
//            leaveApplication.setReportingManagerCommentsApproval(approveLeavesRequest.getComment());
//            leaveApplicationRepository.save(leaveApplication);
//
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//            List<String> emailIds = new ArrayList<String>();
//            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"Leave Application Response",leaveApplication.getUser().getUsername()+" leave approved");
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave approved Successfully!!!");
//
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//    public Map<String,Object> rejectLeaves(RejectLeavesRequest rejectLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(rejectLeavesRequest.getLeaveApplicationId());
//
//            leaveApplication.setStatus("cancelled");
//            leaveApplication.setEmployeeCommentsRejectionRequest(rejectLeavesRequest.getComment());
//            leaveApplicationRepository.save(leaveApplication);
//
//
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//            List<String> emailIds = new ArrayList<String>();
//            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+" has cancelled the leave");
//
//
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage(" leave cancelled Successfully!!!");
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
////    public Map<String,Object> rejectionLeavesApproval(ApproveRejectionLeavesRequest approveRejectionLeavesRequest)
////    {
////        try{
////            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveRejectionLeavesRequest.getLeaveApplicatioId());
//////            if(leaveApplication.getReportingManager().getId() == approveRejectionLeavesRequest.getReportingManagerId())
//////            {
////            leaveApplication.setStatus("LeaveRejectioApproved");
////            leaveApplication.setReportingManagerCommentsRejectionApproval(approveRejectionLeavesRequest.getComment());
////            leaveApplicationRepository.save(leaveApplication);
////            return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejection approved Successfully!!!");
////
//////            }
//////            else
//////            {
//////                return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id");
//////            }
////        }
////        catch(Exception e)
////        {
////            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
////        }
////    }
//
//
//
//    public Map<String,Object> editAppliedLeave(EditAppliedLeaveRequest editAppliedLeaveRequest)
//    {
//
//
//        try {
//
//
//            if(editAppliedLeaveRequest.getStartDate().getTime()<new Date().getTime() && editAppliedLeaveRequest.getEndDate().getTime() <editAppliedLeaveRequest.getStartDate().getTime())
//            {
//                return ResponseJsonUtilNew.postSuccessResponseMessage("start date should be greater then present date");
//
//            }
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(editAppliedLeaveRequest.getLeaveApplicationId());
//            PolicyMaster policyMaster = policyMasterRepository.findOne(editAppliedLeaveRequest.getPolicyMasterId());
//
//            leaveApplication.setLeaveStartDate(editAppliedLeaveRequest.getStartDate());
//                leaveApplication.setLeaveEndDate(editAppliedLeaveRequest.getEndDate());
//                leaveApplication.setAppliedNoOfDays(editAppliedLeaveRequest.getNoOfDays());
//                leaveApplication.setEmployeeCommentSubmission(editAppliedLeaveRequest.getComment());
//                leaveApplication.setPolicyMaster(policyMaster);
//                leaveApplication.setModifiedBy(editAppliedLeaveRequest.getModifiedBy());
//                leaveApplication.setModifiedDate( new Date() );
//                leaveApplication.setUser( userRepository.findOne(editAppliedLeaveRequest.getUserId()) );
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//                List<String> emailIds = new ArrayList<String>();
//                String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId1);
//                EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//                if(employeeReportingManager2 != null)
//                {
//                    String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                    emailIds.add(emailId2);
//                }
//                notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+" edited the leave");
//
//                leaveApplicationRepository.save( leaveApplication);
//                            return ResponseJsonUtilNew.postSuccessResponseMessage("updated sucessfully");
//
//
//        }
//        catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//
//      }
//
//
//
//    public Map<String,Object> managerRejectLeaves(ManagerRejectLeavesRequest managerRejectLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(managerRejectLeavesRequest.getLeaveApplicationId());
//
//            leaveApplication.setStatus("rejected");
//            leaveApplication.setReportingManagerCommentsApproval(managerRejectLeavesRequest.getComment());
//            leaveApplicationRepository.save(leaveApplication);
//
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//            List<String> emailIds = new ArrayList<String>();
//            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+"'s leave rejected");
//
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejected!!!");
//
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//    //employee withdraws leaves
//
//    public Map<String,Object> withdrawLeaves(WithdrawLeavesRequest withdrawLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(withdrawLeavesRequest.getLeaveApplicationId());
//
//            leaveApplication.setWithdrawalStatus("YES");
//            leaveApplication.setWithdrawalDate( new Date(  ) );
//            leaveApplication.setWithdrawalComment(withdrawLeavesRequest.getWithdrawalComment());
//            leaveApplicationRepository.save(leaveApplication);
//
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//            List<String> emailIds = new ArrayList<String>();
//            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application withdrawn",leaveApplication.getUser().getUsername()+"'s leave application withdrawn");
//
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave withdrawn!!!");
//
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//
//
//
//    // below api's to be included in the scheduler
//
//
//    public Map<String,Object> statusChangetoConsumptionStarted()
//    {
//        try{
//            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByApprovedStatusAndCurrentDate("approved",new Date());
//
//            for (LeaveApplication leaveApplication:leaveApplications) {
//
//                leaveApplication.setStatus( "consumption started" );
//                leaveApplication.setWithdrawalStatus( "NO" );
//                leaveApplication.setModifiedDate( new Date(  ) );
//                leaveApplicationRepository.save( leaveApplication );
//
//            }
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave consumption started status updated!!!");
//
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//
//    public Map<String,Object> consumptionStartedLeaveApplicationsJob()
//    {
//        try{
//            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByConsumptionStartedStatusAndCurrentDate("consumption started","NO",new Date());
//
//            for (LeaveApplication leaveApplication:leaveApplications) {
//
//                UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
//                userLeavesAudit.setDebit( 1.0 );
//                userLeavesAudit.setCredit( 0.0 );
//                userLeavesAudit.setLeaveType( leaveApplication.getLeaveType());
//                userLeavesAudit.setPolicyMaster( leaveApplication.getPolicyMaster() );
//                userLeavesAudit.setUser( leaveApplication.getUser() );
//                userLeavesAudit.setCreatedDate( new Date(  ) );
//                userLeavesAuditRepository.save( userLeavesAudit );
//
//            }
//
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave trails included!!!");
//
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }

