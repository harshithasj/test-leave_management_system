//package com.hashworks.lms.serviceImpl;
//
//import com.hashworks.lms.model.Role;
//import com.hashworks.lms.model.User;
//import com.hashworks.lms.repository.UserRepository;
//import com.hashworks.lms.security.UserInfo;
//import com.hashworks.lms.service.IAuthenticationService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpSession;
//import java.util.ArrayList;
//import java.util.Collection;
//
//@Component("authenticationService")
//public class AuthenticationService implements UserDetailsService , IAuthenticationService {
//
//	private final Logger log = LoggerFactory.getLogger(AuthenticationService.class);
//
//    @Autowired
//    private UserRepository userRepository;
//
//    private boolean accountNonExpired = true;
//    private boolean credentialsNonExpired = true;
//    private boolean accountNonLocked = true;
//
//    @Override
//    public UserInfo loadUserByUsername(String email) throws UsernameNotFoundException {
//
//        User user = this.userRepository.findOneByUsername(email);
//
//        if(user == null){
//        	log.error("User not found for email: "+email);
//            throw new UsernameNotFoundException("User with email: "+email+" not found");
//        }
//
//        log.info("Found user with id: "+user.getId()+" ; having RoleType: "+user.getRole().getRoleType()+" ; authorised: " +user.isAuthorised());
//
//
//        UserInfo authenticatedUser=new com.hashworks.lms.security.UserInfo(user.getUsername(), user.getPassword() , user.isAuthorised(), accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthorities(user.getRole()));
//        log.info("authenticated user: "+authenticatedUser.toString());
//        return authenticatedUser;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities(Role role) {
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleType());
//        authorities.add(authority);
//        return authorities;
//    }
//
//    @Override
//    public UserInfo login(String email, String password, HttpSession httpSession) throws UsernameNotFoundException {
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(5);
//        UserInfo userInfo = this.loadUserByUsername(email);
//        if(encoder.matches(password, userInfo.getPassword())) {
//            userInfo.eraseCredentials();
//            return userInfo;
//        } else {
//            throw new UsernameNotFoundException("Invalid Credentials");
//        }
//    }
//}