package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.model.User;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    @Value("${spring.mail.username}")
    private String fromEmail;

//    public void sendNotification(String toEmail, List<String> managerEmail, String subject, String message,String managerName) throws MailException{
//    //    managerEmail.add("hw_leavetracking@hashworks.co");
//        Object[] mails = managerEmail.toArray();
//        SimpleMailMessage simpleMailMessage = new SimpleMailMessage(  );
//        simpleMailMessage.setTo(toEmail);
//        simpleMailMessage.setCc(Arrays.stream(mails).toArray(String[]::new));
//        simpleMailMessage.setFrom( fromEmail );
//        simpleMailMessage.setSubject( subject );
//        simpleMailMessage.setText("Hi "+ managerName+",\n" +message+"\n\n-with regards \nHR Team\nHashworks");
//        javaMailSender.send(simpleMailMessage);
//    }
//    public void sendNotificationToManager( String managerEmail, String subject, String message,String managerName) throws MailException{
//        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
//        simpleMailMessage.setTo(managerEmail);
//        //simpleMailMessage.setCc("kiranholi.g@hashworks.co");
//        simpleMailMessage.setFrom( fromEmail );
//        simpleMailMessage.setSubject( subject );
//        simpleMailMessage.setText("Hi "+ managerName+",\n" +message+"\n-with regards \nHR Team\nHashworks");
//        javaMailSender.send(simpleMailMessage);
//    }
//    public void sendNotificationToAdmin( Object[] adminEmail, String subject, String message,String adminName) throws MailException{
//        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
//        simpleMailMessage.setTo(Arrays.stream(adminEmail).toArray(String[]::new));
//        //simpleMailMessage.setCc("kiranholi.g@hashworks.co");
//        simpleMailMessage.setFrom( fromEmail );
//        simpleMailMessage.setSubject( subject );
//        simpleMailMessage.setText("Hi "+ adminName+",\n" +message+"\n-with regards \nHR Team\nHashworks");
//        javaMailSender.send(simpleMailMessage);
//    }
//
//    public void sendNotificationToEmployeeForPasswordChange( String employeeEmail, String subject, String message,String EmployeeName) throws MailException{
//        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
//        simpleMailMessage.setTo(employeeEmail);
//        //simpleMailMessage.setCc("kiranholi.g@hashworks.co");
//        simpleMailMessage.setFrom( fromEmail );
//        simpleMailMessage.setSubject( subject );
//        simpleMailMessage.setText("Hi "+ EmployeeName+",\n" +message+"\n-with regards \nHR Team\nHashworks");
//        javaMailSender.send(simpleMailMessage);
//    }
}
