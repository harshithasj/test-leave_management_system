package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.*;
import com.hashworks.lms.exception.AppException;
import com.hashworks.lms.model.*;
import com.hashworks.lms.repository.*;
import com.hashworks.lms.service.IEmployeeService;
import com.hashworks.lms.configuration.SecurityConfig;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.xb.ltgfmt.TestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

@Service
@Slf4j
public class EmployeeService implements IEmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ReportingManagerRepository reportingManagerRepository;

    @Autowired
    EmployeeReportingManagerRepository employeeReportingManagerRepository;


    @Autowired
    UserLeavesAuditService userLeavesAuditService;


    @Autowired
    UserLeavesAuditRepository userLeavesAuditRepository;


    @Autowired
    PolicyMasterRepository policyMasterRepository;

    @Autowired
    LeaveApplicationRepository leaveApplicationRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    LeaveTypeRepository leaveTypeRepository;

    @Autowired
    EmployeeFeedbackRepository employeeFeedbackRepository;

    @Autowired
    FeedbackRepository feedbackRepository;
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


    @Override
    public Map<String, Object> getAllNewEmployees() {
        try {
            Boolean enabled = false;
            List<Object[]> employees = employeeRepository.findAllEmployeeDetails();
            List<EmployeeDetailResponse> employeeDetailResponses = new ArrayList<>();
            for (Object[] employee : employees) {
                EmployeeDetailResponse employeeDetailResponse = new EmployeeDetailResponse();
                employeeDetailResponse.setEmployeeId((BigInteger) employee[0]);
                employeeDetailResponse.setDob((Date) employee[1]);
                employeeDetailResponse.setEmailId((String) employee[2]);
                employeeDetailResponse.setMobileNumber((String) employee[3]);
                employeeDetailResponse.setEmployeeName((String) employee[4]);
                employeeDetailResponse.setUserId((BigInteger) employee[5]);
                employeeDetailResponse.setUsername((String) employee[6]);
                employeeDetailResponse.setPassword((String) employee[7]);
                employeeDetailResponse.setRoleId((BigInteger) employee[8]);
                employeeDetailResponse.setRoleName((String) employee[9]);
                employeeDetailResponse.setReportingManagerEmployeeId((BigInteger) employee[10]);
                employeeDetailResponse.setReportingManagerName((String) employee[11]);

                employeeDetailResponses.add(employeeDetailResponse);
            }

            return ResponseJsonUtilNew.getSuccessResponseMessage("Employees Fetched Successfully", employeeDetailResponses);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Map<String, Object> mapRole(RoleAndManagerMultiSaveRequest roleAndManagerMultiSaveRequest) {
        try {
            HashMap<String, Object> map = new HashMap<>();

            for (EmployeeRoleSaveRequest employeeRoleSaveRequest : roleAndManagerMultiSaveRequest.getEmployeeRoleSaveRequests()) {
                Employee employee = employeeRepository.findOne(employeeRoleSaveRequest.getEmployeeId());
                User user = userRepository.findByEmployeeId(employeeRoleSaveRequest.getEmployeeId());
//                User user = new User();
//                user.setAuthId(null);
//                user.setAuthorised( true );
//                user.setEmployee( employee );
//                user.setEnabled( true );
//                user.setPassword(null);
//                user.setUsername(employee.getEmailId());
                Role userRole = roleRepository.findOne(employeeRoleSaveRequest.getRoleId());
                Role role = roleRepository.findByRoleType("Manager");
                //             List<EmployeeReportingManager> employeeReportingManager11 = employeeReportingManagerRepository.findByReportingManagerId(reportingManagerRepository.findByEmployeeIdAndEnabledTrue(user.getEmployee().getId()).getId());
                if (user.getRoles().contains(role) == true && employeeReportingManagerRepository.findByReportingManagerId(reportingManagerRepository.findByEmployeeIdAndEnabledTrue(user.getEmployee().getId()).getId()).size() != 0) {
                    EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
                    if (employeeReportingManager != null) {
                        log.info(userRole.getRoleType());
                        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
                        userRepository.save(user);

                        //map manager's employee to his manager

                        ReportingManager reportingManager = reportingManagerRepository.findByEmployeeIdAndEnabledTrue(user.getEmployee().getId());
                        List<EmployeeReportingManager> employeeReportingManagers = employeeReportingManagerRepository.findByReportingManagerId(reportingManager.getId());
                        for (EmployeeReportingManager employeeReportingManager1 : employeeReportingManagers) {
                            employeeReportingManager1.setReportingManager(employeeReportingManager.getReportingManager());
                        }
                        //end
                        if (leaveApplicationRepository.findByReportingManagerId(reportingManager.getId()).size() > 0) {
                            leaveApplicationRepository.changeReportingManager(employeeReportingManager.getReportingManager().getId(), reportingManager.getId());
                        }

//                leaveApplicationRepository.changeReportingManager(employeeReportingManager.getReportingManager().getId(),reportingManager.getId());
                        reportingManager.setEnabled(false);
                        reportingManagerRepository.save(reportingManager);
                    } else {

                        return ResponseJsonUtilNew.postSuccessResponseMessage("Manager's manager not present so cannot change the role");
                    }
                } else {
                    log.info(userRole.getRoleType());
                    user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
                    userRepository.save(user);
                }


                if (userRole.getRoleType().equals("Manager")) {
                    ReportingManager reportingManager = new ReportingManager();

                    reportingManager.setEmployee(employeeRepository.findOne(employeeRoleSaveRequest.getEmployeeId()));
                    reportingManager.setEnabled(true);
                    reportingManager.setCreatedBy(roleAndManagerMultiSaveRequest.getCreatedBy());
                    reportingManager.setCreatedDate(new Date());
                    reportingManagerRepository.save(reportingManager);
                }

            }

            return ResponseJsonUtilNew.postSuccessResponseMessage("Employee(s) Role Added Successfully!!!");
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }

    }

    @Override
//    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map<String, Object> mapManager(EmployeeRMMappingSaveRequest employeeRMMappingSaveRequest) {
        try {
            Employee employee = employeeRepository.findOne(employeeRMMappingSaveRequest.getEmployeeId());
            ReportingManager reportingManager = reportingManagerRepository.findByEmployeeIdAndEnabledTrue(employeeRMMappingSaveRequest.getReportingManagerEmployeeId());

            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(employeeRMMappingSaveRequest.getEmployeeId());

            if (employee != null && reportingManager != null && employeeReportingManager != null) {
                employeeReportingManager.setEmployee(employee);
                employeeReportingManager.setReportingManager(reportingManager);
                employeeReportingManager.setCreatedBy(employeeRMMappingSaveRequest.getCreatedBy());
                employeeReportingManager.setCreatedDate(new Date());
                employeeReportingManagerRepository.save(employeeReportingManager);
                return ResponseJsonUtilNew.postSuccessResponseMessage("Employee Mapping to a Manager Successfully Updated!!!");
            } else if (employee != null && reportingManager != null && employeeReportingManager == null) {
                EmployeeReportingManager employeeReportingManager1 = new EmployeeReportingManager();
                employeeReportingManager1.setEmployee(employee);
                employeeReportingManager1.setReportingManager(reportingManager);
                employeeReportingManager1.setCreatedBy(employeeRMMappingSaveRequest.getCreatedBy());
                employeeReportingManager1.setCreatedDate(new Date());
                employeeReportingManagerRepository.save(employeeReportingManager1);
                return ResponseJsonUtilNew.postSuccessResponseMessage("Employee Mapping to a Manager Successfully Added!!!");
            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage("Employee/Reporting Manager doesn't exist");
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }


    //Below code needs to be changed =======================================================================

    public Map<String, Object> getAllEmployeeDetails(GetEmployeeDetailsRequest getEmployeeDetailsRequest) {

        Calendar calendar = Calendar.getInstance();
        String currentPolicyYear = String.valueOf((calendar.get(Calendar.YEAR)));

        List<Object[]> employees = employeeRepository.findAllEmployeeDetails();
//       List<Object[]> leaveBalances =employeeRepository.getAllEmployeeBalanceLeaveDetails();
        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList<>();

        for (Object[] employee : employees) {
            List<LeaveBalanceResponse> employeeLeaveBalanceList = new ArrayList<>();
            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
            employeeLeaveBalanceResponse.setEmployeeId((BigInteger) employee[0]);
            employeeLeaveBalanceResponse.setDob((Date) employee[1]);
            employeeLeaveBalanceResponse.setEmailId((String) employee[2]);
            employeeLeaveBalanceResponse.setMobileNumber((String) employee[3]);
            employeeLeaveBalanceResponse.setEmployeeName((String) employee[4]);
            employeeLeaveBalanceResponse.setUserId((BigInteger) employee[5]);
            employeeLeaveBalanceResponse.setUsername((String) employee[6]);
            employeeLeaveBalanceResponse.setPassword((String) employee[7]);
            employeeLeaveBalanceResponse.setRoleId((BigInteger) employee[8]);
            employeeLeaveBalanceResponse.setBadgeNumber((String) employee[9]);
            employeeLeaveBalanceResponse.setRoleName((String) employee[10]);
            employeeLeaveBalanceResponse.setReportingManagerId((BigInteger) employee[11]);
            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[12]);
            employeeLeaveBalanceResponse.setReportingManagerName((String) employee[13]);

            User user = userRepository.findByEmployeeId(employeeLeaveBalanceResponse.getEmployeeId().longValue());

            UserLeaveDetailsRequest userLeaveDetailsRequest = new UserLeaveDetailsRequest();
            userLeaveDetailsRequest.setPolicyMasterId(policyMasterRepository.findEnabledTrue().getId());
            userLeaveDetailsRequest.setUserId(user.getId());

            UserLeaveDetailsResponse userLeaveDetailsResponse = userLeavesAuditService.getUserLeaveDetailsNew2(userLeaveDetailsRequest, getEmployeeDetailsRequest);

            employeeLeaveBalanceResponse.setLeaveDetailsResponses(userLeaveDetailsResponse.getLeaveDetailResponses());

            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);

        }
        return ResponseJsonUtilNew.getSuccessResponseMessage("Employees Fetched Successfully", employeeLeaveBalanceResponses);
    }

    public Map<String, Object> getAllEmployeeDetailsOld() {

        Calendar calendar = Calendar.getInstance();
        String currentPolicyYear = String.valueOf((calendar.get(Calendar.YEAR)));

        List<Object[]> employees = employeeRepository.findAllEmployeeDetails();
//       List<Object[]> leaveBalances =employeeRepository.getAllEmployeeBalanceLeaveDetails();
        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList<>();

        for (Object[] employee : employees) {
            List<LeaveBalanceResponse> employeeLeaveBalanceList = new ArrayList<>();
            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
            employeeLeaveBalanceResponse.setEmployeeId((BigInteger) employee[0]);
            employeeLeaveBalanceResponse.setDob((Date) employee[1]);
            employeeLeaveBalanceResponse.setEmailId((String) employee[2]);
            employeeLeaveBalanceResponse.setMobileNumber((String) employee[3]);
            employeeLeaveBalanceResponse.setEmployeeName((String) employee[4]);
            employeeLeaveBalanceResponse.setUserId((BigInteger) employee[5]);
            employeeLeaveBalanceResponse.setUsername((String) employee[6]);
            employeeLeaveBalanceResponse.setPassword((String) employee[7]);
            employeeLeaveBalanceResponse.setRoleId((BigInteger) employee[8]);
            employeeLeaveBalanceResponse.setBadgeNumber((String) employee[9]);
            employeeLeaveBalanceResponse.setRoleName((String) employee[10]);
            employeeLeaveBalanceResponse.setReportingManagerId((BigInteger) employee[11]);
            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[12]);
            employeeLeaveBalanceResponse.setReportingManagerName((String) employee[13]);

            User user = userRepository.findByEmployeeId(employeeLeaveBalanceResponse.getEmployeeId().longValue());

            UserLeaveDetailsRequest userLeaveDetailsRequest = new UserLeaveDetailsRequest();
            userLeaveDetailsRequest.setPolicyMasterId(policyMasterRepository.findEnabledTrue().getId());
            userLeaveDetailsRequest.setUserId(user.getId());

            UserLeaveDetailsResponse userLeaveDetailsResponse = userLeavesAuditService.getUserLeaveDetailsNew(userLeaveDetailsRequest);

            employeeLeaveBalanceResponse.setLeaveDetailsResponses(userLeaveDetailsResponse.getLeaveDetailResponses());

            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);

        }
        return ResponseJsonUtilNew.getSuccessResponseMessage("Employees Fetched Successfully", employeeLeaveBalanceResponses);
    }


    //Below code needs to be changed =======================================================================

    public Map<String, Object> getEmployeeDetail(SpecificEmployeeDetailRequest specificEmployeeDetailRequest) {
        Calendar calendar = Calendar.getInstance();
        String currentPolicyYear = String.valueOf((calendar.get(Calendar.YEAR)));

        List<Object[]> employees = employeeRepository.getEmployeeDetail(specificEmployeeDetailRequest.getEmployeeId());
        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList<>();

        for (Object[] employee : employees) {
            List<LeaveBalanceResponse> employeeLeaveBalanceList = new ArrayList<>();
            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
            employeeLeaveBalanceResponse.setEmployeeId((BigInteger) employee[0]);
            employeeLeaveBalanceResponse.setDob((Date) employee[1]);
            employeeLeaveBalanceResponse.setEmailId((String) employee[2]);
            employeeLeaveBalanceResponse.setMobileNumber((String) employee[3]);
            employeeLeaveBalanceResponse.setEmployeeName((String) employee[4]);
            employeeLeaveBalanceResponse.setUserId((BigInteger) employee[5]);
            employeeLeaveBalanceResponse.setUsername((String) employee[6]);
            employeeLeaveBalanceResponse.setPassword((String) employee[7]);
            employeeLeaveBalanceResponse.setRoleId((BigInteger) employee[8]);
            employeeLeaveBalanceResponse.setBadgeNumber((String) employee[9]);
            employeeLeaveBalanceResponse.setRoleName((String) employee[10]);
            employeeLeaveBalanceResponse.setReportingManagerId((BigInteger) employee[11]);
            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[12]);
            employeeLeaveBalanceResponse.setReportingManagerName((String) employee[13]);


            User user = userRepository.findByEmployeeId(employeeLeaveBalanceResponse.getEmployeeId().longValue());

            UserLeaveDetailsRequest userLeaveDetailsRequest = new UserLeaveDetailsRequest();
            userLeaveDetailsRequest.setPolicyMasterId(policyMasterRepository.findEnabledTrue().getId());
            userLeaveDetailsRequest.setUserId(user.getId());

            UserLeaveDetailsResponse userLeaveDetailsResponse = userLeavesAuditService.getUserLeaveDetailsNew(userLeaveDetailsRequest);

            employeeLeaveBalanceResponse.setLeaveDetailsResponses(userLeaveDetailsResponse.getLeaveDetailResponses());

            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);

        }

        return ResponseJsonUtilNew.getSuccessResponseMessage("Employee Data Fetched Successfully", employeeLeaveBalanceResponses);

    }

//   public  Map<String,Object> getEmployeeDetailByManager(EmployeeDetailByManagerRequest employeeDetailByManagerRequest)
//    {
//        List< Object[]> employees = employeeRepository.getEmployeeDetailByManager(employeeDetailByManagerRequest.getReportingManagerId());
//        List<Object[]> leaveBalances = employeeRepository.getEmployeeBalanceLeaveDetailsByManager(employeeDetailByManagerRequest.getReportingManagerId());
//
//        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList <>();
//
//        for (Object[] employee:employees)
//        {
//            List<LeaveBalanceResponse> employeeLeaveBalanceList = new ArrayList <>();
//            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
//            employeeLeaveBalanceResponse.setEmployeeId( (BigInteger) employee[0] );
//            employeeLeaveBalanceResponse.setDob ((Date) employee[1] );
//            employeeLeaveBalanceResponse.setEmailId( (String) employee[2] );
//            employeeLeaveBalanceResponse.setMobileNumber( (String) employee[3] );
//            employeeLeaveBalanceResponse.setEmployeeName( (String) employee[4] );
//            employeeLeaveBalanceResponse.setUserId( (BigInteger) employee[5] );
//            employeeLeaveBalanceResponse.setUsername( (String) employee[6] );
//            employeeLeaveBalanceResponse.setPassword( (String) employee[7] );
//            employeeLeaveBalanceResponse.setRoleId( (BigInteger) employee[8] );
//            employeeLeaveBalanceResponse.setRoleName( (String) employee[9] );
//            employeeLeaveBalanceResponse.setReportingManagerId( (BigInteger) employee[10] );
//            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[11]);
//            employeeLeaveBalanceResponse.setReportingManagerName( (String) employee[12] );
//            for(Object[] leaveBalance:leaveBalances)
//            {
//                if(leaveBalance[0] ==employee[0] )
//                {
//                    LeaveBalanceResponse leaveBalanceResponse = new LeaveBalanceResponse();
//                    leaveBalanceResponse.setLeavebalance((Double)leaveBalance[1]);
//                    leaveBalanceResponse.setLeavebalanceFromPreviousYear((Double)leaveBalance[2]);
//                    leaveBalanceResponse.setLeaveType((String)leaveBalance[3]);
//                    employeeLeaveBalanceList.add(leaveBalanceResponse);
//                    employeeLeaveBalanceResponse.setLeaveBalanceResponses(employeeLeaveBalanceList);
//
//                }
//
//            }
//
//            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);
//        }
//
//        return ResponseJsonUtilNew.getSuccessResponseMessage( "Employees Fetched Successfully", employeeLeaveBalanceResponses );
//
////        HashMap map =new HashMap();
////        map.put("data",employees);
////        map.put("l",leaveBalances);
////        return map;
//
//    }
//


    public Map<String, Object> getEmployeeDetailByManager(EmployeeDetailByManagerRequest employeeDetailByManagerRequest) {

        Calendar calendar = Calendar.getInstance();
        String currentPolicyYear = String.valueOf((calendar.get(Calendar.YEAR)));

        List<Object[]> employees = employeeRepository.getEmployeeDetailByManager(employeeDetailByManagerRequest.getReportingManagerId());


        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList<>();

        for (Object[] employee : employees) {
            List<LeaveBalanceResponse> employeeLeaveBalanceList = new ArrayList<>();
            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
            employeeLeaveBalanceResponse.setEmployeeId((BigInteger) employee[0]);
            employeeLeaveBalanceResponse.setDob((Date) employee[1]);
            employeeLeaveBalanceResponse.setEmailId((String) employee[2]);
            employeeLeaveBalanceResponse.setMobileNumber((String) employee[3]);
            employeeLeaveBalanceResponse.setEmployeeName((String) employee[4]);
            employeeLeaveBalanceResponse.setUserId((BigInteger) employee[5]);
            employeeLeaveBalanceResponse.setUsername((String) employee[6]);
            employeeLeaveBalanceResponse.setPassword((String) employee[7]);
            employeeLeaveBalanceResponse.setRoleId((BigInteger) employee[8]);
            employeeLeaveBalanceResponse.setBadgeNumber((String) employee[9]);
            employeeLeaveBalanceResponse.setRoleName((String) employee[10]);
            employeeLeaveBalanceResponse.setReportingManagerId((BigInteger) employee[11]);
            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[12]);
            employeeLeaveBalanceResponse.setReportingManagerName((String) employee[13]);

            User user = userRepository.findByEmployeeId(employeeLeaveBalanceResponse.getEmployeeId().longValue());

            UserLeaveDetailsRequest userLeaveDetailsRequest = new UserLeaveDetailsRequest();
            userLeaveDetailsRequest.setPolicyMasterId(policyMasterRepository.findEnabledTrue().getId());
            userLeaveDetailsRequest.setUserId(user.getId());

            UserLeaveDetailsResponse userLeaveDetailsResponse = userLeavesAuditService.getUserLeaveDetailsNew(userLeaveDetailsRequest);

            employeeLeaveBalanceResponse.setLeaveDetailsResponses(userLeaveDetailsResponse.getLeaveDetailResponses());

            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);
        }

        return ResponseJsonUtilNew.getSuccessResponseMessage("Employees Fetched Successfully", employeeLeaveBalanceResponses);

//        HashMap map =new HashMap();
//        map.put("data",employees);
//        map.put("l",leaveBalances);
//        return map;

    }


//api to get complete leave history of an user.

    //    public Map<String,Object> getEmployeeDetail(SpecificEmployeeDetailRequest specificEmployeeDetailRequest)
//    {
//        Object[] employee = employeeRepository.getEmployeeDetail(specificEmployeeDetailRequest.getEmployeeId());
//
////        List<Object[]> leaveBalances = employeeRepository.getEmployeeBalanceLeaveDetails(specificEmployeeDetailRequest.getEmployeeId());
////        List<EmployeeLeaveBalanceResponse> employeeLeaveBalanceResponses = new ArrayList <>();
//
//        List<UserLeavesAudit> userLeavesAudits = userLeavesAuditRepository.findByUserId( specificEmployeeDetailRequest.getUserId() );
//
//            EmployeeLeaveBalanceResponse employeeLeaveBalanceResponse = new EmployeeLeaveBalanceResponse();
//            employeeLeaveBalanceResponse.setEmployeeId( (BigInteger) employee[0] );
//            employeeLeaveBalanceResponse.setDob ((Date) employee[1] );
//            employeeLeaveBalanceResponse.setEmailId( (String) employee[2] );
//            employeeLeaveBalanceResponse.setMobileNumber( (String) employee[3] );
//            employeeLeaveBalanceResponse.setEmployeeName( (String) employee[4] );
//            employeeLeaveBalanceResponse.setUserId( (BigInteger) employee[5] );
//            employeeLeaveBalanceResponse.setUsername( (String) employee[6] );
//            employeeLeaveBalanceResponse.setPassword( (String) employee[7] );
//            employeeLeaveBalanceResponse.setRoleId( (BigInteger) employee[8] );
//            employeeLeaveBalanceResponse.setRoleName( (String) employee[9] );
//            employeeLeaveBalanceResponse.setReportingManagerId( (BigInteger) employee[10] );
//            employeeLeaveBalanceResponse.setReportingManagerEmployeeId((BigInteger) employee[11]);
//            employeeLeaveBalanceResponse.setReportingManagerName( (String) employee[12] );
//
//            for(UserLeavesAudit userLeavesAudit:userLeavesAudits)
//            {
//                if(leaveBalance[0] ==employee[0] )
//                {
//                    LeaveBalanceResponse leaveBalanceResponse = new LeaveBalanceResponse();
//                    leaveBalanceResponse.setLeavebalance((Double)leaveBalance[1]);
//                    leaveBalanceResponse.setLeavebalanceFromPreviousYear((Double)leaveBalance[2]);
//                    leaveBalanceResponse.setLeaveType((String)leaveBalance[3]);
//                    employeeLeaveBalanceList.add(leaveBalanceResponse);
//                    employeeLeaveBalanceResponse.setLeaveBalanceResponses(employeeLeaveBalanceList);
//
//                }
//
//            }
//
//            employeeLeaveBalanceResponses.add(employeeLeaveBalanceResponse);
//        }
//
//        return ResponseJsonUtilNew.getSuccessResponseMessage( "Employees Fetched Successfully", employeeLeaveBalanceResponses );
////        HashMap map =new HashMap();
////        map.put("data",employee);
////        map.put("l",leaveBalances);
////        return map;
//    }
//
    public Map<String, Object> ChangePassword(ChangePasswordRequest changePasswordRequest) {
        Object[] adminEmailId = userRepository.findAdminEmailId();
        Employee employee = employeeRepository.findByEmailId(changePasswordRequest.getUsername());

      //  notificationService.sendNotificationToAdmin(adminEmailId, "password change request", "\n" + employee.getName() + " has requested for password change" + "\n", "Admin");
//  HashMap map =  new HashMap<String,String>();
//  map.put("email",adminEmailId);
//  return map;
        return ResponseJsonUtilNew.postSuccessResponseMessage("password change requested sucessfully ");

    }

    public Map<String, Object> ChangePasswordByAdmin(ChangePasswordByAdminRequest changePasswordByAdminRequest) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Optional<User> optionalUser = userRepository.findByUsername(changePasswordByAdminRequest.getUsername());
        User user = optionalUser.get();
        user.setPassword(passwordEncoder.encode(changePasswordByAdminRequest.getPassword()));
        userRepository.save(user);

      //  notificationService.sendNotificationToEmployeeForPasswordChange(user.getUsername(), "new password", "\n your new password is " + changePasswordByAdminRequest.getPassword() + "\n", user.getEmployee().getName());
        return ResponseJsonUtilNew.postSuccessResponseMessage("password changed sucessfully");
    }

    public Map<String, Object> disableEmployeeByAdmin(DisableEmployeeRequest disableEmployeeRequest) {
        User user = userRepository.findByEmployeeId(disableEmployeeRequest.getEmployeeId());
        Role role = roleRepository.findByRoleType("Manager");

        if (user.getRoles().contains(role) == true && employeeReportingManagerRepository.findByReportingManagerId(reportingManagerRepository.findByEmployeeIdAndEnabledTrue(user.getEmployee().getId()).getId()).size() != 0) {
            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
            if (employeeReportingManager != null) {
                ReportingManager reportingManager = reportingManagerRepository.findByEmployeeIdAndEnabledTrue(user.getEmployee().getId());
                List<EmployeeReportingManager> employeeReportingManagers = employeeReportingManagerRepository.findByReportingManagerId(reportingManager.getId());
                for (EmployeeReportingManager employeeReportingManager1 : employeeReportingManagers) {
                    employeeReportingManager1.setReportingManager(employeeReportingManager.getReportingManager());
                }
                reportingManager.setEnabled(false);
                if (leaveApplicationRepository.findByReportingManagerId(reportingManager.getId()).size() > 0) {
                    leaveApplicationRepository.changeReportingManager(employeeReportingManager.getReportingManager().getId(), reportingManager.getId());
                }

                user.setEnabled(false);
                user.getEmployee().setEnabled(false);
                userRepository.save(user);
                reportingManagerRepository.save(reportingManager);
                return ResponseJsonUtilNew.postSuccessResponseMessage("employee diabled sucessfully");

            } else {

                return ResponseJsonUtilNew.postSuccessResponseMessage("assaign manager then disable");
            }

        } else {
            user.setEnabled(false);
            user.getEmployee().setEnabled(false);
            userRepository.save(user);
            return ResponseJsonUtilNew.postSuccessResponseMessage("employee diabled sucessfully");

        }
    }

    public Map exportToExcel(ExportToExcelRequest exportToExcelRequest) {
        //    List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByDate(exportToExcelRequest.getStartDate(),exportToExcelRequest.getEndDate());
        Map<String, Object> hashmap = new HashMap<>();
        ResponseEntity<Object> responseEntity = null;


        Workbook workbook = new XSSFWorkbook();
//    CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Employee");
//    //header
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 8);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        List<LeaveType> leaveTypes = leaveTypeRepository.findAll();
        int rowCount = 1;
        for (LeaveType leaveType : leaveTypes) {
            Row headerRow2 = sheet.createRow(0);
            Cell cel2 = headerRow2.createCell(0);
            cel2.setCellValue("employee name");
            cel2.setCellStyle(headerCellStyle);

            Cell cel3 = headerRow2.createCell(1);
            cel3.setCellValue("badge no");
            cel3.setCellStyle(headerCellStyle);

            Cell cel4 = headerRow2.createCell(2);
            cel4.setCellValue("start date");
            cel4.setCellStyle(headerCellStyle);

            Cell cel5 = headerRow2.createCell(3);
            cel5.setCellValue("end date");
            cel5.setCellStyle(headerCellStyle);

            Cell cel6 = headerRow2.createCell(4);
            cel6.setCellValue("no of days");
            cel6.setCellStyle(headerCellStyle);

            Cell cel7 = headerRow2.createCell(5);
            cel7.setCellValue("leavetype");
            cel7.setCellStyle(headerCellStyle);

            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByDate(exportToExcelRequest.getStartDate(), exportToExcelRequest.getEndDate(), leaveType.getId());

            CellStyle cellStyle = workbook.createCellStyle();
            CreationHelper createHelper = workbook.getCreationHelper();
            cellStyle.setDataFormat(
                    createHelper.createDataFormat().getFormat("dd/mm/yy"));
            rowCount++;
            for (int i = 0; i < leaveApplications.size(); i++) {
                Row headerRow3 = sheet.createRow(rowCount);

                Cell cel10 = headerRow3.createCell(0);
                cel10.setCellValue(leaveApplications.get(i).getEmployee().getName());

                Cell cel11 = headerRow3.createCell(1);
                cel11.setCellValue(leaveApplications.get(i).getEmployee().getBadgeNumber());

                Cell cel12 = headerRow3.createCell(2);
                cel12.setCellValue(leaveApplications.get(i).getLeaveStartDate());
                cel12.setCellStyle(cellStyle);

                if (leaveApplications.get(i).getStatus().equals("withdrawn")) {
                    Cell cel13 = headerRow3.createCell(3);
                    cel13.setCellValue(leaveApplications.get(i).getWithdrawalDate());
                    cel13.setCellStyle(cellStyle);

                    LeaveApplication leaveApplication = leaveApplications.get(i);
                    int sum = userLeavesAuditRepository.getSumOfWithDrawLeaves(leaveApplication.getUser().getId(), leaveApplication.getPolicyMaster().getId(), leaveApplication.getLeaveType().getId(), leaveApplication.getLeaveStartDate(), leaveApplication.getWithdrawalDate());
                    Cell cel14 = headerRow3.createCell(4);
                    cel14.setCellValue(sum);
                } else {
                    Cell cel13 = headerRow3.createCell(3);
                    cel13.setCellValue(leaveApplications.get(i).getLeaveEndDate());
                    cel13.setCellStyle(cellStyle);

                    Cell cel14 = headerRow3.createCell(4);
                    cel14.setCellValue(leaveApplications.get(i).getAppliedNoOfDays());
                }


                Cell cel14 = headerRow3.createCell(4);
                cel14.setCellValue(leaveApplications.get(i).getAppliedNoOfDays());

                Cell cel15 = headerRow3.createCell(5);
                cel15.setCellValue(leaveApplications.get(i).getLeaveType().getName());
                rowCount++;
            }

        }

//header end
//    List<Employee> employees = employeeRepository.findAllByEnabled(true);
//    List<ExportToExcelResponse> exportToExcelResponses = new ArrayList<ExportToExcelResponse>();
//

        try {
            // Write the output to a file

//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        Date today = Calendar.getInstance().getTime();
//        String reportDate = df.format(today);

            FileOutputStream fileOut = new FileOutputStream("/home/ubuntu/LeaveReport.xls");
            workbook.write(fileOut);
            fileOut.close();
            // Closing the workbook
            workbook.close();

//        File file = new File("/home/hashworks/Downloads/LeaveReport.xls");
//        System.out.println(file.getName() + ".....inputStrem");
//        FileInputStream inputStream = new FileInputStream(file);
//        response.setHeader("Content-Disposition", "attachment; filename=\"LeaveReport.xls\"");
//        response.setContentType("application/ms-excel");
//        response.setContentLengthLong(file.length());
//
//        ServletOutputStream outputStream = response.getOutputStream();
//        IOUtils.copy(inputStream, outputStream);
//
//        outputStream.close();
//        inputStream.close();

            return ResponseJsonUtilNew.postSuccessResponseMessage("downloaded sucessfully");


        } catch (Exception e) {
            System.out.println("exception occured" + e);
        }
        return ResponseJsonUtilNew.postSuccessResponseMessage("downloaded sucessfully");
    }

    public Map<String, Object> employeeFeedback(EmployeeFeedbackRequest employeeFeedbackRequest) {
        Employee employee = employeeRepository.findByEmailId(employeeFeedbackRequest.getEmailId());
        Feedback feedback = new Feedback();
        feedback.setFeedback(employeeFeedbackRequest.getFeedback());
        feedback.setCreatedDate(new Date());
        feedbackRepository.save(feedback);

        EmployeeFeedback employeeFeedback = new EmployeeFeedback();
        employeeFeedback.setEmployee(employee);
        employeeFeedback.setEmployeeName(employee.getName());
        employeeFeedback.setFeedBack(feedback);
        employeeFeedbackRepository.save(employeeFeedback);

        return ResponseJsonUtilNew.postSuccessResponseMessage("feedback added sucessfully");

    }

    //public void downloadExcel()
//{
//
//}
    public Map<String, Object> pendingLeavesByAdmin() {
        List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByStatus("pending");
        List<PendingLeavesByAdminResponse> pendingLeavesByAdminResponses = new ArrayList<>();
        for (LeaveApplication leaveApplication : leaveApplications) {
            PendingLeavesByAdminResponse pendingLeavesByAdminResponse = new PendingLeavesByAdminResponse();
            pendingLeavesByAdminResponse.setEmployeeName(leaveApplication.getEmployee().getName());
            pendingLeavesByAdminResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
            pendingLeavesByAdminResponse.setLeaveType(leaveApplication.getLeaveType().getName());
            pendingLeavesByAdminResponse.setStartDate(leaveApplication.getLeaveStartDate());
            pendingLeavesByAdminResponse.setEndDate(leaveApplication.getLeaveEndDate());
            pendingLeavesByAdminResponse.setStatus(leaveApplication.getStatus());
            pendingLeavesByAdminResponses.add(pendingLeavesByAdminResponse);
        }
//    HashMap myMap = new HashMap();
//    myMap.put("data",pendingLeavesByAdminResponses);
//    return myMap;
        return ResponseJsonUtilNew.getSuccessResponseMessage("pending leaves data fetched sucessfully", pendingLeavesByAdminResponses);
    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    @Transactional
    public Map<String, Object> signUp(SignUpRequest signUpRequest) {
        Employee employee = employeeRepository.findByEmailId(signUpRequest.getEmail());
        Employee employeebadge=employeeRepository.findByBadgeNumber(signUpRequest.getBadgeNumber());
        Employee empmob=employeeRepository.findByMobileNumber(signUpRequest.getMobileNumber());
        if (employee != null && employee.getEmailId() != null) {
            throw new AppException("Employee already registered.!!!");
        } else if(employeebadge!=null&&employeebadge.getBadgeNumber()!=null){
            throw new AppException("Badge number already exist.!!!");
        }else if (empmob!=null&&empmob.getMobileNumber()!=null){
            throw new AppException("Mobile number already exist.!!!");
        }
        else {
            employee = new Employee();
            Object[] maxId = employeeRepository.getMaxId();
            employee.setId(Long.parseLong(maxId[0].toString()));
            employee.setEmailId(signUpRequest.getEmail().trim());
            employee.setCreatedBy(signUpRequest.getCreatedBy());
            employee.setCreatedDate(new Date());
            employee.setMobileNumber(signUpRequest.getMobileNumber());
            employee.setName(signUpRequest.getName());
            employee.setEnabled(true);
            employee.setBadgeNumber(signUpRequest.getBadgeNumber());
            employee = employeeRepository.save(employee);
            User user = new User();
            user.setId(employee.getId());
            user.setEnabled(true);
            String password = randomAlphaNumeric(employee.getEmailId().length()/2);
            user.setPassword(SecurityConfig.getPasswordEncoder().encode(password));
            user.setUsername(employee.getEmailId());
            user.setEmployee(employee);
            Set<Role> roles = roleRepository.findById(signUpRequest.getRoleId());
            user.setRoles(roles);
            user = userRepository.save(user);
          //  notificationService.sendNotificationToEmployeeForPasswordChange(user.getUsername(), "Password", "Your Leave Management application password is : " + password +". \n while login to" +
           //         " leave management, please use same password.", employee.getName());
            return ResponseJsonUtilNew.postSuccessResponseMessage("Employee added successfully!!!");
        }
    }

    public Map<String, Object> resetPassword(ResetPasswordRequest resetPassword) {
        Optional<User> userOptional = userRepository.findByUsername(resetPassword.getUserName());
        if (!userOptional.isPresent()) {
            return ResponseJsonUtilNew.entityNotFoundMessage("User Name Not found!!!");
        }
        User user = userOptional.get();
        user.setPassword(SecurityConfig.getPasswordEncoder().encode(resetPassword.getPassword().trim()));
        userRepository.save(user);
  //      notificationService.sendNotificationToEmployeeForPasswordChange(user.getUsername(), "Password Reset", "Your password reset has done. ", user.getEmployee().getName());
        return ResponseJsonUtilNew.postSuccessResponseMessage("password changed successfully!!!");

    }

//    public Map<String, Object> forgotPassword(String email) {
//        Optional<User> userOptional = userRepository.findByUsername(email);
//        if (!userOptional.isPresent()) {
//            return ResponseJsonUtilNew.entityNotFoundMessage("User Not found!!!");
//        }
//        User user = userOptional.get();
//        String password=SecurityConfig.getPasswordEncoder().;
//        notificationService.sendNotificationToEmployeeForPasswordChange(user.getUsername(), "Password", "Your Leave Management applicagion password is : " + password,user.getEmployee().getName());
//        return ResponseJsonUtilNew.postSuccessResponseMessage("Employee added successfully!!!");
//    }
}
