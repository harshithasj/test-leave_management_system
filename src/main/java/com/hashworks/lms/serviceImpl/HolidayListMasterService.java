package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.model.HolidayListMaster;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.repository.HolidayListMasterRepository;
import com.hashworks.lms.repository.RoleRepository;
import com.hashworks.lms.service.IHolidayListMasterService;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class HolidayListMasterService implements IHolidayListMasterService {

    @Autowired
    HolidayListMasterRepository holidayListMasterRepository;


    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> addHoliday(HolidaySaveRequest holidaySaveRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
            HolidayListMaster holidayListMaster = new HolidayListMaster();
            holidayListMaster.setName( holidaySaveRequest.getName() );
            holidayListMaster.setDate( holidaySaveRequest.getDate() );
            holidayListMaster.setEnabled(true);
            holidayListMaster.setCreatedBy( holidaySaveRequest.getCreatedBy() );
            holidayListMaster.setCreatedDate( new Date() );
            holidayListMasterRepository.save( holidayListMaster );
            return ResponseJsonUtilNew.postSuccessResponseMessage( "Holiday Added Successfully!!!" );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> updateHoliday(HolidayUpdateRequest holidayUpdateRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
            HolidayListMaster holidayListMaster = holidayListMasterRepository.findOne( holidayUpdateRequest.getId());
            if ( holidayListMaster != null) {
                holidayListMaster.setId(holidayUpdateRequest.getId());
                holidayListMaster.setName( holidayUpdateRequest.getName() );
                holidayListMaster.setDate( holidayUpdateRequest.getDate() );
         //       holidayListMaster.setEnabled( holidayUpdateRequest.getEnabled() );
                holidayListMaster.setModifiedBy( holidayUpdateRequest.getModifiedBy() );
                holidayListMaster.setModifiedDate( new Date() );
                holidayListMasterRepository.save( holidayListMaster );
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Holiday Updated Successfully!!!" );
            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage( "Holiday doesn't exist" );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }


    @Override
    public Map <String, Object> getAllHolidays() {
        try {
            List<HolidayListMaster> holidayListMasters = holidayListMasterRepository.findAll();
            return ResponseJsonUtilNew.getSuccessResponseMessage( "Holidays Fetched Successfully", holidayListMasters );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> deleteHoliday(HolidayDeleteRequest holidayDeleteRequest){
        try {
            HashMap <String, Object> map = new HashMap <>();
            if (holidayListMasterRepository.findOne( holidayDeleteRequest.getId() ) != null) {

                holidayListMasterRepository.delete( holidayDeleteRequest.getId() );
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Holiday Deleted Successfully!!!" );

            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage( "Holiday doesn't exist" );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );

        }
    }
}
