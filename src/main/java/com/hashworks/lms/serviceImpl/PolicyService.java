package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.PolicyDetailMasterResponse;
import com.hashworks.lms.dto.response.PolicyResponse;
import com.hashworks.lms.model.PolicyDetailMaster;
import com.hashworks.lms.model.PolicyMaster;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.repository.LeaveTypeRepository;
import com.hashworks.lms.repository.PolicyDetailMasterRepository;
import com.hashworks.lms.repository.PolicyMasterRepository;
import com.hashworks.lms.repository.RoleRepository;
import com.hashworks.lms.service.IPolicyService;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
public class PolicyService implements IPolicyService {

    @Autowired
    PolicyMasterRepository policyMasterRepository;

    @Autowired
    PolicyDetailMasterRepository policyDetailMasterRepository;

    @Autowired
    LeaveTypeRepository leaveTypeRepository;

    //working code - earlier version -- start point

    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> addPolicy(PolicySaveRequest policySaveRequest) {
        try {
            PolicyMaster policyMaster = new PolicyMaster();

            policyMaster.setYear( policySaveRequest.getYear() );
            policyMaster.setName( policySaveRequest.getName() );
            policyMaster.setLeavePolicyType( policySaveRequest.getLeavePolicyType() );
            policyMaster.setCreatedDate( new Date() );
            policyMaster.setCreatedBy( policySaveRequest.getCreatedBy() );
            policyMaster.setEnabled( false );

            policyMaster = policyMasterRepository.save( policyMaster );

            if (policyMaster.getId() > 0) {
                for (PolicyDetailMasterSaveRequest policyDetailMasterRequest : policySaveRequest.getPolicyDetailMasterSaveRequests()) {
                    PolicyDetailMaster policyDetailMaster = new PolicyDetailMaster();

                    policyDetailMaster.setCarryForwardAllowed( policyDetailMasterRequest.getCarryForwardAllowed() );
                    policyDetailMaster.setTotalNoOfDays( policyDetailMasterRequest.getTotalNoOfDays() );
                    policyDetailMaster.setLeaveType( leaveTypeRepository.findOne( policyDetailMasterRequest.getId() ) );
                    policyDetailMaster.setPolicyMaster( policyMasterRepository.findOne( policyMaster.getId() ) );
                    policyDetailMasterRepository.save( policyDetailMaster );
                }
            }
            return ResponseJsonUtilNew.postSuccessResponseMessage( "Policy Added Successfully!!!" );

        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @Override
//    @Transactional(propagation = Propagation.REQUIRED,readOnly = true)
    public Map <String, Object> getAllPolicies() {
        try {
            List <PolicyMaster> policyMasters = policyMasterRepository.findAll();

            List <PolicyResponse> policyResponses = new ArrayList <>();


            for (PolicyMaster policyMaster : policyMasters) {
                PolicyResponse policyResponse = new PolicyResponse();
                policyResponse.setPolicyId( policyMaster.getId() );
                policyResponse.setPolicyName( policyMaster.getName() );
                policyResponse.setPolicyYear( policyMaster.getYear() );
                policyResponse.setLeavePolicyType( policyMaster.getLeavePolicyType() );
                policyResponse.setEnabled( policyMaster.getEnabled() );
                policyResponse.setCreatedBy( policyMaster.getCreatedBy() );
                policyResponse.setCreatedDate( policyMaster.getCreatedDate() );
                policyResponse.setModifiedBy( policyMaster.getModifiedBy() );
                policyResponse.setModifiedDate( policyMaster.getModifiedDate() );


                List <PolicyDetailMasterResponse> policyDetailMasterResponses = new ArrayList <>();

                List <PolicyDetailMaster> policyDetailMasters = new ArrayList <>();

                policyDetailMasters = policyDetailMasterRepository.findByPolicyMasterId( (policyMaster.getId()) );


                for (PolicyDetailMaster policyDetailMaster : policyDetailMasters) {
                    PolicyDetailMasterResponse policyDetailMasterResponse = new PolicyDetailMasterResponse();

                    policyDetailMasterResponse.setPolicyDetailId( policyDetailMaster.getId() );
                    policyDetailMasterResponse.setLeavetypeId( policyDetailMaster.getLeaveType().getId() );
                    policyDetailMasterResponse.setLeavetypeName( policyDetailMaster.getLeaveType().getName() );
                    policyDetailMasterResponse.setCarryForwardAllowed( policyDetailMaster.getCarryForwardAllowed() );
                    policyDetailMasterResponse.setTotalNoOfDays( policyDetailMaster.getTotalNoOfDays() );
                    policyDetailMasterResponses.add( policyDetailMasterResponse );
                }
                policyResponse.setPolicyDetailMasterResponses( policyDetailMasterResponses );
                policyResponses.add( policyResponse );

            }

            return ResponseJsonUtilNew.getSuccessResponseMessage( "Policies Fetched Successfully", policyResponses );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getMessage());
        }
    }

 public    Map<String,Object> deletePolicy(PolicyDeleteRequest policyDeleteRequest)
    {
        PolicyMaster policyMaster = policyMasterRepository.findOne(policyDeleteRequest.getPolicyId());
        if(policyMaster.getEnabled() == true)
        {
            return ResponseJsonUtilNew.postSuccessResponseMessage( "current policy cannot be deleted" );

        }
        else
        {
            policyDetailMasterRepository.deleteByPolicyMasterId(policyDeleteRequest.getPolicyId());
            policyMasterRepository.delete(policyMaster.getId());
            return ResponseJsonUtilNew.postSuccessResponseMessage( "deleted successfully !!!" );
        }
    }
    //working code - earlier version -- end point

}
