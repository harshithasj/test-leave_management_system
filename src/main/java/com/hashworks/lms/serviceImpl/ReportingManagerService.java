package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.model.ReportingManager;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.repository.EmployeeRepository;
import com.hashworks.lms.repository.ReportingManagerRepository;
import com.hashworks.lms.repository.RoleRepository;
import com.hashworks.lms.service.IReportingManagerService;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@EnableAsync(proxyTargetClass = true)
public class ReportingManagerService implements IReportingManagerService {

    @Autowired
    ReportingManagerRepository reportingManagerRepository;

    @Autowired
    EmployeeRepository employeeRepository;
//
//    @Autowired
//    RoleService roleService;



    @Override
    public Map <String, Object> getAllRMs() {
        try {
            List <ReportingManager> reportingManagers = reportingManagerRepository.findAll();
//            roleService.getAllRoles();
            return ResponseJsonUtilNew.getSuccessResponseMessage( "Reporting Mangers Fetched Successfully", reportingManagers );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> addRM(ReportingManagerSaveRequest reportingManagerSaveRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
            ReportingManager reportingManager = new ReportingManager();
            reportingManager.setEmployee(employeeRepository.findOne( reportingManagerSaveRequest.getEmployeeId() ));
            reportingManager.setEnabled( true );
            reportingManager.setCreatedBy( reportingManagerSaveRequest.getCreatedBy() );
            reportingManager.setCreatedDate( new Date() );
            reportingManagerRepository.save( reportingManager );
            return ResponseJsonUtilNew.postSuccessResponseMessage( "Reporting Manager Added Successfully!!!" );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> addAllRMs(ReportingManagerMultiSaveRequest reportingManagerMultiSaveRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
//            ReportingManager reportingManager = new ReportingManager();
//            reportingManagerMultiSaveRequest.setEmployeeIds( employeeRepository.findAll( reportingManagerMultiSaveRequest.setEmployeeIds(  ); ) );
//            if (reportingManagerMultiSaveRequest.)

//            log.info("length:"+ reportingManagerMultiSaveRequest.getEmployeeIds().size()); //returns the list length

            for (Long employeeId : reportingManagerMultiSaveRequest.getEmployeeIds()) {

                ReportingManager reportingManager = new ReportingManager();
                reportingManager.setEmployee( employeeRepository.findOne(employeeId));
                reportingManager.setEnabled( true );
                reportingManager.setCreatedBy( reportingManagerMultiSaveRequest.getCreatedBy() );
                reportingManager.setCreatedDate( new Date() );
                reportingManagerRepository.save( reportingManager );
            }
            return ResponseJsonUtilNew.postSuccessResponseMessage( "Reporting Manager(s) Added Successfully!!!" );

        }
        catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> updateRM(ReportingManagerUpdateRequest reportingManagerUpdateRequest) {

        try {
            HashMap <String, Object> map = new HashMap <>();

            ReportingManager reportingManager = reportingManagerRepository.findOne( reportingManagerUpdateRequest.getReportingManagerId());
            if (reportingManager != null) {
//                ReportingManager reportingManager = new ReportingManager();
                reportingManager.setId( reportingManagerUpdateRequest.getReportingManagerId());
                reportingManager.setEnabled( reportingManagerUpdateRequest.getEnabled() );
                reportingManager.setModifiedBy( reportingManagerUpdateRequest.getModifiedBy() );
                reportingManager.setModifiedDate( new Date() );
                reportingManagerRepository.save( reportingManager );
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Reporting Manager Updated Successfully!!!" );
            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage( "Reporting Manager doesn't exist" );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }

//    @Override
//    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
//    public Map <String, Object> deleteRole(RoleDeleteRequest roleDeleteRequest) {
//        try {
//            HashMap <String, Object> map = new HashMap <>();
//            if (roleRepository.findOne( roleDeleteRequest.getId() ) != null) {
//
//                roleRepository.delete( roleDeleteRequest.getId() );
//                return ResponseJsonUtilNew.postSuccessResponseMessage( "Role Deleted Successfully!!!" );
//
//            } else {
//                return ResponseJsonUtilNew.entityNotFoundMessage( "Role doesn't exist" );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//
//        }
//    }


}
