package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.model.*;
//import com.hashworks.lms.model.UserLeaveBalance;
import com.hashworks.lms.model.enumeration.LeavePolicyType;
import com.hashworks.lms.repository.*;
//import com.hashworks.lms.repository.UserLeaveBalanceRepository;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Slf4j
public class ScheduledTasks {

    @Autowired
    PolicyDetailMasterRepository policyDetailMasterRepository;

    @Autowired
    PolicyMasterRepository policyMasterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserLeavesAuditRepository userLeavesAuditRepository;


    @Autowired
    LeaveApplicationRepository leaveApplicationRepository;


    @Autowired
    NotificationService notificationService;

    @Autowired
    ReportingManagerRepository reportingManagerRepository;

    @Autowired
    HolidayListMasterRepository holidayListMasterRepository;

    @Autowired
    LeaveTypeRepository leaveTypeRepository;


//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
//            "MM/dd/yyyy HH:mm:ss");

//        * * * * * *
//        | | | | | |
//        | | | | | +-- Year
//        | | | | +---- month
//        | | | +------ day
//        | | +-------- hours
//        | +---------- MInutes
//        +------------ seconds

//    @Scheduled(cron = "0 0 1 4 * *") //run on 1st april at 00:00 morning .
//    @Scheduled(cron = "*/5 * * * * *") //run at every 5 second



    //Scheduling job code started




        //this job takes previous years policy , takes the leave count of which can
        // be carryforwarded and then adds to usersleavebalance in users_leaves_audit table
        // and then disable previous year policy and enable current year policy
 //       @Scheduled(cron = "10 44 10 * * ?") //runs at 230PM everyday ..just for demo purpose //JOB - 1
 //   @Scheduled(cron = "0 12 10 * * ?")  //runs at th year end enabling abd dus
    @Scheduled(cron = "0 0 2 1 4 ?") //runs at 2AM at 1st Jan every year // this job maintained policy enable and disable functionality
    public void otherYearEndActivityYearJob() {

        log.info("Year End Activity scheduler invoked which disable the previous year policy and enables the current year policy");

        Calendar calendar = Calendar.getInstance();

        System.out.println( "Year: " + (calendar.get( Calendar.YEAR ) - 1) );

        String previousPolicyYear = String.valueOf( (calendar.get( Calendar.YEAR ) - 1) );

        String currentPolicyYear =  String.valueOf( (calendar.get( Calendar.YEAR )) );

        PolicyMaster previousYearPolicyMaster = policyMasterRepository.findByYear( previousPolicyYear );

        if(previousYearPolicyMaster != null)
        {
            PolicyMaster currentYearPolicyMaster = policyMasterRepository.findByYear( currentPolicyYear );


            List <User> users = userRepository.findByEnabledTrue();

            List <PolicyDetailMaster> previousYearPolicyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterIdAndCarryForwardAllowedTrue( previousYearPolicyMaster.getId() );

            for (User user:users) {

                for (PolicyDetailMaster previousYearPolicyDetailMaster : previousYearPolicyDetailMasters) {


                    Double userLeaveDetailsAtFyEnd = userLeavesAuditRepository.getLeavesAvailableAtFyEnd(user.getId(),previousYearPolicyDetailMaster.getPolicyMaster().getId(),previousYearPolicyDetailMaster.getId());

                    UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
                    userLeavesAudit.setDebit( 0.0 );
                    userLeavesAudit.setCredit( (userLeaveDetailsAtFyEnd != null && userLeaveDetailsAtFyEnd > 0.0) ? userLeaveDetailsAtFyEnd : 0.0  );
                    userLeavesAudit.setLeaveType( previousYearPolicyDetailMaster.getLeaveType());
                    userLeavesAudit.setPolicyMaster(currentYearPolicyMaster);
                    userLeavesAudit.setUser( user );
                    userLeavesAudit.setCreatedDate( new Date(  ) );
                    userLeavesAuditRepository.save( userLeavesAudit );
                }

            }
            previousYearPolicyMaster.setEnabled( false );
            policyMasterRepository.save( previousYearPolicyMaster );

            currentYearPolicyMaster.setEnabled( true );
            policyMasterRepository.save( currentYearPolicyMaster );
            log.info("Year End Activity scheduler completed");
        }
        else
        {
            log.info("no previous year policy");
        }

        }








//     @Scheduled(cron = "00 04 10 * * ?")    //runs at 2:31PM everyday.Just for demo purpose //JOB - 2
    @Scheduled(cron = "0 30 2 1 * ?") //runs at 2:30AM Everymonth // this job credits leaves to all users every month
    public void leaveAllocationBasedOnPolicyMonthlyJob() {
        log.info("Leave Allocation Based on Policy scheduler invoked");
        Calendar calendar = Calendar.getInstance();

       String currentPolicyYear = String.valueOf( calendar.get( Calendar.YEAR ) ); //this is the correct one

//        String currentPolicyYear = String.valueOf( (calendar.get( Calendar.YEAR ) - 1) ); //used for just testing purpose

        PolicyMaster policyMaster = policyMasterRepository.findEnabledTrue();

        List <PolicyDetailMaster> policyDetailMasters = policyDetailMasterRepository.findAllByPolicyMasterId( policyMaster.getId() );

        List <User> users = userRepository.findByEnabledTrue();

//        System.out.println( String.valueOf( policyMaster.getLeavePolicyType() ) );


        if (policyMaster.getLeavePolicyType().equals( LeavePolicyType.MONTHLY )) {

            for (User user : users) {

                for (PolicyDetailMaster policyDetailMaster : policyDetailMasters) {

                    UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
                    userLeavesAudit.setDebit( 0.0 );
                    userLeavesAudit.setCredit( policyDetailMaster.getTotalNoOfDays()/12.0 );
                    userLeavesAudit.setLeaveType( policyDetailMaster.getLeaveType());
                    userLeavesAudit.setPolicyMaster( policyDetailMaster.getPolicyMaster() );
                    userLeavesAudit.setUser( user );
                    userLeavesAudit.setCreatedDate( new Date(  ) );
                    userLeavesAuditRepository.save( userLeavesAudit );

                }


            }

        } else {
            //code to add for other leave policy type monthly quaterly yearly
        }

        log.info("Leave Allocation Based on Policy scheduler completed");
    }







  //  @Scheduled(cron = "00 45 21 * * ?")    //runs at 2:40PM everyday.Just for demo purpose //JOB - 3
  //  @Scheduled(cron = "0 00 3 * * ?") //runs at 3AM everyday //this job changes the leave application status as "consumption started" on the day when your applied leave starts
      @Scheduled(cron = "0 00 20 * * ?")
    public void statusChangetoConsumptionStartedDailyJob() {

        log.info("daily job updating the leave applications status from approved to consumption started started");

        List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByApprovedStatusAndCurrentDate2("approved",new Date());

        if(leaveApplications.size() != 0)
        {
            for (LeaveApplication leaveApplication:leaveApplications) {

                leaveApplication.setStatus( "consumption started" );
                leaveApplication.setWithdrawalStatus( "NO" );
                leaveApplication.setModifiedDate( new Date(  ) );
                leaveApplicationRepository.save( leaveApplication );

            }


//            UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
//            userLeavesAudit.setDebit( 1.0 );
//            userLeavesAudit.setCredit( 0.0 );
//            userLeavesAudit.setLeaveType( leaveApplication.getLeaveType());
//            userLeavesAudit.setPolicyMaster( leaveApplication.getPolicyMaster() );
//            userLeavesAudit.setUser( leaveApplication.getUser() );
//            userLeavesAudit.setCreatedDate( new Date(  ) );
//            userLeavesAuditRepository.save( userLeavesAudit );
        }

        log.info("daily job updating the leave applications status from approved to consumption started completed");
    }




 //   @Scheduled(cron = "0 58 10 * * ?") //runs at 2:45PM everyday.Just for demo purpose //JOB - 4
 //   @Scheduled(cron = "0 00 4 * * ?") //runs at 4AM everyday //this job debits the leaves for which you have applied everyday
    @Scheduled(cron = "0 00 22 * * ?")
    public void consumptionStartedLeaveApplicationsJobDailyJob() {

        log.info("daily job updating the leave balances started");
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        int sum = holidayListMasterRepository.findByHoliday(date);
        if(calendar.get(Calendar.DAY_OF_WEEK) != 7 && calendar.get(Calendar.DAY_OF_WEEK) != 1 && sum == 0)
        {
            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByConsumptionStartedStatusAndCurrentDate("consumption started","NO",new Date());
            if(leaveApplications.size() != 0)
            {
                for (LeaveApplication leaveApplication:leaveApplications) {

                    UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
                    userLeavesAudit.setDebit( 1.0 );
                    userLeavesAudit.setCredit( 0.0 );
                    userLeavesAudit.setLeaveType( leaveApplication.getLeaveType());
                    userLeavesAudit.setPolicyMaster( leaveApplication.getPolicyMaster() );
                    userLeavesAudit.setUser( leaveApplication.getUser() );
                    userLeavesAudit.setCreatedDate( new Date(  ) );
                    userLeavesAuditRepository.save( userLeavesAudit );

                }
                log.info("daily job updating the leave balances completed");
            }

        }

    }

 //   @Scheduled(cron = "00 46 21 * * ?") //runs at 2:50PM everyday.Just for demo purpose //JOB - 5
    @Scheduled(cron = "0 00 23 * * ?") //runs at 10PM everyday //this job changes the leave application status as "consumed fully" on the day when your applied leave ends
    public void statusChangetoConsumedFullyDailyJob() {

        log.info("daily job updating the leave applications status to consumed fully started");


        List<LeaveApplication> consumedLeaveApplications = leaveApplicationRepository.findByApprovedStatusAndCurrentDate("consumption started",new Date());
            if(consumedLeaveApplications.size() != 0)
            {
                for (LeaveApplication consumedLeaveApplication:consumedLeaveApplications) {

                    consumedLeaveApplication.setStatus( "consumed fully" );
                    consumedLeaveApplication.setWithdrawalStatus( "NA" );
                    consumedLeaveApplication.setModifiedDate( new Date(  ) );
                    leaveApplicationRepository.save( consumedLeaveApplication );

                }
            }


        log.info("daily job updating the leave applications status to consumed fully completed");
    }





    @Scheduled(cron = "00 30 10 * * ?")
    public void viewLeavesByManager()
    {
        try{
            List<ReportingManager> reportingManagers = reportingManagerRepository.findAll();
            for (Iterator<ReportingManager> iter = reportingManagers.iterator(); iter.hasNext(); )
            {
                ReportingManager rm1 =iter.next();
                List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerId1(rm1.getId());
                //         ReportingManager rm1 = reportingManagerRepository.findOne(managerLeaveViewRequest.getManagerId());
                if(leaveApplications.size() > 0)
                {
                    String message ="\n Below employees have applied the leaves on the following dates \n \n";
                    for (Iterator<LeaveApplication> iter1 = leaveApplications.iterator(); iter1.hasNext(); ) {
                        LeaveApplication leaveApplication = iter1.next();
                        String endDate = formatDate(leaveApplication.getLeaveEndDate());
                        String startDate = formatDate(leaveApplication.getLeaveStartDate());
                        message  = message + leaveApplication.getUser().getEmployee().getName()+" has applied "+leaveApplication.getLeaveType().getName()+" from "+startDate+" to " +endDate+".\n";
                    }
                    message = message + "\nplease click the link to respond : http://leave.hashworks.co \n";
//                    notificationService.sendNotificationToManager(rm1.getEmployee().getEmailId(),"leave applications",message,rm1.getEmployee().getName());

                }
            }
            //    return ResponseJsonUtilNew.getSuccessResponseMessage("message","email function executed sucessfully");
        }
        catch(NullPointerException e)
        {
            // return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            // return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }
    public String formatDate(Date date)
    {
        //Date myDate = new Date();
        SimpleDateFormat sd1 = new SimpleDateFormat("dd-MM-yyyy");
        String s = sd1.format(date);
//        System.out.println(s);
        return s;
    }

    //   @Scheduled(cron = "00 35 12 * * ?")  //demo purpose
    @Scheduled(cron = "00 00 3 1 4 ?")
    public void reduceCreditIfMoreThen30()
    {
        List<User> users = userRepository.findByEnabledTrue();
        PolicyMaster policyMaster = policyMasterRepository.findEnabledTrue();
        LeaveType leaveType = leaveTypeRepository.findByName("Earn leave");
        for (User user : users){
            Double[] creditSums = userLeavesAuditRepository.getLeavesCreditedMoreThen30(user.getId(),policyMaster.getId(), leaveType.getId());

            for (Double creditSum : creditSums){
         if(creditSum.doubleValue() > 30){
                System.out.println(creditSum);
                double remain = (creditSum.doubleValue() - 30) * (-1);
             System.out.println(remain);
             UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
             userLeavesAudit.setCreatedDate(new Date());
             userLeavesAudit.setCredit(remain);
             userLeavesAudit.setDebit(0.0);
             userLeavesAudit.setLeaveType(leaveType);
             userLeavesAudit.setPolicyMaster(policyMaster);
             userLeavesAudit.setUser(user);
             userLeavesAuditRepository.save(userLeavesAudit);
            }
            }
        }
    }

    //Scheduling job code ended
}
