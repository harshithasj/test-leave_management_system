package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.RoleDeleteRequest;
import com.hashworks.lms.dto.request.RoleSaveRequest;
import com.hashworks.lms.dto.request.RoleUpdateRequest;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.repository.RoleRepository;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RoleService implements IRoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Map <String, Object> getAllRoles() {
        try {
            List <Role> roles = roleRepository.findAll();
            return ResponseJsonUtilNew.getSuccessResponseMessage( "Roles Fetched Successfully", roles );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> addRole(RoleSaveRequest roleSaveRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
            Role role = new Role();
            role.setRoleType( roleSaveRequest.getRoleType() );
            role.setEnabled( true);
            role.setCreatedBy( roleSaveRequest.getCreatedBy() );
            role.setCreatedDate( new Date() );
            roleRepository.save( role );
            return ResponseJsonUtilNew.postSuccessResponseMessage( "Role Added Successfully!!!" );
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> updateRole(RoleUpdateRequest roleUpdateRequest) {

        try {
            HashMap <String, Object> map = new HashMap <>();
            Role role = roleRepository.findOne( roleUpdateRequest.getId() );
            if ( role != null) {
                role.setRoleId( roleUpdateRequest.getId() );
                role.setRoleType( roleUpdateRequest.getRoleType() );
                role.setEnabled( roleUpdateRequest.getEnabled() );
                role.setModifiedBy( roleUpdateRequest.getModifiedBy() );
                role.setModifiedDate( new Date() );
                roleRepository.save( role );
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Role Updated Successfully!!!" );
            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage( "Role doesn't exist" );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public Map <String, Object> deleteRole(RoleDeleteRequest roleDeleteRequest) {
        try {
            HashMap <String, Object> map = new HashMap <>();
            if (roleRepository.findOne( roleDeleteRequest.getId() ) != null) {

                roleRepository.delete( roleDeleteRequest.getId() );
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Role Deleted Successfully!!!" );

            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage( "Role doesn't exist" );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );

        }
    }


}
