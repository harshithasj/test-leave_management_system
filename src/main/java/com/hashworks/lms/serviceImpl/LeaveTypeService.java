package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.LeaveTypeDeleteRequest;
import com.hashworks.lms.dto.request.LeaveTypeSaveRequest;
import com.hashworks.lms.dto.request.LeaveTypeUpdateRequest;
import com.hashworks.lms.model.LeaveType;
import com.hashworks.lms.repository.LeaveTypeRepository;
import com.hashworks.lms.service.ILeaveTypeService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class LeaveTypeService implements ILeaveTypeService {

    @Autowired
    LeaveTypeRepository leaveTypeRepository;

    @Override
    public Map<String,Object> getAllLeaveTypes()
    {
        try {
            List <LeaveType> leaveTypes = leaveTypeRepository.findAllByIsEnabled();
            return ResponseJsonUtilNew.getSuccessResponseMessage( "leaveTypes Fetched Successfully", leaveTypes );
        }catch (Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

    public Map<String,Object> addLeaveType(LeaveTypeSaveRequest leaveTypeRequest)
    {
        try {

                LeaveType leaveType = new LeaveType();
                leaveType.setName(leaveTypeRequest.getName());
                leaveType.setEnabled(true);
                leaveType.setCreatedBy(leaveTypeRequest.getCreatedBy());
                leaveType.setCreatedDate(new Date());
                leaveType.setModifiedDate(null);
                leaveTypeRepository.save(leaveType);
                return ResponseJsonUtilNew.postSuccessResponseMessage( "leave type Added Successfully!!!" );

         }
         catch(Exception e)
         {
             return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
         }


    }

//    public ResponseEntity<Map> addAllLeaveTypes(String requestString)
//    {
//        try {
//        JSONObject jo = new JSONObject(requestString);
//        JSONArray leaveTypesJson = jo.getJSONArray("leaveTypes");
//
//        for(int i=0;i<leaveTypesJson.length();i++)
//        {
//            JSONObject jo1 = (JSONObject)leaveTypesJson.get(i);
//            String lName = jo1.getString("lName");
//            Boolean lEnabled = jo1.getBoolean("lEnabled");
//            String lcreatedBy = jo1.getString("lCreatedBy");
//
//            LeaveType leaveType = new LeaveType();
//            leaveType.setName(lName);
//            leaveType.setEnabled(lEnabled);
//            leaveType.setCreatedBy(lcreatedBy);
//            leaveType.setModifiedBy(lcreatedBy);
//            leaveType.setCreatedDate(new Date());
//            leaveType.setModifiedDate(new Date());
//            leaveTypeRepository.save(leaveType);
//        }
//
//        return new ResponseEntity<Map>(ResponseJsonUtil.getSuccessResponseJson("added sucessfully"),HttpStatus.OK);
//        }
//        catch(org.json.JSONException e)
//        {
//            return new ResponseEntity<Map>(ResponseJsonUtil.getFailedResponseJson("Provide all fields"), HttpStatus.BAD_REQUEST);
//        }
//
//        catch(javax.validation.ConstraintViolationException e)
//        {
//            return new ResponseEntity<Map>(ResponseJsonUtil.getFailedResponseJson("null fields not allowed"), HttpStatus.BAD_REQUEST);
//        }
//
//    }
    public Map<String,Object> deleteLeaveType(LeaveTypeDeleteRequest leaveTypeRequest)
    {
        try {
            if (leaveTypeRepository.findOne( leaveTypeRequest.getId() ) != null) {

                leaveTypeRepository.deleteLeaveType(leaveTypeRequest.getId());
                return ResponseJsonUtilNew.postSuccessResponseMessage( "leaveType Deleted Successfully!!!" );

            } else {
                return ResponseJsonUtilNew.entityNotFoundMessage("LeaveType id does not exist");
            }

                    }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }

    }

    public Map<String,Object> editLeaveType(LeaveTypeUpdateRequest leaveTypeRequest)
    {
        try{

                LeaveType leaveType = leaveTypeRepository.findOne(leaveTypeRequest.getId());
                leaveType.setId(leaveTypeRequest.getId());
                leaveType.setModifiedDate(new Date());
                leaveType.setEnabled(leaveTypeRequest.getEnabled());
                leaveType.setName(leaveTypeRequest.getName());
                leaveType.setModifiedBy(leaveTypeRequest.getModifiedBy());
                leaveTypeRepository.save(leaveType);
                return ResponseJsonUtilNew.postSuccessResponseMessage( "Leave type updated Successfully!!!" );


        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }

    }
}
