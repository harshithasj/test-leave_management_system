package com.hashworks.lms.serviceImpl;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.ViewLeaveStatusResponse;
import com.hashworks.lms.dto.response.ViewLeavesByManagerResponse;
import com.hashworks.lms.model.*;
import com.hashworks.lms.repository.*;
import com.hashworks.lms.service.ILeaveApplicationService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class LeaveApplicationService implements ILeaveApplicationService {

    //working code - earlier version -- start point

//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    PolicyMasterRepository policyMasterRepository;
//
//    @Autowired
//    LeaveTypeRepository leaveTypeRepository;
//
//    @Autowired
//    LeaveApplicationRepository leaveApplicationRepository;
//
//    @Autowired
//    EmployeeReportingManagerRepository employeeReportingManagerRepository;
//
//    @Autowired
//    ReportingManagerRepository reportingManagerRepository;
//
//    @Autowired
//    NotificationService notificationService;
//
//    @Autowired
//    UserLeaveBalanceRepository userLeaveBalanceRepository;
//
//    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
//    public  Map<String,Object> applyForLeave(LeaveApplicationSaveRequest leaveApplicationRequest)
//   {
//
//       try {
//
//          LeaveType leaveType= leaveTypeRepository.findOne(leaveApplicationRequest.getLeaveTypeId());
//          User user = userRepository.findOne(leaveApplicationRequest.getUserId());
//          PolicyMaster policyMaster = policyMasterRepository.findOne(leaveApplicationRequest.getPolicyMasterId());
//          LeaveType leaveType1 = leaveTypeRepository.findOne(leaveApplicationRequest.getLeaveTypeId());
//          ReportingManager reportingManager = reportingManagerRepository.findOne(leaveApplicationRequest.getReportingManagerId());
//
//           if(leaveType.getName().equals("LOP"))
//           {
//               LeaveApplication leaveApplication = new LeaveApplication();
//               leaveApplication.setUser(user);
//               leaveApplication.setPolicyMaster(policyMaster);
//               leaveApplication.setLeaveType(leaveType1);
//               leaveApplication.setReportingManager(reportingManager);
//
//               leaveApplication.setStatus("pending");
//               leaveApplication.setCreatedDate(new Date());
//               leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
//               leaveApplication.setEmployeeCommentSubmission(leaveApplicationRequest.getEmployeeCommentSubmission());
//               leaveApplication.setLeaveStartDate(leaveApplicationRequest.getStartDate());
//               leaveApplication.setLeaveEndDate(leaveApplicationRequest.getEndDate());
//               leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
//               leaveApplication.setAppliedNoOfDays(leaveApplicationRequest.getNoOfDays());
//               leaveApplication.setEmployee(userRepository.findByUserId(leaveApplicationRequest.getUserId()));
//
//               EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
//
//               List<String> emailIds = new ArrayList<String>();
//               String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//               emailIds.add(emailId1);
//               EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//               if(employeeReportingManager2 != null)
//               {
//                   String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                   emailIds.add(emailId2);
//               }
//
////               notificationService.sendNotification(user.getUsername(),emailIds,"Leave Application","Hi "+emailId1+",\n" +user.getUsername()+"has applied for LOP from"+leaveApplicationRequest.getStartDate()+" to "+leaveApplicationRequest.getEndDate());
//               notificationService.sendNotification(user.getUsername(),emailIds,"Leave Application","Leave Application Submitted Successfully");
//
//               leaveApplicationRepository.save(leaveApplication);
//
//
//               return ResponseJsonUtilNew.postSuccessResponseMessage("leave applied Successfully!!!");
//           }
//
//           UserLeaveBalance userLeaveBalance = userLeaveBalanceRepository.findByUserIdAndLeaveType(leaveApplicationRequest.getUserId(),leaveApplicationRequest.getLeaveTypeId());
//           if( userLeaveBalance.getBalanceLeavesAvailable() >= leaveApplicationRequest.getNoOfDays())
//           {
//               LeaveApplication leaveApplication = new LeaveApplication();
//               leaveApplication.setUser(user);
//               leaveApplication.setPolicyMaster(policyMaster);
//               leaveApplication.setLeaveType(leaveType1);
//               leaveApplication.setReportingManager(reportingManager);
//
//               leaveApplication.setStatus("pending");
//               leaveApplication.setCreatedDate(new Date());
//               leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
//               leaveApplication.setEmployeeCommentSubmission(leaveApplicationRequest.getEmployeeCommentSubmission());
//               leaveApplication.setLeaveStartDate(leaveApplicationRequest.getStartDate());
//               leaveApplication.setLeaveEndDate(leaveApplicationRequest.getEndDate());
//               leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
//               leaveApplication.setAppliedNoOfDays(leaveApplicationRequest.getNoOfDays());
//               leaveApplication.setEmployee(userRepository.findByUserId(leaveApplicationRequest.getUserId()));
//               leaveApplicationRepository.save(leaveApplication);
//               userLeaveBalance.setBalanceLeavesAvailable(userLeaveBalance.getBalanceLeavesAvailable()-leaveApplication.getAppliedNoOfDays());
//               userLeaveBalanceRepository.save(userLeaveBalance);
//
//
//               EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
//
//               List<String> emailIds = new ArrayList<String>();
//               String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//               emailIds.add(emailId1);
//               EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//               if(employeeReportingManager2 != null)
//               {
//                   String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                   emailIds.add(emailId2);
//               }
////               notificationService.sendNotification(user.getUsername(),emailIds,"leave application","Hi "+emailId1+",\n" +user.getUsername()+"has applied for "+leaveType.getName()+" from"+leaveApplicationRequest.getStartDate()+" to "+leaveApplicationRequest.getEndDate());
//               notificationService.sendNotification(user.getUsername(),emailIds,"leave application","Leave Application Submitted Successfully" );
//
//
////               EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
////
////               List<String> emailIds = new ArrayList<String>();
////               String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
////               emailIds.add(emailId1);
////               EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
////               if(employeeReportingManager2 != null)
////               {
////                   String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
////                   emailIds.add(emailId2);
////               }
////
////
//
//
//
// //              User user = userRepository.findOne(leaveApplicationRequest.getUserId());
////               EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());
////               notificationService.sendNotification(user.getUsername(),employeeReportingManager.getReportingManager().getEmployee().getEmailId(),"leave application",user.getUsername()+"applied leave");
//
//               return ResponseJsonUtilNew.postSuccessResponseMessage("leave applied Successfully!!!");
//
//           }
//           else
//           {
//               return ResponseJsonUtilNew.postSuccessResponseMessage("no leave balance!!!");
//
//           }
//
//       }
//       catch(NullPointerException e)
//       {
//           return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//       }
//       catch(Exception e)
//       {
//           return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//       }
//
//
//   }
//    public Map<String,Object> viewLeavesStatus(LeaveStatusRequest leaveStatusRequest)
//    {
//        try {
//            List<LeaveApplication> leaves = leaveApplicationRepository.findByUserId(leaveStatusRequest.getId());
//            List<ViewLeaveStatusResponse> leaveResponse =new ArrayList<>();
//
//            for (Iterator<LeaveApplication> iter = leaves.iterator(); iter.hasNext(); ) {
//                LeaveApplication leaveApplication = iter.next();
//                ViewLeaveStatusResponse viewLeaveStatusResponse = new ViewLeaveStatusResponse();
//                viewLeaveStatusResponse.setUserId(leaveApplication.getUser().getId());
//                viewLeaveStatusResponse.setCreatedBy(leaveApplication.getCreatedBy());
//                viewLeaveStatusResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//                viewLeaveStatusResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//                viewLeaveStatusResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//                viewLeaveStatusResponse.setStartDate(leaveApplication.getLeaveStartDate());
//                viewLeaveStatusResponse.setEndDate(leaveApplication.getLeaveEndDate());
//                viewLeaveStatusResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//                viewLeaveStatusResponse.setStatus(leaveApplication.getStatus());
//                viewLeaveStatusResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//                viewLeaveStatusResponse.setLeaveApplicationId(leaveApplication.getId());
//                viewLeaveStatusResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//                leaveResponse.add(viewLeaveStatusResponse);
//            }
//
//            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//
//    }
//
//   public Map<String,Object> viewLeavesByManager(ManagerLeaveViewRequest managerLeaveViewRequest)
//   {
//       try{
//       List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerId(managerLeaveViewRequest.getManagerId());
//
//           List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();
//
//           for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
//               LeaveApplication leaveApplication = iter.next();
//               ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
//               viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
//               viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
//               viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
//               viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//               viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//               viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//               viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
//               viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
//               viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//               viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
//               viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//               viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
//               viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//               leaveResponse.add(viewLeavesByManagerResponse);
//           }
//
//
//           return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//       }
//       catch(NullPointerException e)
//       {
//           return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//       }
//       catch(Exception e)
//       {
//           return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//       }
//   }
//
//  public  Map<String,Object> viewLeavesByManagerByDate(ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest)
//    {
//        try{
//            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerIdByDate(managerLeaveViewByDateRequest.getManagerId(),managerLeaveViewByDateRequest.getStartDate(),managerLeaveViewByDateRequest.getEndDate());
//
//            List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();
//
//            for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
//                LeaveApplication leaveApplication = iter.next();
//                ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
//                viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
//                viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
//                viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
//                viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//                viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//                viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//                viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
//                viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
//                viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//                viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
//                viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//                viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
//                viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//                leaveResponse.add(viewLeavesByManagerResponse);
//            }
//
//
//            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//   public Map<String,Object> approveLeaves(ApproveLeavesRequest approveLeavesRequest)
//   {
//       try{
//      LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveLeavesRequest.getLeaveApplicationId());
//     // if(leaveApplication.getReportingManager().getId() == approveLeavesRequest.getReportingManagerId())
//    //  {
//          leaveApplication.setStatus("approved");
//          leaveApplication.setReportingManagerCommentsApproval(approveLeavesRequest.getComment());
//          leaveApplicationRepository.save(leaveApplication);
//
//           EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//           List<String> emailIds = new ArrayList<String>();
//           String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//           emailIds.add(emailId1);
//           EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//           if(employeeReportingManager2 != null)
//           {
//               String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//               emailIds.add(emailId2);
//           }
//           notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"Leave Application Response",leaveApplication.getUser().getUsername()+" leave approved");
//
//
//           //     leaveApplication.getUser().getId()
////           EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
////           notificationService.sendNotification(leaveApplication.getUser().getUsername(),employeeReportingManager.getReportingManager().getEmployee().getEmailId(),"leave application",leaveApplication.getUser().getUsername()+" leave approved");
//
//
//           return ResponseJsonUtilNew.postSuccessResponseMessage("leave approved Successfully!!!");
//
////      }
////      else
////      {
////          return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id");
////      }
//       }
//       catch(NullPointerException e)
//       {
//           return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//       }
//       catch(Exception e)
//       {
//           return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//       }
//   }
//
//   public Map<String,Object> rejectLeaves(RejectLeavesRequest rejectLeavesRequest)
//    {
//      try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(rejectLeavesRequest.getLeaveApplicationId());
////            if(leaveApplication.getReportingManager().getId() == rejectLeavesRequest.getReportingManagerId() )
////            {
//               UserLeaveBalance userLeaveBalance = userLeaveBalanceRepository.findByUserIdAndLeaveType(leaveApplication.getUser().getId(),leaveApplication.getLeaveType().getId());
//               userLeaveBalance.setBalanceLeavesAvailable(userLeaveBalance.getBalanceLeavesAvailable()+leaveApplication.getAppliedNoOfDays());
//               userLeaveBalanceRepository.save(userLeaveBalance);
//                leaveApplication.setStatus("cancelled");
//                leaveApplication.setEmployeeCommentsRejectionRequest(rejectLeavesRequest.getComment());
//                leaveApplicationRepository.save(leaveApplication);
//
//          EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//          List<String> emailIds = new ArrayList<String>();
//          String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//          emailIds.add(emailId1);
//          EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//          if(employeeReportingManager2 != null)
//          {
//              String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//              emailIds.add(emailId2);
//          }
//          notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+" has cancelled the leave");
//
//
//
//
//
////                EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
////                notificationService.sendNotification(leaveApplication.getUser().getUsername(),employeeReportingManager.getReportingManager().getEmployee().getEmailId(),"leave application",leaveApplication.getUser().getUsername()+" leaves cancelled ");
//
//                return ResponseJsonUtilNew.postSuccessResponseMessage(" leave cancelled Successfully!!!");
//
////            }
////            else
////            {
////                return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id");
////            }
//        }
//      catch(NullPointerException e)
//      {
//          return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
//      }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//
//   public Map<String,Object> rejectionLeavesApproval(ApproveRejectionLeavesRequest approveRejectionLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveRejectionLeavesRequest.getLeaveApplicatioId());
////            if(leaveApplication.getReportingManager().getId() == approveRejectionLeavesRequest.getReportingManagerId())
////            {
//                leaveApplication.setStatus("LeaveRejectioApproved");
//                leaveApplication.setReportingManagerCommentsRejectionApproval(approveRejectionLeavesRequest.getComment());
//                leaveApplicationRepository.save(leaveApplication);
//                return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejection approved Successfully!!!");
//
////            }
////            else
////            {
////                return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id");
////            }
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }
//    public Map<String,Object> editAppliedLeave(EditAppliedLeaveRequest editAppliedLeaveRequest)
//    {
//        try{
////            if(editAppliedLeaveRequest.getStartDate().getTime()<new Date().getTime() && editAppliedLeaveRequest.getEndDate().getTime() <editAppliedLeaveRequest.getStartDate().getTime())
////            {
////                return ResponseJsonUtilNew.postSuccessResponseMessage("start date should be greater then present date");
////
////            }
//        LeaveApplication leaveApplication = leaveApplicationRepository.findOne(editAppliedLeaveRequest.getLeaveApplicationId());
//
//        UserLeaveBalance userLeaveBalance = userLeaveBalanceRepository.findByUserIdAndLeaveType(leaveApplication.getUser().getId(),leaveApplication.getLeaveType().getId());
//                userLeaveBalance.setBalanceLeavesAvailable(userLeaveBalance.getBalanceLeavesAvailable()+leaveApplication.getAppliedNoOfDays());
//                userLeaveBalanceRepository.save(userLeaveBalance);
//
//             leaveApplication.setLeaveType(leaveTypeRepository.findOne(editAppliedLeaveRequest.getLeaveTypeId()));
//
//            UserLeaveBalance userLeaveBalance1 =    userLeaveBalanceRepository.findByUserIdAndLeaveType(leaveApplication.getUser().getId(),editAppliedLeaveRequest.getLeaveTypeId());
//            PolicyMaster policyMaster = policyMasterRepository.findOne(editAppliedLeaveRequest.getPolicyMasterId());
//            if(userLeaveBalance1.getBalanceLeavesAvailable()>=editAppliedLeaveRequest.getNoOfDays())
//            {
//                leaveApplication.setLeaveStartDate(editAppliedLeaveRequest.getStartDate());
//                leaveApplication.setLeaveEndDate(editAppliedLeaveRequest.getEndDate());
//                leaveApplication.setAppliedNoOfDays(editAppliedLeaveRequest.getNoOfDays());
//                leaveApplication.setEmployeeCommentSubmission(editAppliedLeaveRequest.getComment());
//                leaveApplication.setPolicyMaster(policyMaster);
//                leaveApplication.setModifiedBy(editAppliedLeaveRequest.getModifiedBy());
//                leaveApplicationRepository.save( leaveApplication);
//                userLeaveBalance1.setBalanceLeavesAvailable(userLeaveBalance1.getBalanceLeavesAvailable()-editAppliedLeaveRequest.getNoOfDays());
//                userLeaveBalanceRepository.save(userLeaveBalance1);
//
//                EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//                List<String> emailIds = new ArrayList<String>();
//                String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId1);
//                EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//                if(employeeReportingManager2 != null)
//                {
//                    String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                    emailIds.add(emailId2);
//                }
//                notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+" edited the leave");
//
//
//                return ResponseJsonUtilNew.postSuccessResponseMessage("updated sucessfully");
//
//            }else
//            {
//                return ResponseJsonUtilNew.postSuccessResponseMessage("no leave balance!!!");
//
//            }
//                     }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//
//    }
//
//   public Map<String,Object> managerRejectLeaves(ManagerRejectLeavesRequest managerRejectLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(managerRejectLeavesRequest.getLeaveApplicationId());
//
//         //   if(leaveApplication.getReportingManager().getId() == managerRejectLeavesRequest.getReportingManagerId() && leaveApplication.getStatus().equals("pending"))
//         //   {
//                UserLeaveBalance userLeaveBalance =  userLeaveBalanceRepository.findByUserIdAndLeaveType(leaveApplication.getUser().getId(),leaveApplication.getLeaveType().getId());
//                userLeaveBalance.setBalanceLeavesAvailable(userLeaveBalance.getBalanceLeavesAvailable()+leaveApplication.getAppliedNoOfDays());
//                userLeaveBalanceRepository.save(userLeaveBalance);
//
//                leaveApplication.setStatus("rejected");
//                leaveApplication.setReportingManagerCommentsApproval(managerRejectLeavesRequest.getComment());
//                leaveApplicationRepository.save(leaveApplication);
//
//            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
//
//            List<String> emailIds = new ArrayList<String>();
//            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
//            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+"'s leave rejected");
//
//
//
////                EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());
////                notificationService.sendNotification(leaveApplication.getUser().getUsername(),employeeReportingManager.getReportingManager().getEmployee().getEmailId(),"leave application",leaveApplication.getUser().getUsername()+" leave rejected  by manager");
//
//                return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejected!!!");
//
////            }
////            else
////            {
////                return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id or not a pending request");
////            }
//        }
//        catch(NullPointerException e)
//        {
//            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }

    //working code - earlier version - end point




    @Autowired
    UserRepository userRepository;

    @Autowired
    PolicyMasterRepository policyMasterRepository;

    @Autowired
    LeaveTypeRepository leaveTypeRepository;

    @Autowired
    LeaveApplicationRepository leaveApplicationRepository;

    @Autowired
    EmployeeReportingManagerRepository employeeReportingManagerRepository;

    @Autowired
    ReportingManagerRepository reportingManagerRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    UserLeavesAuditRepository userLeavesAuditRepository;

//    @Autowired
//    UserLeaveBalanceRepository userLeaveBalanceRepository;

    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    public  Map<String,Object> applyForLeave(LeaveApplicationSaveRequest leaveApplicationRequest)
    {
        try {
              if(new Date().getTime() > leaveApplicationRequest.getStartDate().getTime() && new Date().getTime() > leaveApplicationRequest.getEndDate().getTime() )
              {
             return  applyForPastLeave(leaveApplicationRequest);

              }
              else{
                  HashMap <String, Object> map = new HashMap <>();
                  LeaveApplication leaveApplication = new LeaveApplication();
                  User user = userRepository.findOne(leaveApplicationRequest.getUserId());
                  PolicyMaster policyMaster = policyMasterRepository.findOne(leaveApplicationRequest.getPolicyMasterId());
                  LeaveType leaveType = leaveTypeRepository.findOne(leaveApplicationRequest.getLeaveTypeId());
                  ReportingManager reportingManager = reportingManagerRepository.findOne(leaveApplicationRequest.getReportingManagerId());

                  if(user != null && policyMaster != null && leaveType != null && reportingManager != null){
                      leaveApplication.setUser(user);
                      leaveApplication.setPolicyMaster(policyMaster);
                      leaveApplication.setLeaveType(leaveType);
                      leaveApplication.setReportingManager(reportingManager);
                      leaveApplication.setStatus("pending");
                      leaveApplication.setCreatedDate(new Date());
                      leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
                      leaveApplication.setEmployeeCommentSubmission(leaveApplicationRequest.getEmployeeCommentSubmission());
                      leaveApplication.setLeaveStartDate(leaveApplicationRequest.getStartDate());
                      leaveApplication.setLeaveEndDate(leaveApplicationRequest.getEndDate());
                      leaveApplication.setAppliedNoOfDays(leaveApplicationRequest.getNoOfDays());
                      leaveApplication.setEmployee(userRepository.findByUserId(leaveApplicationRequest.getUserId()));
                      LeaveApplication leaveApplication1= leaveApplicationRepository.save(leaveApplication);

                      // Email Notification

                      EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());

                      List<String> emailIds = new ArrayList<String>();
                      String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
                       emailIds.add(emailId1);
                      emailIds.add(user.getEmployee().getEmailId());
//                      EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//                      if(employeeReportingManager2 != null)
//                      {
//                          String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                          emailIds.add(emailId2);
//                      }

                     // notificationService.sendNotification(emailId1,emailIds,"Leave Application","\n"+user.getEmployee().getName()+" applied "+leaveApplication1.getLeaveType().getName() +" from "+formatDate(leaveApplication1.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),employeeReportingManager.getReportingManager().getEmployee().getName());

                      return ResponseJsonUtilNew.postSuccessResponseMessage( "Leave Application Submitted Successfully!!!" );
                  }
                  else{
                      return ResponseJsonUtilNew.entityNotFoundMessage( "User/PolicyMaster/LeaveType/ReportingManager doesn't exist" );
                  }
              }


        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }


    }

    public Map<String,Object> viewLeavesStatus(LeaveStatusRequest leaveStatusRequest)
    {
        try {
            List<LeaveApplication> leaves = leaveApplicationRepository.findByUserId(leaveStatusRequest.getId());
            List<ViewLeaveStatusResponse> leaveResponse =new ArrayList<>();

            for (Iterator<LeaveApplication> iter = leaves.iterator(); iter.hasNext(); ) {
                LeaveApplication leaveApplication = iter.next();
                ViewLeaveStatusResponse viewLeaveStatusResponse = new ViewLeaveStatusResponse();
                viewLeaveStatusResponse.setUserId(leaveApplication.getUser().getId());
                viewLeaveStatusResponse.setCreatedBy(leaveApplication.getCreatedBy());
                viewLeaveStatusResponse.setCreatedDate(leaveApplication.getCreatedDate());
                viewLeaveStatusResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
                viewLeaveStatusResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
                viewLeaveStatusResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
                viewLeaveStatusResponse.setStartDate(leaveApplication.getLeaveStartDate());
                viewLeaveStatusResponse.setEndDate(leaveApplication.getLeaveEndDate());
                viewLeaveStatusResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
                viewLeaveStatusResponse.setStatus(leaveApplication.getStatus());
                viewLeaveStatusResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
                viewLeaveStatusResponse.setLeaveApplicationId(leaveApplication.getId());
                viewLeaveStatusResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
                leaveResponse.add(viewLeaveStatusResponse);
            }

            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }

    }

    public Map<String,Object> viewLeavesByManager(ManagerLeaveViewRequest managerLeaveViewRequest)
    {
        try{
            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerId(managerLeaveViewRequest.getManagerId());

            List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();

            for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
                LeaveApplication leaveApplication = iter.next();
                ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
                viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
                viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
                viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
                viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
                viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
                viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
                viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
                viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
                viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
                viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
                viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
                viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
                viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
                leaveResponse.add(viewLeavesByManagerResponse);
            }


            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

    public  Map<String,Object> viewLeavesByManagerByDate(ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest)
    {
        try{
            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByReportingManagerIdByDate(managerLeaveViewByDateRequest.getManagerId(),managerLeaveViewByDateRequest.getStartDate(),managerLeaveViewByDateRequest.getEndDate());

            List<ViewLeavesByManagerResponse> leaveResponse =new ArrayList<>();

            for (Iterator<LeaveApplication> iter = leaveApplications.iterator(); iter.hasNext(); ) {
                LeaveApplication leaveApplication = iter.next();
                ViewLeavesByManagerResponse viewLeavesByManagerResponse = new ViewLeavesByManagerResponse();
                viewLeavesByManagerResponse.setUserId(leaveApplication.getUser().getId());
                viewLeavesByManagerResponse.setUserName(leaveApplication.getUser().getUsername());
                viewLeavesByManagerResponse.setCreatedBy(leaveApplication.getCreatedBy());
                viewLeavesByManagerResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
                viewLeavesByManagerResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
                viewLeavesByManagerResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
                viewLeavesByManagerResponse.setStartDate(leaveApplication.getLeaveStartDate());
                viewLeavesByManagerResponse.setEndDate(leaveApplication.getLeaveEndDate());
                viewLeavesByManagerResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
                viewLeavesByManagerResponse.setStatus(leaveApplication.getStatus());
                viewLeavesByManagerResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
                viewLeavesByManagerResponse.setLeaveApplicationId(leaveApplication.getId());
                viewLeavesByManagerResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
                leaveResponse.add(viewLeavesByManagerResponse);
            }


            return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }


    public Map<String,Object> approveLeaves(ApproveLeavesRequest approveLeavesRequest)
    {
        try{
            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveLeavesRequest.getLeaveApplicationId());
            // if(leaveApplication.getReportingManager().getId() == approveLeavesRequest.getReportingManagerId())
            //  {
            leaveApplication.setStatus("approved");
            leaveApplication.setReportingManagerCommentsApproval(approveLeavesRequest.getComment());
            leaveApplicationRepository.save(leaveApplication);

            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());

            List<String> emailIds = new ArrayList<String>();
            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//           notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"Leave Application Response","\n"+leaveApplication.getReportingManager().getEmployee().getName()+" approved " + leaveApplication.getLeaveType().getName() +" from "+formatDate(leaveApplication.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),leaveApplication.getUser().getEmployee().getName());

            return ResponseJsonUtilNew.postSuccessResponseMessage("leave approved Successfully!!!");

        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }


    public Map<String,Object> rejectLeaves(RejectLeavesRequest rejectLeavesRequest)
    {
        try{
            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(rejectLeavesRequest.getLeaveApplicationId());

            leaveApplication.setStatus("cancelled");
            leaveApplication.setEmployeeCommentsRejectionRequest(rejectLeavesRequest.getComment());
            leaveApplicationRepository.save(leaveApplication);


            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());

            List<String> emailIds = new ArrayList<String>();
            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
            emailIds.add(leaveApplication.getUser().getUsername());
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//          notificationService.sendNotification(emailId1,emailIds,"leave application cancelled","\n"+leaveApplication.getUser().getEmployee().getName()+" has cancelled the leave "+formatDate(leaveApplication.getLeaveStartDate())+ " to " +formatDate(leaveApplication.getLeaveEndDate()),leaveApplication.getReportingManager().getEmployee().getName());



            return ResponseJsonUtilNew.postSuccessResponseMessage(" leave cancelled Successfully!!!");
        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given id not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

//    public Map<String,Object> rejectionLeavesApproval(ApproveRejectionLeavesRequest approveRejectionLeavesRequest)
//    {
//        try{
//            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(approveRejectionLeavesRequest.getLeaveApplicatioId());
////            if(leaveApplication.getReportingManager().getId() == approveRejectionLeavesRequest.getReportingManagerId())
////            {
//            leaveApplication.setStatus("LeaveRejectioApproved");
//            leaveApplication.setReportingManagerCommentsRejectionApproval(approveRejectionLeavesRequest.getComment());
//            leaveApplicationRepository.save(leaveApplication);
//            return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejection approved Successfully!!!");
//
////            }
////            else
////            {
////                return ResponseJsonUtilNew.postSuccessResponseMessage("invalid reporting manager id");
////            }
//        }
//        catch(Exception e)
//        {
//            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//        }
//    }



    public Map<String,Object> editAppliedLeave(EditAppliedLeaveRequest editAppliedLeaveRequest)
    {


        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = formatter.parse(editAppliedLeaveRequest.getStartDate());
            Date endDate = formatter.parse(editAppliedLeaveRequest.getEndDate());

            if(startDate.getTime()<new Date().getTime() )
            {
                return ResponseJsonUtilNew.postSuccessResponseMessage("start date should be greater then present date");

            }
            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(editAppliedLeaveRequest.getLeaveApplicationId());
            PolicyMaster policyMaster = policyMasterRepository.findOne(editAppliedLeaveRequest.getPolicyMasterId());

            leaveApplication.setLeaveStartDate(startDate);
                leaveApplication.setLeaveEndDate(endDate);
                leaveApplication.setAppliedNoOfDays(editAppliedLeaveRequest.getNoOfDays());
                leaveApplication.setEmployeeCommentSubmission(editAppliedLeaveRequest.getComment());
                leaveApplication.setPolicyMaster(policyMaster);
                leaveApplication.setModifiedBy(editAppliedLeaveRequest.getModifiedBy());
                leaveApplication.setModifiedDate( new Date() );
                leaveApplication.setUser( userRepository.findOne(editAppliedLeaveRequest.getUserId()) );
            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());

                List<String> emailIds = new ArrayList<String>();
                String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
                emailIds.add(leaveApplication.getUser().getUsername());
//                EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//                if(employeeReportingManager2 != null)
//                {
//                    String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                    emailIds.add(emailId2);
//                }
//                notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+" edited the leave");
//            notificationService.sendNotification(emailId1,emailIds,"leave application edited","\n"+leaveApplication.getUser().getEmployee().getName()+" has edited "+leaveApplication.getLeaveType().getName()+" from "+formatDate(leaveApplication.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),leaveApplication.getReportingManager().getEmployee().getName());

                leaveApplicationRepository.save( leaveApplication);
                            return ResponseJsonUtilNew.postSuccessResponseMessage("updated sucessfully");


        }
        catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

      }



    public Map<String,Object> managerRejectLeaves(ManagerRejectLeavesRequest managerRejectLeavesRequest)
    {
        try{
            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(managerRejectLeavesRequest.getLeaveApplicationId());

            leaveApplication.setStatus("rejected");
            leaveApplication.setReportingManagerCommentsApproval(managerRejectLeavesRequest.getComment());
            leaveApplicationRepository.save(leaveApplication);

            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());

            List<String> emailIds = new ArrayList<String>();
            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
            emailIds.add(emailId1);
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//            notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"leave application",leaveApplication.getUser().getUsername()+"'s leave rejected");
//           notificationService.sendNotification(leaveApplication.getUser().getUsername(),emailIds,"Leave Application Response","\n"+leaveApplication.getReportingManager().getEmployee().getName()+" rejected " + leaveApplication.getLeaveType().getName() +" from "+formatDate(leaveApplication.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),leaveApplication.getUser().getEmployee().getName());



            return ResponseJsonUtilNew.postSuccessResponseMessage("leave rejected!!!");

        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

    //employee withdraws leaves

    public Map<String,Object> withdrawLeaves(WithdrawLeavesRequest withdrawLeavesRequest)
    {
        try{
            LeaveApplication leaveApplication = leaveApplicationRepository.findOne(withdrawLeavesRequest.getLeaveApplicationId());

            leaveApplication.setStatus( "withdrawn" );
            leaveApplication.setWithdrawalStatus("YES");
            leaveApplication.setWithdrawalDate( new Date(  ) );
            leaveApplication.setWithdrawalComment(withdrawLeavesRequest.getWithdrawalComment());
            leaveApplicationRepository.save(leaveApplication);

            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(leaveApplication.getUser().getEmployee().getId());

            List<String> emailIds = new ArrayList<String>();
            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
            emailIds.add(leaveApplication.getEmployee().getEmailId());
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//           notificationService.sendNotification(emailId1,emailIds,"leave application withdrawn","\n"+leaveApplication.getUser().getEmployee().getName()+" has withdrawn "+leaveApplication.getLeaveType().getName()+" from "+formatDate(leaveApplication.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),leaveApplication.getReportingManager().getEmployee().getName());


            return ResponseJsonUtilNew.postSuccessResponseMessage("leave withdrawn!!!");

        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

//public Map<String,Object> getEmployeeLOPInfo(EmployeeLOPInfoRequest employeeLOPInfoRequest)
//{
//    try{
//        //get the 6 months before date start
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MONTH, -6);
//        calendar.set(Calendar.DAY_OF_MONTH, 1);
//        Date date = calendar.getTime();
//        //end
//
//        List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByStatusAndEmployeeId(employeeLOPInfoRequest.getUserId(),date);
//        List<ViewLeaveStatusResponse> leaveResponse =new ArrayList<>();
//        for (LeaveApplication leaveApplication:leaveApplications)
//        {
//            ViewLeaveStatusResponse viewLeaveStatusResponse = new ViewLeaveStatusResponse();
//            viewLeaveStatusResponse.setNoOfDays(leaveApplication.getAppliedNoOfDays());
//            viewLeaveStatusResponse.setStatus(leaveApplication.getStatus());
//            viewLeaveStatusResponse.setStartDate(leaveApplication.getLeaveStartDate());
//            viewLeaveStatusResponse.setEndDate(leaveApplication.getLeaveEndDate());
//            viewLeaveStatusResponse.setPolicyMasterName(leaveApplication.getPolicyMaster().getName());
//            viewLeaveStatusResponse.setLeaveTypeName(leaveApplication.getLeaveType().getName());
//            viewLeaveStatusResponse.setReportingManagerId(leaveApplication.getReportingManager().getId());
//            viewLeaveStatusResponse.setEmployeeCommentSubmission(leaveApplication.getEmployeeCommentSubmission());
//            viewLeaveStatusResponse.setReportingManagerName(leaveApplication.getReportingManager().getEmployee().getName());
//            viewLeaveStatusResponse.setLeaveApplicationId(leaveApplication.getId());
//            viewLeaveStatusResponse.setCreatedBy(leaveApplication.getCreatedBy());
//            viewLeaveStatusResponse.setUserId(leaveApplication.getUser().getId());
//            leaveResponse.add(viewLeaveStatusResponse);
//        }
//        return ResponseJsonUtilNew.getSuccessResponseMessage("leaves Fetched Successfully", leaveResponse);
//
//    }
//    catch(Exception e)
//    {
//        return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
//    }
//}


    // below api's to be included in the scheduler


    public Map<String,Object> statusChangetoConsumptionStarted()
    {
        try{
            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByApprovedStatusAndCurrentDate("approved",new Date());

            for (LeaveApplication leaveApplication:leaveApplications) {

                leaveApplication.setStatus( "consumption started" );
                leaveApplication.setWithdrawalStatus( "NO" );
                leaveApplication.setModifiedDate( new Date(  ) );
                leaveApplicationRepository.save( leaveApplication );

            }

            return ResponseJsonUtilNew.postSuccessResponseMessage("leave consumption started status updated!!!");

        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }


    public Map<String,Object> consumptionStartedLeaveApplicationsJob()
    {
        try{
            List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByConsumptionStartedStatusAndCurrentDate("consumption started","NO",new Date());

            for (LeaveApplication leaveApplication:leaveApplications) {

                UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
                userLeavesAudit.setDebit( 1.0 );
                userLeavesAudit.setCredit( 0.0 );
                userLeavesAudit.setLeaveType( leaveApplication.getLeaveType());
                userLeavesAudit.setPolicyMaster( leaveApplication.getPolicyMaster() );
                userLeavesAudit.setUser( leaveApplication.getUser() );
                userLeavesAudit.setCreatedDate( new Date(  ) );
                userLeavesAuditRepository.save( userLeavesAudit );

            }

            return ResponseJsonUtilNew.postSuccessResponseMessage("leave trails included!!!");

        }
        catch(NullPointerException e)
        {
            return ResponseJsonUtilNew.entityNotFoundMessage("given ids not found");
        }
        catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage(e.getLocalizedMessage());
        }
    }

    public String formatDate(Date date)
    {
        //Date myDate = new Date();
        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sd1.format(date);
//        System.out.println(s);
        return s;
    }

    public  Map<String,Object> applyForPastLeave(LeaveApplicationSaveRequest leaveApplicationRequest)
    {
        HashMap <String, Object> map = new HashMap <>();
        LeaveApplication leaveApplication = new LeaveApplication();
        User user = userRepository.findOne(leaveApplicationRequest.getUserId());
        PolicyMaster policyMaster = policyMasterRepository.findOne(leaveApplicationRequest.getPolicyMasterId());
        LeaveType leaveType = leaveTypeRepository.findOne(leaveApplicationRequest.getLeaveTypeId());
        ReportingManager reportingManager = reportingManagerRepository.findOne(leaveApplicationRequest.getReportingManagerId());

        if(user != null && policyMaster != null && leaveType != null && reportingManager != null){
            leaveApplication.setUser(user);
            leaveApplication.setPolicyMaster(policyMaster);
            leaveApplication.setLeaveType(leaveType);
            leaveApplication.setReportingManager(reportingManager);
            leaveApplication.setStatus("consumed fully");
            leaveApplication.setCreatedDate(new Date());
            leaveApplication.setCreatedBy(leaveApplicationRequest.getCreatedBy());
            leaveApplication.setEmployeeCommentSubmission(leaveApplicationRequest.getEmployeeCommentSubmission());
            leaveApplication.setLeaveStartDate(leaveApplicationRequest.getStartDate());
            leaveApplication.setLeaveEndDate(leaveApplicationRequest.getEndDate());
            leaveApplication.setAppliedNoOfDays(leaveApplicationRequest.getNoOfDays());
            leaveApplication.setEmployee(userRepository.findByUserId(leaveApplicationRequest.getUserId()));
            leaveApplication.setWithdrawalStatus("NA");
//            leaveApplication.setModifiedDate(new Date());
            LeaveApplication leaveApplication1= leaveApplicationRepository.save(leaveApplication);

            for(int i=0;i<leaveApplicationRequest.getNoOfDays();i++)
            {
                UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
                userLeavesAudit.setDebit( 1.0 );
                userLeavesAudit.setCredit( 0.0 );
                userLeavesAudit.setLeaveType( leaveApplication1.getLeaveType());
                userLeavesAudit.setPolicyMaster( leaveApplication1.getPolicyMaster() );
                userLeavesAudit.setUser( leaveApplication1.getUser() );
                userLeavesAudit.setCreatedDate( new Date(  ) );
                userLeavesAuditRepository.save( userLeavesAudit );
            }

            // Email Notification

            EmployeeReportingManager employeeReportingManager = employeeReportingManagerRepository.findByEmployeeId(user.getEmployee().getId());

            List<String> emailIds = new ArrayList<String>();
            String  emailId1 = employeeReportingManager.getReportingManager().getEmployee().getEmailId();
             emailIds.add(emailId1);
            emailIds.add(user.getEmployee().getEmailId());
//            EmployeeReportingManager employeeReportingManager2 =  employeeReportingManagerRepository.findByEmployeeId(employeeReportingManager.getReportingManager().getEmployee().getId());
//            if(employeeReportingManager2 != null)
//            {
//                String emailId2 = employeeReportingManager2.getReportingManager().getEmployee().getEmailId();
//                emailIds.add(emailId2);
//            }
//
//            notificationService.sendNotification(emailId1,emailIds,"Leave Application","\n"+user.getEmployee().getName()+" applied "+leaveApplication1.getLeaveType().getName() +" from "+formatDate(leaveApplication1.getLeaveStartDate())+" to "+formatDate(leaveApplication.getLeaveEndDate()),employeeReportingManager.getReportingManager().getEmployee().getName());


            return ResponseJsonUtilNew.postSuccessResponseMessage( "Leave Application Submitted Successfully!!!" );
        }
        else{
            return ResponseJsonUtilNew.entityNotFoundMessage( "User/PolicyMaster/LeaveType/ReportingManager doesn't exist" );
        }
    }



}
