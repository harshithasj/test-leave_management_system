package com.hashworks.lms.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajeevkumarsingh on 19/08/17.
 */
@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

//    public String generateToken(Authentication authentication) {
//
//        UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();
//        System.out.println( userDetails.getName() );//added to get name
//        System.out.println( userDetails.getAuthorities() ); // added to get role
//        Date now = new Date();
//        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);
//
//        return Jwts.builder()
//                .setSubject(Long.toString(userDetails.getId()))
//                .setIssuedAt(new Date())
//                .setExpiration(expiryDate)
//                .signWith(SignatureAlgorithm.HS512, jwtSecret)
//                .compact();
//    }


    public Map<String,Object> generateToken(Authentication authentication) {

        UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();
        System.out.println( userDetails.getUsername() );//added to get name
        System.out.println( userDetails.getAuthorities() ); // added to get role
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        String token =  Jwts.builder()
                .setSubject(Long.toString(userDetails.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith( SignatureAlgorithm.HS512, jwtSecret)
                .compact();

        Long userid = userDetails.getId();
        String username = userDetails.getUsername();
        Collection <? extends GrantedAuthority> role = userDetails.getAuthorities();

        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", true );
        map.put( "statusCode", HttpStatus.OK.value() ); //400 Bad Request
        map.put( "statusMessage", "Login Success" );
        map.put( "token",token );
        map.put(  "userid",userid);
        map.put("role",role);
        map.put("username",username);

        return map;
    }
    public Map<String,Object> generateToken(Authentication authentication,String firstName) {

        UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();
        System.out.println( userDetails.getUsername() );//added to get name
        System.out.println( userDetails.getAuthorities() ); // added to get role
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        String token =  Jwts.builder()
                .setSubject(Long.toString(userDetails.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith( SignatureAlgorithm.HS512, jwtSecret)
                .compact();

        Long userid = userDetails.getId();
        String username = userDetails.getUsername();
        Collection <? extends GrantedAuthority> role = userDetails.getAuthorities();

        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", true );
        map.put( "statusCode", HttpStatus.OK.value() ); //400 Bad Request
        map.put( "statusMessage", "Login Success" );
        map.put( "token",token );
        map.put(  "userid",userid);
        map.put("role",role);
        map.put("username",username);
        map.put("name",firstName);

        return map;
    }


    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}
