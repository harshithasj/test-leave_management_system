package com.hashworks.lms.dto.request;

import com.hashworks.lms.model.Employee;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Data
public class ReportingManagerSaveRequest {


    @Setter
    @Getter
    @NotNull
    private Long employeeId;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String createdBy;

}
