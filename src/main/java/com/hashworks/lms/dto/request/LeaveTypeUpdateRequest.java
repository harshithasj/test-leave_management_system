package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.bytebuddy.implementation.bind.annotation.Empty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class LeaveTypeUpdateRequest {

    @Getter
    @Setter
    @NotNull
    private Long id;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String name;

    @Getter
    @Setter
    @NotNull
    private Boolean enabled;

//    @Getter
//    @Setter
//    @NotNull
//    private String createdBy;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String modifiedBy;
}
