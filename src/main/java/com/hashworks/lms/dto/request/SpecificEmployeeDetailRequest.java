package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
public class SpecificEmployeeDetailRequest {

    @Getter
    @Setter
    private Long employeeId;

    @Getter
    @Setter
    private Long userId;
}
