package com.hashworks.lms.dto.request;

import com.hashworks.lms.model.Employee;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class EmployeeFeedbackRequest {

    @Setter
    @Getter
    @NotNull
    private String emailId;

    @Setter
    @Getter
    @NotNull
    private String  feedback;


}
