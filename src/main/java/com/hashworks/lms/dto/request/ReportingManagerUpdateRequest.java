package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;


@NoArgsConstructor
@Data
public class ReportingManagerUpdateRequest {


    @Getter
    @Setter
    @NotNull
    private Long reportingManagerId;


    @Setter
    @Getter
    @NotNull
    private Boolean enabled;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String modifiedBy;

}
