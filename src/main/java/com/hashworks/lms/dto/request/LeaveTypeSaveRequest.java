package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class LeaveTypeSaveRequest {

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String name;


    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String createdBy;

//    @Getter
//    @Setter
//    @NotNull
//    private String modifiedBy;

}
