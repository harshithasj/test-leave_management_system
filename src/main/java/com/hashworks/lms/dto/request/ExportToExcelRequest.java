package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class ExportToExcelRequest {

    @Getter
    @Setter
    @NotNull
    private Date startDate;

    @Getter
    @Setter
    @NotNull
    private Date endDate;
}
