package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class LeaveTypeDeleteRequest {

    @Getter
    @Setter
    @NotNull
    private Long id;
}
