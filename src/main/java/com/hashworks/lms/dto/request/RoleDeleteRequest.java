package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Data
public class RoleDeleteRequest {

    @Getter
    @Setter
    @NotNull
    private Long id;

}
