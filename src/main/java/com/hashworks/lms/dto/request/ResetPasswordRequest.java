package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ResetPasswordRequest {
    @Setter
    @Getter
    @NotNull
    private String userName;

    @Setter
    @Getter
    @NotNull
    private String password;

    @Setter
    @Getter
    @NotNull
    private String oldPassword;

}
