package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ApproveRejectionLeavesRequest {

    @Getter
    @Setter
    @NotNull
    private Long leaveApplicatioId;

    @Getter
    @Setter
    @NotNull
    private Long reportingManagerId;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String comment;
}
