package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class GetEmployeeDetailsRequest {

    private Date startDate;

    private Date endDate;
}
