package com.hashworks.lms.dto.request;

import com.hashworks.lms.model.LeaveType;
import com.hashworks.lms.model.PolicyMaster;
import com.hashworks.lms.model.enumeration.LeavePolicyType;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class PolicySaveRequest {

    @Setter
    @Getter
    @NotNull
    private String year;

    @Setter
    @Getter
    @NotNull
    private String name;

    @Setter
    @Getter
    @NotNull
    private LeavePolicyType leavePolicyType;

    @Setter
    @Getter
    @NotNull
    private List<PolicyDetailMasterSaveRequest> policyDetailMasterSaveRequests;

    @Setter
    @Getter
    @NotNull
    private String createdBy;


}
