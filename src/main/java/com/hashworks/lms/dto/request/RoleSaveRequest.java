package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Data
public class RoleSaveRequest {


    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String roleType;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String createdBy;


}
