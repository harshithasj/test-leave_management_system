package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

@NoArgsConstructor
@Data
public class LeaveApplicationSaveRequest {

    @Getter
    @Setter
    @NotNull
    private Long userId;

    @Getter
    @Setter
    @NotNull
    private Long policyMasterId;

    @Getter
    @Setter
    @NotNull
    private Long leaveTypeId;

    @Getter
    @Setter
    @NotNull
    private Date startDate;

    @Getter
    @Setter
    @NotNull
    private Date endDate;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String employeeCommentSubmission;


    @Setter
    @Getter
    @NotNull
    private Long reportingManagerId;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String createdBy;

    @Getter
    @Setter
    @NotNull
    private Double noOfDays;
}
