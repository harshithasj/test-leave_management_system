package com.hashworks.lms.dto.request;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class SignUpRequest {

    @Setter
    @Getter
    @NotNull
    private String badgeNumber;

    @Setter
    @Getter
    @NotNull
    private String createdBy;

    @Setter
    @Getter
    @NotNull
    @Email
    private String email;

    @Setter
    @Getter
    @NotNull
    private String mobileNumber;

    @Setter
    @Getter
    @NotNull
    private String name;

    @Setter
    @Getter
    @NotNull
    private Long roleId;
}
