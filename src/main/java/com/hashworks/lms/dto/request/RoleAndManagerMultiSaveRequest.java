package com.hashworks.lms.dto.request;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Data
public class RoleAndManagerMultiSaveRequest {

    @Setter
    @Getter
    @NotNull
    private List<EmployeeRoleSaveRequest> employeeRoleSaveRequests;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String createdBy;

}
