package com.hashworks.lms.dto.request;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
public class ReportingManagerMultiSaveRequest {

    @Setter
    @Getter
    @NotNull
    private List<Long> employeeIds;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String createdBy;

}
