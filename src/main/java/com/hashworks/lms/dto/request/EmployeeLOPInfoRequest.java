package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class EmployeeLOPInfoRequest {

    @Setter
    @Getter
    @NotNull
    private Long userId;
}
