package com.hashworks.lms.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class UserLeaveDetailsRequest {

    @Getter
    @Setter
    @NotNull
    private Long userId;

    @Getter
    @Setter
    @NotNull
    private Long policyMasterId;


}
