package com.hashworks.lms.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

public class DisableEmployeeRequest {

    @NotNull
    @Setter
    @Getter
    private Long employeeId;



}
