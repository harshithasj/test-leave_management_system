package com.hashworks.lms.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Data
public class HolidaySaveRequest {


    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String name;

    @Setter
    @Getter
    @NotNull
    private Date date;



    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String createdBy;

}
