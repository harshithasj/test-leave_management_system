package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class WithdrawLeavesRequest {

    @Getter
    @Setter
    @NotNull
    private Long leaveApplicationId;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String withdrawalComment;
}
