package com.hashworks.lms.dto.request;

import com.hashworks.lms.model.LeaveType;
import com.hashworks.lms.model.PolicyMaster;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class PolicyDetailMasterSaveRequest {



    @Setter
    @Getter
    @NotNull
    private Long id;//leavetypeid

    @Setter
    @Getter
    @NotNull
    private Long totalNoOfDays; //days

    @Setter
    @Getter
    @NotNull
    private Boolean carryForwardAllowed;//carryforward allowed


    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String name;//leavetypename


}
