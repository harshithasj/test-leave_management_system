package com.hashworks.lms.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@Data
public class HolidayUpdateRequest {

    @Getter
    @Setter
    @NotNull
    private Long id;

    @Setter
    @Getter
    @NotNull
    private String name;

    @Setter
    @Getter
    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date date;

    @Setter
    @Getter
    @NotNull
    private Boolean enabled;

    @Setter
    @Getter
    @NotNull
    @NotEmpty
    private String modifiedBy;

}
