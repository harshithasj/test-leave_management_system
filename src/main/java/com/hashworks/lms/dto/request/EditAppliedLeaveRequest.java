package com.hashworks.lms.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class EditAppliedLeaveRequest {

    @Setter
    @Getter
    @NotNull
    private Long leaveApplicationId;

    @Setter
    @Getter
    @NotNull
    private Long leaveTypeId;

    @Setter
    @Getter
    @NotNull
    private String startDate;

    @Setter
    @Getter
    @NotNull
    private String endDate;

    @Setter
    @Getter
    @NotNull
    private String comment;

    @Getter
    @Setter
    @NotNull
    private Long policyMasterId;

    @Setter
    @Getter
    @NotNull
    private Long reportingManagerId;

    @Getter
    @Setter
    @NotNull
    @NotEmpty
    private String modifiedBy;

    @Getter
    @Setter
    @NotNull
    private Double noOfDays;


    @Getter
    @Setter
    @NotNull
    private Long userId;

}
