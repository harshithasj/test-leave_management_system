package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@Data
@NoArgsConstructor
public class LeaveBalanceResponse {

    @Getter
    @Setter
    private String leaveType;

    @Getter
    @Setter
    private Double leavebalance;

    @Getter
    @Setter
    private Double leavebalanceFromPreviousYear;


}
