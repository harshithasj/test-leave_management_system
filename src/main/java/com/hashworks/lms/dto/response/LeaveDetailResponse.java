package com.hashworks.lms.dto.response;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;

@Data
@NoArgsConstructor
public class LeaveDetailResponse {

    @Getter
    @Setter
    private Long leaveTypeId;

    @Getter
    @Setter
    private String leaveType;

    @Getter
    @Setter
    private Double credited ;

    @Getter
    @Setter
    private Double consumed ;

    @Getter
    @Setter
    private Double available;


    @Getter
    @Setter
    private Double applied ;


    @Getter
    @Setter
    private Double approved ;

}
