package com.hashworks.lms.dto.response;

import com.hashworks.lms.model.enumeration.LeavePolicyType;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Data
@NoArgsConstructor
public class UserLeaveDetailsResponse {


    @Setter
    @Getter
    private Long userId;

    @Setter
    @Getter
    private List<LeaveDetailResponse> leaveDetailResponses;


}
