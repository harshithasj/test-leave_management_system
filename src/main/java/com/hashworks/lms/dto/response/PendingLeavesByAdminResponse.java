package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Data
@NoArgsConstructor
public class PendingLeavesByAdminResponse {

    @Getter
    @Setter
    private String employeeName;

    @Getter
    @Setter
    private String reportingManagerName;

    @Getter
    @Setter
    private String leaveType;

    @Setter
    @Getter
    private Date startDate;

    @Setter
    @Getter
    private Date endDate;

    @Setter
    @Getter
    private String status;
}
