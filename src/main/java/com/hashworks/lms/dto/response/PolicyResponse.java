package com.hashworks.lms.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hashworks.lms.model.enumeration.LeavePolicyType;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class PolicyResponse {

    @Setter
    @Getter
    private Long policyId;

    @Setter
    @Getter
    private String policyYear;

    @Setter
    @Getter
    private String policyName;

    @Setter
    @Getter
    private LeavePolicyType leavePolicyType;

    @Setter
    @Getter
    private List<PolicyDetailMasterResponse> policyDetailMasterResponses;

    @Setter
    @Getter
    private Boolean enabled;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    //@Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    //@Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date modifiedDate;

}
