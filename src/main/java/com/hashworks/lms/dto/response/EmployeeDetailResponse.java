package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@NoArgsConstructor
@Data
public class EmployeeDetailResponse {

    @Getter
    @Setter
    private BigInteger employeeId;

    @Getter
    @Setter
    private Date dob;

    @Getter
    @Setter
    private String emailId;

    @Getter
    @Setter
    private String mobileNumber;

    @Getter
    @Setter
    private String employeeName;

    @Getter
    @Setter
    private BigInteger userId;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private BigInteger roleId;

    @Getter
    @Setter
    private String roleName;

    @Getter
    @Setter
    private BigInteger reportingManagerEmployeeId;

    @Getter
    @Setter
    private String reportingManagerName;

//    @Getter
//    @Setter
//    private String username;
//
//    @Getter
//    @Setter
//    private String role;
//
//    @Getter
//    @Setter
//    private Long reportingManagerEmployeeId;



//    public EmployeeDetailResponse(Long employeeId,Date dob,String reportingManagerName){
//        this.employeeId = employeeId;
//        this.dob = dob;
//        this.reportingManagerName = reportingManagerName;
//    }
}
