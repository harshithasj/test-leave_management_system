package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class PolicyDetailMasterResponse {

    @Getter
    @Setter
    private Long policyDetailId;

    @Setter
    @Getter
    private Long totalNoOfDays; //days

    @Setter
    @Getter
    private Long leavetypeId;//leavetypeid

    @Setter
    @Getter
    private String leavetypeName;//leavetypename

    @Setter
    @Getter
    private Boolean carryForwardAllowed;//carryforward allowed


}
