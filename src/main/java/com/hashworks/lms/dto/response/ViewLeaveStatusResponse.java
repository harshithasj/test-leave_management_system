package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class ViewLeaveStatusResponse {

    @Getter
    @Setter
    private long userId;

    @Getter
    @Setter
    private String policyMasterName;

    @Getter
    @Setter
    private String leaveTypeName;

    @Getter
    @Setter
    private Date StartDate;

    @Getter
    @Setter
    private Date endDate;

    @Setter
    @Getter
    private String employeeCommentSubmission;


    @Setter
    @Getter
    private String reportingManagerName;

    @Setter
    @Getter
    private double noOfDays;

    @Getter
    @Setter
    private String createdBy;

    @Getter
    @Setter
    private Date createdDate;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private long leaveApplicationId;

    @Getter
    @Setter
    private long ReportingManagerId;

}
