package com.hashworks.lms.dto.response;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class EmployeeLeaveBalanceResponse {

    @Getter
    @Setter
    private BigInteger employeeId;

    @Getter
    @Setter
    private Date dob;

    @Getter
    @Setter
    private String emailId;

    @Getter
    @Setter
    private String mobileNumber;

    @Getter
    @Setter
    private String employeeName;

    @Getter
    @Setter
    private BigInteger userId;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private BigInteger roleId;

    @Getter
    @Setter
    private String badgeNumber;

    @Getter
    @Setter
    private String roleName;

    @Getter
    @Setter
    private BigInteger reportingManagerEmployeeId;

    @Getter
    @Setter
    private BigInteger reportingManagerId;

    @Getter
    @Setter
    private String reportingManagerName;
//
//    @Setter
//    @Getter
//    private List<LeaveBalanceResponse> leaveBalanceResponses;

    @Setter
    @Getter
    private List<LeaveDetailResponse> leaveDetailsResponses;
}
