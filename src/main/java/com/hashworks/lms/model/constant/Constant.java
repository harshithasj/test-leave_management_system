package com.hashworks.lms.model.constant;

import java.io.Serializable;

public final class Constant implements Serializable {
    private static final long serialVersionUID = -1L;
    public static final String WELCOME_MESSAGE = "Welcome to Leave Management System API";
    public static final String AUTHENTICATED_USER = "authenticatedUser";
    public static final String STATUS = "status";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String ERRORS = "errors";
    public static final String ID = "id";
    public static final String DATA = "data";
    public static final String USER_NOT_FOUND = "UserNotFound";
    public static final String INTERNAL_ERROR = "internalError";
    public static final String MESSAGE = "message";
    public static final String AUTHORISED_USER = "authorisedUser";
}