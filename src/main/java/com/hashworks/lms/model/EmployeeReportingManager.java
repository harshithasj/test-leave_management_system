package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
public class EmployeeReportingManager {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "reporting_manager_id", nullable = false)
    private ReportingManager reportingManager;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date modifiedDate;


}
