package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class PolicyMasterHolidayListMaster {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "policy_master_id")
    private PolicyMaster policyMaster;

    @Setter
    @Getter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "holiday_list_master_id")
    private HolidayListMaster holidayListMaster;

    @Setter
    @Getter
    private String location;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PolicyMasterHolidayListMaster)) return false;
        PolicyMasterHolidayListMaster that = (PolicyMasterHolidayListMaster) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(policyMaster, that.policyMaster) &&
                Objects.equals(holidayListMaster, that.holidayListMaster) &&
                Objects.equals(location, that.location);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, policyMaster, holidayListMaster, location);
    }

    @Override
    public String toString() {
        return "PolicyMasterHolidayListMaster{" +
                "id=" + id +
                ", policyMaster=" + policyMaster +
                ", holidayListMaster=" + holidayListMaster +
                ", location='" + location + '\'' +
                '}';
    }
}
