package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class EmployeeFeedback {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name ="employee_id")
    private Employee employee;

    @Getter
    @Setter
    private String employeeName;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name ="feedBack_id")
    private Feedback feedBack;
}
