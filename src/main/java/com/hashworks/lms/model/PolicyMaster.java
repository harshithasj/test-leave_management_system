package com.hashworks.lms.model;

import com.hashworks.lms.model.enumeration.LeavePolicyType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class PolicyMaster {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String year;

    @Setter
    @Getter
    private Boolean enabled;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    private Date createdDate = new Date();

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    private Date modifiedDate = new Date();

    @Setter
    @Getter
    private LeavePolicyType leavePolicyType;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof PolicyMaster)) return false;
//        PolicyMaster that = (PolicyMaster) o;
//        return enabled == that.enabled &&
//                Objects.equals(id, that.id) &&
//                Objects.equals(name, that.name) &&
//                Objects.equals(year, that.year) &&
//                leavePolicyType == that.leavePolicyType;
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, name, year, enabled, leavePolicyType);
//    }
//
//    @Override
//    public String toString() {
//        return "PolicyMaster{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", year='" + year + '\'' +
//                ", enabled=" + enabled +
//                ", createdBy='" + createdBy + '\'' +
//                ", createdDate=" + createdDate +
//                ", modifiedBy='" + modifiedBy + '\'' +
//                ", modifiedDate=" + modifiedDate +
//                ", leavePolicyType=" + leavePolicyType +
//                '}';
//    }
}
