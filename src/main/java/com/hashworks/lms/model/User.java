package com.hashworks.lms.model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
public class User {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Getter
//    @Setter
//    private Long id;
//
//    @Getter
//    @Setter
//    private String name;
//
//    @Getter
//    @Setter
//    private String email;
//
//    @Getter
//    @Setter
//    private String password;
//
//    @Getter
//    @Setter
//    private String oauthId;
//
//    @Getter
//    @Setter
//    private boolean authorised = false;
//
//    @Getter
//    @Setter
//    private String employeeId;
//
//    @Getter
//    @Setter
//    private String mobile;
//
//    @Getter
//    @Setter
//    private String address;
//
//    @Getter
//    @Setter
//    private Date createdDate = new Date();
//
//    @Getter
//    @Setter
//    private Date updatedDate = new Date();
//
//    @Getter
//    @Setter
//    private Date joiningDate = new Date();
//
//    @Getter
//    @Setter
//    @ManyToOne
//    @JoinColumn(name = "role_id")
//    private Role role;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private Boolean enabled;


    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name ="employee_id")
    private Employee employee;


    @Getter
    @Setter
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;


//    @Getter
//    @Setter
//    @OneToOne(cascade = CascadeType.MERGE)
//    @JoinColumn(name = "role_id")
//    private Role role;


    @Getter
    @Setter
    private String authId;

    @Getter
    @Setter
    private boolean authorised = false;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof User)) return false;
//        User user = (User) o;
//        return enabled == user.enabled &&
//                authorised == user.authorised &&
//                Objects.equals(id, user.id) &&
//                Objects.equals(username, user.username) &&
//                Objects.equals(password, user.password) &&
//                Objects.equals(roles, user.roles);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, username, password, enabled, roles, authorised);
//    }
//
//    @Override
//    public String toString() {
//        return "Users{" +
//                "id=" + id +
//                ", username='" + username + '\'' +
//                ", password='" + password + '\'' +
//                ", enabled=" + enabled +
//                ", employee=" + employee +
//                ", role=" + roles +
//                ", authId='" + authId + '\'' +
//                ", authorised=" + authorised +
//                '}';
//    }

    public User() {
    }

    public User(String username, String password, Boolean enabled) {
//        this.enabled = user.get();
//        this.email = users.getEmail();
        this.username = username;
        this.password = password;
        this.enabled = enabled;

    }
}
