package com.hashworks.lms.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class HolidayListMaster {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date date;

    @Setter
    @Getter
    private Boolean enabled;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date modifiedDate;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof HolidayListMaster)) return false;
//        HolidayListMaster that = (HolidayListMaster) o;
//        return enabled == that.enabled &&
//                Objects.equals(id, that.id) &&
//                Objects.equals(name, that.name) &&
//                Objects.equals(date, that.date);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, name, date, enabled);
//    }
//
//    @Override
//    public String toString() {
//        return "HolidayListMaster{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", date=" + date +
//                ", enabled=" + enabled +
//                ", createdBy='" + createdBy + '\'' +
//                ", createdDate=" + createdDate +
//                ", modifiedBy='" + modifiedBy + '\'' +
//                ", modifiedDate=" + modifiedDate +
//                '}';
//    }
}
