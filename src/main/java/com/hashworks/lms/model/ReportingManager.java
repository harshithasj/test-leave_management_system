package com.hashworks.lms.model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "reporting_manager")
public class ReportingManager {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    @Setter
    @Getter
    @JoinColumn(name = "employee_id")
    @OneToOne(cascade = CascadeType.MERGE)
    private Employee employee;

    @Getter
    @Setter
    private Boolean enabled;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date modifiedDate;


}
