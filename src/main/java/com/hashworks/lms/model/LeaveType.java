package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "leave_type")
public class LeaveType {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    @NotNull
    private String createdBy;

    @Setter
    @Getter
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    private Date modifiedDate;

    @Setter
    @Getter
    private Boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LeaveType)) return false;
        LeaveType leaveType = (LeaveType) o;
        return Objects.equals(id, leaveType.id) &&
                Objects.equals(name, leaveType.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "LeaveType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", modifiedDate=" + modifiedDate +
                ", enabled=" + enabled +
                '}';
    }
}
