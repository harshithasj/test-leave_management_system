package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Getter
    @Setter
    private String name;

    @Setter
    @Getter
    private Date  dob;


    @Setter
    @Getter
    private String badgeNumber;

    @Getter
    @Setter
    private String emailId;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    private Date modifiedDate;

    @Setter
    @Getter
    private String mobileNumber;

    @Setter
    @Getter
    private String address;

    @Setter
    @Getter
    private Boolean enabled;

//    @Setter
//    @Getter
//    private Boolean newJoin;
//
//    @Getter
//    @Setter
//    private Date doj;
//
//

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Employee)) return false;
//        Employee employee = (Employee) o;
//        return id == employee.id &&
//                name == employee.name &&
//                Objects.equals(dob, employee.dob) &&
//                Objects.equals(badgeNumber, employee.badgeNumber) &&
//                Objects.equals(emailId, employee.emailId);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, name, dob, badgeNumber, emailId);
//    }

//    @Override
//    public String toString() {
//        return "Employee{" +
//                "id=" + id +
//                ", name=" + name +
//                ", manager=" + manager +
//                ", dob=" + dob +
//                ", badgeNumber='" + badgeNumber + '\'' +
//                ", emailId='" + emailId + '\'' +
//                ", createdBy='" + createdBy + '\'' +
//                ", createdDate=" + createdDate +
//                ", modifiedBy='" + modifiedBy + '\'' +
//                ", modifiedDate=" + modifiedDate +
//                ", mobileNumber='" + mobileNumber + '\'' +
//                ", address='" + address + '\'' +
//                ", enabled=" + enabled +
//                '}';
//    }
}
