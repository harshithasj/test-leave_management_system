package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "role")
public class Role {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private Long roleId;

    @Setter
    @Getter
    private String roleType;

    @Setter
    @Getter
    private String createdBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Setter
    @Getter
    private String modifiedBy;

    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date modifiedDate;

    @Setter
    @Getter
    private Boolean enabled;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Role)) return false;
//        Role role = (Role) o;
//        return enabled == role.enabled &&
//                Objects.equals(id, role.id) &&
//                Objects.equals(roleType, role.roleType);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, roleType, enabled);
//    }
//
//    @Override
//    public String toString() {
//        return "Role{" +
//                "id=" + id +
//                ", roleType='" + roleType + '\'' +
//                ", createdBy='" + createdBy + '\'' +
//                ", createdDate=" + createdDate +
//                ", modifiedBy='" + modifiedBy + '\'' +
//                ", modifiedDate=" + modifiedDate +
//                ", enabled=" + enabled +
//                '}';
//    }
}
