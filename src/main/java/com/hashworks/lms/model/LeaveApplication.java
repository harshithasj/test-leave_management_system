package com.hashworks.lms.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@NoArgsConstructor
@Entity
@Table(name="leave_application")
public class LeaveApplication {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="user_id")
    private User user;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="policy_master_id")
    private PolicyMaster policyMaster;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "leave_type_id")
    private LeaveType leaveType;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "reporting_manager_id")
    private ReportingManager reportingManager;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "employee_id")
    private Employee employee;


    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date leaveStartDate;

    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date leaveEndDate;

    @Getter
    @Setter
    private Double appliedNoOfDays;

    @Getter
    @Setter
    private String employeeCommentSubmission;

    @Getter
    @Setter
    private String reportingManagerCommentsApproval;

    @Getter
    @Setter
    private String employeeCommentsRejectionRequest;

    @Getter
    @Setter
    private String reportingManagerCommentsRejectionApproval;

    @Getter
    @Setter
    private String employeeLeaveApplicationWithdrawalCommentsSubmission;


    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date withdrawalDate;

    @Getter
    @Setter
    private String withdrawalComment;

    @Getter
    @Setter
    private String withdrawalStatus; // values : NO,YES


    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private String createdBy;

    @Setter
    @Getter
    private String modifiedBy;

    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date modifiedDate;

}
