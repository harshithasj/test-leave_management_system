package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class PolicyDetailMaster {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private Long totalNoOfDays;

    @Setter
    @Getter
    private Boolean carryForwardAllowed;

    @Setter
    @Getter
    @JoinColumn(name = "leave_type_id")
    @OneToOne(cascade = CascadeType.MERGE)
    private LeaveType leaveType;

    @Setter
    @Getter
    @JoinColumn(name = "policy_master_id")
    @OneToOne(cascade = CascadeType.MERGE)
    private PolicyMaster policyMaster;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof PolicyDetailMaster)) return false;
//        PolicyDetailMaster that = (PolicyDetailMaster) o;
//        return isCarryForwardAllowed == that.isCarryForwardAllowed &&
//                Objects.equals(id, that.id) &&
//                Objects.equals(totalNoOfDays, that.totalNoOfDays) &&
//                Objects.equals(leaveType, that.leaveType) &&
//                Objects.equals(policyMaster, that.policyMaster);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, totalNoOfDays, isCarryForwardAllowed, leaveType, policyMaster);
//    }
//
//    @Override
//    public String toString() {
//        return "PolicyDetailMaster{" +
//                "id=" + id +
//                ", totalNoOfDays=" + totalNoOfDays +
//                ", isCarryForwardAllowed=" + isCarryForwardAllowed +
//                ", leaveType=" + leaveType +
//                ", policyMaster=" + policyMaster +
//                '}';
//    }
}
