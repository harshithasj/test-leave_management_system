package com.hashworks.lms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_leaves_audit")
public class UserLeavesAudit {

    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    private Double debit;

    @Getter
    @Setter
    private Double credit;


    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "leave_type_id")
    private LeaveType leaveType;


    @Getter
    @Setter
    @JoinColumn(name = "policy_master_id")
    @OneToOne(cascade = CascadeType.MERGE)
    private PolicyMaster policyMaster;


    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;


    @Setter
    @Getter
    @Temporal(TemporalType.DATE)
    private Date createdDate;



}
