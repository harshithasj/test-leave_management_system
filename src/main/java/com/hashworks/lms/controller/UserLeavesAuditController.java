package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.service.IHolidayListMasterService;
import com.hashworks.lms.service.IUserLeavesAuditService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping(value = "/rest/userLeaves/")
@CrossOrigin
public class UserLeavesAuditController {

//
//    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
//    public String addRole(RequestBody Role role){
//        try {
//            //log.info("Calling Home API");
//            Map<String,String> responseMap = new HashMap();
//            responseMap.put( Constant.MESSAGE,Constant.WELCOME_MESSAGE);
//            return new ResponseEntity( ResponseJsonUtil.getSuccessResponseJson(responseMap), HttpStatus.OK);
//        } catch (Exception e){
//            log.error(e.getMessage(), e);
//            return new ResponseEntity<>(ResponseJsonUtil.getFailedResponseJson(Constant.INTERNAL_ERROR, e.getMessage()), HttpStatus.OK);
//        }
//    }


    @Autowired
    private IUserLeavesAuditService iUserLeavesAuditService;


//
//    @RequestMapping(value = "/add" , method = RequestMethod.POST)
//    public Map<String,Object> addHoliday(@RequestBody HolidayListMaster holidayListMaster){
//        HashMap<String,Object> map = new HashMap<>();
//        if(holidayListMaster.getName() != null && holidayListMaster.getDate() != null && holidayListMaster.getEnabled() != null) {
//            //role.setRoleType( role.getRoleType() );
//            //role.setEnabled( role.isEnabled() );
//            holidayListMaster.setCreatedDate(new Date());
//            holidayListMasterRepository.save(holidayListMaster);
//            map.put("success", true);
//            map.put("statusCode", HttpStatus.OK.value()); //200 OK
//            map.put("statusMessage", holidayListMaster.getName() + " Holiday Added Successfully");
//            return map;
//        }else{
//            map.put("success", false);
//            map.put("statusCode", HttpStatus.BAD_REQUEST.value()); //400 Bad Request
//            map.put("statusMessage", "Holiday Addition failed. !!! Kindly Pass all the required request parameters.");
//            return map;
//        }
//    }
//
//
//    @RequestMapping(value = "/update" , method = RequestMethod.POST)
//    public Map<String,Object> updateHoliday(@RequestBody HolidayListMaster holidayListMaster){
//        HashMap<String,Object> map = new HashMap<>();
//        if(holidayListMaster.getId() != null && holidayListMaster.getName() != null && holidayListMaster.getDate() != null && holidayListMaster.getEnabled() != null){
//            if( holidayListMasterRepository.findOne(holidayListMaster.getId()) != null){
//                holidayListMasterRepository.save(holidayListMaster);
//                map.put("success", true);
//                map.put("statusCode", HttpStatus.OK.value()); //200 OK
//                map.put("statusMessage", " Holiday Updated Successfully");
//                return map;
//            }else {
//                map.put("success", false);
//                map.put("statusCode", HttpStatus.BAD_REQUEST.value()); //400 Bad Request
//                map.put("statusMessage","Holiday doesn't exist");
//                return map;
//            }
//        }else{
//            map.put("success", false);
//            map.put("statusCode", HttpStatus.BAD_REQUEST.value()); //400 Bad Request
//            map.put("statusMessage","Holiday Updation Failed. !!! Kindly Pass all the required request parameters.");
//            return map;
//        }
//    }
//
//    @RequestMapping(value = "/get" , method = RequestMethod.GET)
//    public Map<String, Object> getAllHolidays(){
//        List<HolidayListMaster> holidayListMasters = holidayListMasterRepository.findAll();
//        Map<String, Object> map = new HashMap<>();
//        map.put("success", true);
//        map.put("statusCode", HttpStatus.OK.value());
//        map.put("statusMessage", "Holidays fetched successfully");
//        map.put("result", holidayListMasters);
//        return map;
//    }
//
//
//
//    @PostMapping(value = "/delete")
//    public Map<String,Object> deleteHoliday(@RequestBody HolidayListMaster holidayListMaster){
//        HashMap<String,Object> map = new HashMap<>();
//        if(holidayListMaster.getId() != null){
//            if( holidayListMasterRepository.findOne(holidayListMaster.getId()) != null){
//                holidayListMasterRepository.delete(holidayListMaster);
//                map.put("success", true);
//                map.put("statusCode", HttpStatus.OK.value()); //200 OK
//                map.put("statusMessage", " Holiday Deleted Successfully");
//                return map;
//            }else{
//                map.put("success", false);
//                map.put("statusCode", HttpStatus.BAD_REQUEST.value()); //400 Bad Request
//                map.put("statusMessage","Holiday doesnt exist");
//                return map;
//            }
//        }else{
//            map.put("success", false);
//            map.put("statusCode", HttpStatus.BAD_REQUEST.value()); //400 Bad Request
//            map.put("statusMessage","Holiday Deletion Failed. !!! Kindly Pass all the required request parameters.");
//            return map;
//        }
//    }



//    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
//    public Map <String, Object> getAllUserLeaves() {
//        try {
//            return iUserLeavesAuditService.getUserLeaves();
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }


    @RequestMapping(value = "/getUserLeaves", method = RequestMethod.POST)
    public Map <String, Object> getUserLeaves(@Valid @RequestBody UserLeaveDetailsRequest userLeaveDetailsRequest, Errors errors) {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iUserLeavesAuditService.getUserLeaveDetails( userLeaveDetailsRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

}
