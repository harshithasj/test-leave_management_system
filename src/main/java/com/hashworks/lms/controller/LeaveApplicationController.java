package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.serviceImpl.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hashworks.lms.service.ILeaveApplicationService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@CrossOrigin
@RestController
@Slf4j
@RequestMapping(value = "/rest/leaveApplication")
public class LeaveApplicationController {

    @Autowired
    ILeaveApplicationService iLeaveApplicationService;

    @Autowired
    private NotificationService notificationService;


    @RequestMapping( value ="/apply", method = RequestMethod.POST)
  public  Map<String,Object> applyForLeave(@Valid @RequestBody LeaveApplicationSaveRequest leaveApplicationRequest, Errors errors)
    {

        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return iLeaveApplicationService.applyForLeave(leaveApplicationRequest);
            }
        }
        catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @RequestMapping(value ="/employee/viewLeavesStatus", method = RequestMethod.POST)
  public  Map<String,Object> viewLeaveStatus(@Valid @RequestBody LeaveStatusRequest leaveStatusRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.viewLeavesStatus(leaveStatusRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value ="/manager/viewLeaves", method =RequestMethod.POST)
 public   Map<String,Object> viewLeavesByManager(@Valid @RequestBody ManagerLeaveViewRequest managerLeaveViewRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.viewLeavesByManager(managerLeaveViewRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value ="/manager/viewLeavesByDate", method =RequestMethod.POST)
    public   Map<String,Object> viewLeavesByManagerByDate (@Valid @RequestBody ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.viewLeavesByManagerByDate(managerLeaveViewByDateRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

  public  @RequestMapping(value ="/manager/ApproveLeaves", method =RequestMethod.POST)
    Map<String,Object> approveLeaves(@Valid @RequestBody ApproveLeavesRequest approveLeavesRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.approveLeaves(approveLeavesRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value ="/employee/LeaveCancellation", method =RequestMethod.POST)
  public  Map<String,Object> rejectLeaves(@Valid @RequestBody RejectLeavesRequest rejectLeavesRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else{
            return iLeaveApplicationService.rejectLeaves(rejectLeavesRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }
//    @RequestMapping(value ="/manager/rejectionLeavesApproval", method =RequestMethod.POST)
//  public  Map<String,Object> rejectionLeavesApproval(@Valid @RequestBody ApproveRejectionLeavesRequest approveRejectionLeavesRequest, Errors errors)
//    {
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            }else {
//                return iLeaveApplicationService.rejectionLeavesApproval(approveRejectionLeavesRequest);
//            }
//        }
//        catch(Exception e){
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

    @RequestMapping(value ="/employee/editAppledLeave", method = RequestMethod.POST)
    public  Map<String,Object> editAppliedLeave(@Valid @RequestBody EditAppliedLeaveRequest editAppliedLeaveRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.editAppliedLeave(editAppliedLeaveRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    public  @RequestMapping(value ="/manager/RejectLeaves", method =RequestMethod.POST)
    Map<String,Object> managerRejectLeaves(@Valid @RequestBody ManagerRejectLeavesRequest managerRejectLeavesRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.managerRejectLeaves(managerRejectLeavesRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @RequestMapping(value ="/employee/leavetrail", method = RequestMethod.GET)
    public  Map<String,Object> performLeaveTrail()
    {
        try {

                return iLeaveApplicationService.consumptionStartedLeaveApplicationsJob();
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


//    @RequestMapping(value = "/apply" , method = RequestMethod.GET)
//    public String applyLeave(){
//
//        try{
//            notificationService.sendNotification();
//        }catch (MailException e){
//            //catch error
//        }
//        return "Leave Applied successfully and email has been sent to approver successfully";
//    }


    @RequestMapping(value ="/employee/withdrawLeaves", method = RequestMethod.POST)
    public  Map<String,Object> withdrawLeave(@Valid @RequestBody WithdrawLeavesRequest withdrawLeavesRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            }else {
                return iLeaveApplicationService.withdrawLeaves(withdrawLeavesRequest);
            }
        }
        catch(Exception e){
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @RequestMapping(value = "/getEmployeeLOPInfo", method = RequestMethod.POST)
//    public Map <String, Object> getEmployeeLOPInfo(@Valid @RequestBody EmployeeLOPInfoRequest employeeLOPInfoRequest,Errors errors)
//    {
//        try {
//            return this.iLeaveApplicationService.getEmployeeLOPInfo(employeeLOPInfoRequest);
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }


}
