package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.service.IReportingManagerService;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping(value = "/rest/config/manager")
@CrossOrigin
public class ReportingManagerController {


    @Autowired
    private IReportingManagerService iReportingManagerService;


//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    @Transactional
//    public Map<String, Object> addRole(@Valid @RequestBody RoleSaveRequest roleSaveRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else {
//            Role role = new Role();
//            role.setRoleType( roleSaveRequest.getRoleType() );
//            role.setEnabled( roleSaveRequest.getEnabled() );
//            role.setCreatedBy( roleSaveRequest.getCreatedBy() );
//            role.setCreatedDate( new Date() );
//            Role rolenew = roleRepository.save( role );
////            if (rolenew.getId() > 11){
////                throw new RuntimeException( "Rollback needs to be done" );
////            }
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", role.getRoleType() + " Role Added Successfully" );
//            return map;
//        }
//    }


//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public Map<String, Object> updateRole(@Valid @RequestBody RoleUpdateRequest roleUpdateRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleUpdateRequest.getId() ) != null) {
//            Role role = new Role();
//            role.setRoleType( roleUpdateRequest.getRoleType() );
//            role.setEnabled( roleUpdateRequest.getEnabled() );
//            role.setModifiedBy( roleUpdateRequest.getModifiedBy() );
//            role.setModifiedDate( new Date() );
//            roleRepository.save( role );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Updated Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesn't exist" );
//            return map;
//        }
//
//    }
//
//
//    @RequestMapping(value = "/get" , method = RequestMethod.GET)
//    public Map<String, Object> getAllRoles(){
//        List<Role> roles = roleRepository.findAll();
//        Map<String, Object> map = new HashMap<>();
//        map.put("success", true);
//        map.put("statusCode", HttpStatus.OK.value());
//        map.put("statusMessage", "Roles fetched successfully");
//        map.put("result", roles);
//        return map;
//    }


//    @PostMapping(value = "/delete")
//    public Map<String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleDeleteRequest.getId() ) != null) {
//
//            roleRepository.delete( roleDeleteRequest.getId() );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Deleted Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesnt exist" );
//            return map;
//        }
//
//    }

//    @PreAuthorize( "hasAnyRole('ADMIN')" )
//    @RequestMapping(value = "/addOne", method = RequestMethod.POST)
//    public Map <String, Object> addRM(@Valid @RequestBody ReportingManagerSaveRequest reportingManagerSaveRequest, Errors errors) {
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return this.iReportingManagerService.addOneRM( reportingManagerSaveRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Map <String, Object> addAllRMs(@Valid @RequestBody ReportingManagerMultiSaveRequest reportingManagerMultiSaveRequest, Errors errors) {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iReportingManagerService.addAllRMs( reportingManagerMultiSaveRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Map <String, Object> updateRM(@Valid @RequestBody ReportingManagerUpdateRequest reportingManagerUpdateRequest, Errors errors) {
        HashMap <String, Object> map = new HashMap <>();
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iReportingManagerService.updateRM( reportingManagerUpdateRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

////    @PreAuthorize( "hasAnyRole('ADMIN')" )
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Map <String, Object> getAllRMs() {
        try {
            return this.iReportingManagerService.getAllRMs();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

////    @PreAuthorize( "hasAnyRole('ADMIN')" )
//    @RequestMapping(value = "/delete", method = RequestMethod.POST)
//    public Map <String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return this.iRoleService.deleteRole( roleDeleteRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//
//    }


}
