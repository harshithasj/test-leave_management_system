package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.LeaveTypeDeleteRequest;
import com.hashworks.lms.dto.request.LeaveTypeSaveRequest;
import com.hashworks.lms.dto.request.LeaveTypeUpdateRequest;
import com.hashworks.lms.service.ILeaveTypeService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping(value = "/rest/config/leaveType")
@CrossOrigin
public class LeaveTypeController {

    @Autowired
    ILeaveTypeService iLeaveTypeService;

    @RequestMapping( value ="/get", method = RequestMethod.GET)
    Map<String, Object>  getAllLeaveTypes()
    {
        try {
            return iLeaveTypeService.getAllLeaveTypes();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }

    @Transactional
    @RequestMapping( value ="/add", method = RequestMethod.POST)
    Map<String,Object>  addLeaveType(@Valid  @RequestBody LeaveTypeSaveRequest leaveTypeRequest, Errors errors)
    {

        try {
            if (errors.hasErrors()) {
                System.out.println(errors);
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return iLeaveTypeService.addLeaveType(leaveTypeRequest);
            }
        }
        catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }


    }

//    @RequestMapping( value ="/addAllLeaveTypes", method = RequestMethod.POST)
//    ResponseEntity<Map>  addAllLeaveTypes(@RequestBody String requestString)
//    {
//        //try{
//           return iLeaveTypeService.addAllLeaveTypes(requestString);
//           //return new ResponseEntity<Map>(ResponseJsonUtil.getSuccessResponseJson(msg),HttpStatus.OK);
//        //}
//        //catch(Exception e)
//        //{
//           // log.error(e.getMessage(),e);
//           // return  new ResponseEntity<Map>(ResponseJsonUtil.getFailedResponseJson(e.getMessage()), HttpStatus.BAD_REQUEST);
//       // }
//    }

    @RequestMapping( value ="/delete", method = RequestMethod.POST)
    Map<String,Object>  deleteLeaveType(@Valid  @RequestBody LeaveTypeDeleteRequest leaveTypeRequest,Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return iLeaveTypeService.deleteLeaveType(leaveTypeRequest);
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }

    }

    @RequestMapping( value ="/edit", method = RequestMethod.POST)
    Map<String,Object>   editLeaveType(@Valid  @RequestBody LeaveTypeUpdateRequest leaveTypeRequest, Errors errors)
    {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return  iLeaveTypeService.editLeaveType(leaveTypeRequest);
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }


    }

}
