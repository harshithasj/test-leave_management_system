package com.hashworks.lms.controller;

//import com.example.polls.payload.LoginRequest;
//import com.example.polls.repository.UserRepository;
//import com.example.polls.security.JwtTokenProvider;
import com.hashworks.lms.RestExceptionHandler;
import com.hashworks.lms.dto.request.LoginRequest;
import com.hashworks.lms.exception.AppException;
import com.hashworks.lms.model.User;
import com.hashworks.lms.repository.UserRepository;
import com.hashworks.lms.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

//import com.example.polls.payload.ApiResponse;
//import com.example.polls.payload.JwtAuthenticationResponse;
//import com.example.polls.payload.SignUpRequest;
//import com.example.polls.repository.RoleRepository;

/**
 * Created by rajeevkumarsingh on 02/08/17.
 */
@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {


    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;

//    @Autowired
//    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

//    @PostMapping("/signin")
//    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
//
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        loginRequest.getUsernameOrEmail(),
//                        loginRequest.getPassword()
//                )
//        );
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//
//        String jwt = tokenProvider.generateToken(authentication);
//        System.out.println( authentication.getPrincipal().toString() );
//        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
//    }

    @PostMapping("/signin")
    public Map<String,Object> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<User> userOptional=userRepository.findByUsername(loginRequest.getUsername().trim());
        User user=null;
        if (userOptional.isPresent()){
             user=userOptional.get();
            if(user.getEnabled()==false || user.getEmployee().getEnabled()==false){
                throw new AppException("Access Denied!!!");
            }
        }

      return tokenProvider.generateToken(authentication,user.getEmployee().getName());
//        System.out.println( authentication.getPrincipal().toString() );
//        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
//        return
    }

//
//    @PostMapping("/signup")
//    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
//        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
//            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
//            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        // Creating user's account
//        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
//                signUpRequest.getEmail(), signUpRequest.getPassword());
//
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
//
//        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
//                .orElseThrow(() -> new AppException("User Role not set."));
//
//        user.setRoles(Collections.singleton(userRole));
//
//        User result = userRepository.save(user);
//
//        URI location = ServletUriComponentsBuilder
//                .fromCurrentContextPath().path("/users/{username}")
//                .buildAndExpand(result.getUsername()).toUri();
//
//        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
//    }
}
