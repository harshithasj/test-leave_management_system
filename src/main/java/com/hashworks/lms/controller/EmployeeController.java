package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.ExportToExcelResponse;
import com.hashworks.lms.model.LeaveApplication;
import com.hashworks.lms.model.UserLeavesAudit;
import com.hashworks.lms.repository.LeaveApplicationRepository;
import com.hashworks.lms.repository.LeaveTypeRepository;
import com.hashworks.lms.repository.UserLeavesAuditRepository;
import com.hashworks.lms.service.IEmployeeService;
import com.hashworks.lms.service.IRoleService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@RestController
@Slf4j
@RequestMapping(value = "/rest/employees")
@CrossOrigin
public class EmployeeController {


    @Autowired
    private IEmployeeService iEmployeeService;

    @Autowired
    private LeaveApplicationRepository leaveApplicationRepository;

    @Autowired
    private UserLeavesAuditRepository userLeavesAuditRepository;

    @Autowired
    AuthenticationManager authenticationManager;


//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    @Transactional
//    public Map<String, Object> addRole(@Valid @RequestBody RoleSaveRequest roleSaveRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else {
//            Role role = new Role();
//            role.setRoleType( roleSaveRequest.getRoleType() );
//            role.setEnabled( roleSaveRequest.getEnabled() );
//            role.setCreatedBy( roleSaveRequest.getCreatedBy() );
//            role.setCreatedDate( new Date() );
//            Role rolenew = roleRepository.save( role );
////            if (rolenew.getId() > 11){
////                throw new RuntimeException( "Rollback needs to be done" );
////            }
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", role.getRoleType() + " Role Added Successfully" );
//            return map;
//        }
//    }


//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public Map<String, Object> updateRole(@Valid @RequestBody RoleUpdateRequest roleUpdateRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleUpdateRequest.getId() ) != null) {
//            Role role = new Role();
//            role.setRoleType( roleUpdateRequest.getRoleType() );
//            role.setEnabled( roleUpdateRequest.getEnabled() );
//            role.setModifiedBy( roleUpdateRequest.getModifiedBy() );
//            role.setModifiedDate( new Date() );
//            roleRepository.save( role );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Updated Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesn't exist" );
//            return map;
//        }
//
//    }


//    @RequestMapping(value = "/get" , method = RequestMethod.GET)
//    public Map<String, Object> getAllRoles(){
//        List<Role> roles = roleRepository.findAll();
//        Map<String, Object> map = new HashMap<>();
//        map.put("success", true);
//        map.put("statusCode", HttpStatus.OK.value());
//        map.put("statusMessage", "Roles fetched successfully");
//        map.put("result", roles);
//        return map;
//    }


//    @PostMapping(value = "/delete")
//    public Map<String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleDeleteRequest.getId() ) != null) {
//
//            roleRepository.delete( roleDeleteRequest.getId() );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Deleted Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesnt exist" );
//            return map;
//        }
//
//    }

//    @PreAuthorize( "hasAnyRole('ADMIN')" )
//    @RequestMapping(value = "/mapRole", method = RequestMethod.POST)
//    public Map <String, Object> mapRole(@Valid @RequestBody EmployeeRoleSaveRequest roleSaveRequest, Errors errors) {
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return this.iRoleService.addRole( roleSaveRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public Map <String, Object> updateRole(@Valid @RequestBody RoleUpdateRequest roleUpdateRequest, Errors errors) {
//        HashMap <String, Object> map = new HashMap <>();
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return this.iRoleService.updateRole( roleUpdateRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

//    @PreAuthorize( "hasAnyRole('ROLE_ADMIN')" )
    @RequestMapping(value = "/getAllNew", method = RequestMethod.GET)
    public Map <String, Object> getAllNewEmployees() {
        try {
            return this.iEmployeeService.getAllNewEmployees();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @PreAuthorize( "hasAnyRole('ROLE_ADMIN')" )
    @RequestMapping(value = "/mapRole", method = RequestMethod.POST)
    public Map <String, Object> mapRole(@Valid @RequestBody RoleAndManagerMultiSaveRequest roleAndManagerMultiSaveRequest, Errors errors) {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iEmployeeService.mapRole( roleAndManagerMultiSaveRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @PreAuthorize( "hasAnyRole('ROLE_ADMIN')" )
    @RequestMapping(value = "/mapManager", method = RequestMethod.POST)
    public Map <String, Object> mapManager(@Valid @RequestBody EmployeeRMMappingSaveRequest employeeRMMappingSaveRequest, Errors errors) {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iEmployeeService.mapManager( employeeRMMappingSaveRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @PreAuthorize( "hasAnyRole('Admin')" )
    @RequestMapping(value = "/getAllEmployeeDetails", method = RequestMethod.POST)
    public Map <String, Object> getAllEmployeeDetails(@Valid @RequestBody GetEmployeeDetailsRequest getEmployeeDetailsRequest) {
        try {
            return this.iEmployeeService.getAllEmployeeDetails(getEmployeeDetailsRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/getAllEmployeeDetailsOld", method = RequestMethod.GET)
    public Map <String, Object> getAllEmployeeDetailsOld() {
        try {
            return this.iEmployeeService.getAllEmployeeDetailsOld();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @PreAuthorize( "hasAnyRole('ADMIN')" )
//    @RequestMapping(value = "/delete", method = RequestMethod.POST)
//    public Map <String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return this.iRoleService.deleteRole( roleDeleteRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//
//    }

//    @PreAuthorize( "hasAnyRole('ROLE_ADMIN','ROLE_MANAGER','ROLE_EMPLOYEE')" )
    @RequestMapping(value = "/getEmployeeDetail", method = RequestMethod.POST)
    public Map <String, Object> getEmployeeDetail(@Valid @RequestBody SpecificEmployeeDetailRequest specificEmployeeDetailRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.getEmployeeDetail(specificEmployeeDetailRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/getEmployeeDetailByManager", method = RequestMethod.POST)
    public Map <String, Object> getEmployeeDetailByManager(@Valid @RequestBody EmployeeDetailByManagerRequest employeeDetailByManagerRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.getEmployeeDetailByManager(employeeDetailByManagerRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/changePasswordMail", method = RequestMethod.POST)
    public Map <String, Object>changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.ChangePassword(changePasswordRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/changePasswordByAdmin", method = RequestMethod.POST)
    public Map <String, Object>changePasswordByAdmin(@Valid @RequestBody ChangePasswordByAdminRequest changePasswordByAdminRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.ChangePasswordByAdmin(changePasswordByAdminRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/DisableEmployeeByAdmin", method = RequestMethod.POST)
    public Map <String, Object>disableEmployeeByAdmin(@Valid @RequestBody DisableEmployeeRequest disableEmployeeRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.disableEmployeeByAdmin(disableEmployeeRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }


    @RequestMapping(value = "/exportToExcel", method = RequestMethod.POST)
    public Map exportToExcel(@Valid @RequestBody ExportToExcelRequest exportToExcelRequest,Errors errors)
    {
      //  try {
         return    this.iEmployeeService.exportToExcel(exportToExcelRequest);
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
    }

    @RequestMapping(value = "/EmployeeFeedback", method = RequestMethod.POST)
    public Map <String, Object>employeeFeedback(@Valid @RequestBody EmployeeFeedbackRequest employeeFeedbackRequest,Errors errors)
    {
        try {
            return this.iEmployeeService.employeeFeedback(employeeFeedbackRequest);
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

    @RequestMapping(value = "/pendingLeavesByAdmin", method = RequestMethod.GET)
    public Map <String, Object>pendingLeavesByAdmin()
    {
        try {
            return this.iEmployeeService.pendingLeavesByAdmin();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//Get All Employee Details latest
//
//    @RequestMapping(value = "/geeeet", method = RequestMethod.GET)
//    public Map <String, Object> getAllEmployeeDetails() {
//        try {
//     //       return this.iEmployeeService.getAllEmployeeDetails();
//     log.info("daily job updating the leave balances started");
//            Calendar calendar = Calendar.getInstance();
//            System.out.println(calendar.get()+"..............");
//    List<LeaveApplication> leaveApplications = leaveApplicationRepository.findByConsumptionStartedStatusAndCurrentDate("consumption started","NO",new Date());
//
//        for (LeaveApplication leaveApplication:leaveApplications) {
//
//        UserLeavesAudit userLeavesAudit = new UserLeavesAudit();
//        userLeavesAudit.setDebit( 1.0 );
//        userLeavesAudit.setCredit( 0.0 );
//        userLeavesAudit.setLeaveType( leaveApplication.getLeaveType());
//        userLeavesAudit.setPolicyMaster( leaveApplication.getPolicyMaster() );
//        userLeavesAudit.setUser( leaveApplication.getUser() );
//        userLeavesAudit.setCreatedDate( new Date(  ) );
//        userLeavesAuditRepository.save( userLeavesAudit );
//
//    }
//            log.info("daily job updating the leave balances completed");
//        HashMap hm = new HashMap();
//            hm.put("hi","hi");
//            return hm;
//
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void index(HttpServletRequest request, HttpServletResponse response)  {

        try{
            File file = new File("/home/ubuntu/LeaveReport.xls");
            System.out.println(file.getName() + ".....inputStrem");
            FileInputStream inputStream = new FileInputStream(file);
            response.setHeader("Content-Disposition", "attachment; filename=\"LeaveReport.xls\"");
            response.setContentType("application/ms-excel");
            response.setContentLengthLong(file.length());

            ServletOutputStream outputStream = response.getOutputStream();
            IOUtils.copy(inputStream, outputStream);

            outputStream.close();
            inputStream.close();

        }
        catch(Exception e)
        {
            System.out.println(e);
        }
//        ResponseEntity<Resource> res=null;
//        try {
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
//            headers.add("Pragma", "no-cache");
//            headers.add("Expires", "0");
//            File file = new File("/home/hashworks/abc.xls");
//            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
//
//            return ResponseEntity.ok()
//                    .headers(headers)
//                    .contentLength(file.length())
//                    .contentType(MediaType.parseMediaType("application/ms-excel"))
//                    .body(resource);
//        }catch(Exception e)
//        {
//            System.out.println(e);
//        }
//                return res;
    }
    @PostMapping("/signup")
    public Map<String,Object> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        try {
            return iEmployeeService.signUp(signUpRequest);
        }catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }
    @PostMapping("/resetpassword")
    public Map<String,Object> resetPassword(@Valid @RequestBody ResetPasswordRequest resetPassword){
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            resetPassword.getUserName(),
                            resetPassword.getOldPassword()
                    )
            );
            return iEmployeeService.resetPassword(resetPassword);
        }catch(Exception e)
        {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }
}
