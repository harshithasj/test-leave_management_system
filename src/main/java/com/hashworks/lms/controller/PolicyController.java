package com.hashworks.lms.controller;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.PolicyDetailMasterResponse;
import com.hashworks.lms.dto.response.PolicyResponse;
import com.hashworks.lms.model.LeaveType;
import com.hashworks.lms.model.PolicyDetailMaster;
import com.hashworks.lms.model.PolicyMaster;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.repository.LeaveTypeRepository;
import com.hashworks.lms.repository.PolicyDetailMasterRepository;
import com.hashworks.lms.repository.PolicyMasterRepository;
import com.hashworks.lms.repository.RoleRepository;
import com.hashworks.lms.service.IPolicyService;
import com.hashworks.lms.utils.ResponseJsonUtilNew;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@Slf4j
@RequestMapping(value = "/rest/config/policy")
@CrossOrigin
public class PolicyController {

    @Autowired
    private IPolicyService iPolicyService;
//
//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    @Transactional(propagation=Propagation.REQUIRED,readOnly = false)
//    public Map<String, Object> addPolicy(@Valid @RequestBody PolicySaveRequest policySaveRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else {
//
//            PolicyMaster policyMaster = new PolicyMaster();
//
//            policyMaster.setYear( policySaveRequest.getYear() );
//            policyMaster.setName( policySaveRequest.getName() );
//            policyMaster.setLeavePolicyType( policySaveRequest.getLeavePolicyType() );
//            policyMaster.setCreatedDate( new Date() );
//            policyMaster.setCreatedBy( policySaveRequest.getCreatedBy() );
//            policyMaster.setEnabled( policySaveRequest.getEnabled() );
//
//            policyMaster = policyMasterRepository.save( policyMaster );
//
//            if(policyMaster.getId() > 1){
//                for (PolicyDetailMasterSaveRequest policyDetailMasterRequest : policySaveRequest.getPolicyDetailMasterSaveRequests()) {
//                    PolicyDetailMaster policyDetailMaster = new PolicyDetailMaster();
//
//                    policyDetailMaster.setCarryForwardAllowed( policyDetailMasterRequest.getCarryForwardAllowed() );
//                           policyDetailMaster.setTotalNoOfDays( policyDetailMasterRequest.getTotalNoOfDays() );
//                           policyDetailMaster.setLeaveType( leaveTypeRepository.findOne( policyDetailMasterRequest.getId() ) );
//                           policyDetailMaster.setPolicyMaster( policyMasterRepository.findOne(policyMaster.getId()) );
//                           policyDetailMasterRepository.save(policyDetailMaster);
//                    }
//            }
//
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", policyMaster.getName() + " Policy Added Successfully" );
//            return map;
//        }
//    }

//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public Map<String, Object> updateRole(@Valid @RequestBody RoleUpdateRequest roleUpdateRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleUpdateRequest.getId() ) != null) {
//            Role role = new Role();
//            role.setRoleType( roleUpdateRequest.getRoleType() );
//            role.setEnabled( roleUpdateRequest.getEnabled() );
//            role.setModifiedBy( roleUpdateRequest.getModifiedBy() );
//            role.setModifiedDate( new Date() );
//            roleRepository.save( role );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Updated Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesn't exist" );
//            return map;
//        }
//
//    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @Transactional
    public Map <String, Object> addPolicy(@Valid @RequestBody PolicySaveRequest policySaveRequest, Errors errors) {
        try {
            if (errors.hasErrors()) {
                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
            } else {
                return this.iPolicyService.addPolicy( policySaveRequest );
            }
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
        }
    }

//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public Map <String, Object> updateRole(@Valid @RequestBody RoleUpdateRequest roleUpdateRequest, Errors errors) {
//        HashMap <String, Object> map = new HashMap <>();
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return iRoleService.updateRole( roleUpdateRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Map <String, Object> getAllPolicies() {
        try {
//            System.out.println( SecurityContextHolder.getContext().getAuthentication() ); //Principal class contains the object
            return this.iPolicyService.getAllPolicies();
        } catch (Exception e) {
            return ResponseJsonUtilNew.genericExceptionMessage( e.getMessage() );
        }
    }

//    @RequestMapping(value = "/delete", method = RequestMethod.POST)
//    public Map <String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//
//        try {
//            if (errors.hasErrors()) {
//                return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
//            } else {
//                return iRoleService.deleteRole( roleDeleteRequest );
//            }
//        } catch (Exception e) {
//            return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
//        }
//
//    }


//
//    @PostMapping(value = "/delete")
//    public Map<String, Object> deleteRole(@Valid @RequestBody RoleDeleteRequest roleDeleteRequest, Errors errors) {
//        HashMap<String, Object> map = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
//            return map;
//        } else if (roleRepository.findOne( roleDeleteRequest.getId() ) != null) {
//
//            roleRepository.delete( roleDeleteRequest.getId() );
//            map.put( "success", true );
//            map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
//            map.put( "statusMessage", " Role Deleted Successfully" );
//            return map;
//        } else {
//            map.put( "success", false );
//            map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
//            map.put( "statusMessage", "Role doesnt exist" );
//            return map;
//        }
//
//    }
@RequestMapping(value = "/delete", method = RequestMethod.POST)
@Transactional
public Map <String, Object> deletePolicy(@Valid @RequestBody PolicyDeleteRequest policyDeleteRequest, Errors errors) {
    try {
        if (errors.hasErrors()) {
            return ResponseJsonUtilNew.getInvalidIncorrectParametersError();
        } else {
            return this.iPolicyService.deletePolicy( policyDeleteRequest );
        }
    } catch (Exception e) {
        return ResponseJsonUtilNew.genericExceptionMessage( e.getLocalizedMessage() );
    }
}


}
