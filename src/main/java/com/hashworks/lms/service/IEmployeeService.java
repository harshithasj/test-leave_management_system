package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.*;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface IEmployeeService {


    Map<String, Object> getAllNewEmployees();

    Map<String, Object> mapRole(RoleAndManagerMultiSaveRequest roleAndManagerMultiSaveRequest);

    Map<String,Object> mapManager(EmployeeRMMappingSaveRequest employeeRMMappingSaveRequest);

    Map<String,Object> getAllEmployeeDetails(GetEmployeeDetailsRequest getEmployeeDetailsRequest);

    Map<String,Object> getEmployeeDetail(SpecificEmployeeDetailRequest specificEmployeeDetailRequest);

    Map<String,Object> getEmployeeDetailByManager(EmployeeDetailByManagerRequest employeeDetailByManagerRequest);

    Map<String,Object> exportToExcel(ExportToExcelRequest exportToExcelRequest);

    Map<String,Object> getAllEmployeeDetailsOld();

    Map<String,Object> ChangePassword(ChangePasswordRequest changePasswordRequest);

    Map<String,Object> ChangePasswordByAdmin(ChangePasswordByAdminRequest changePasswordByAdminRequest);

    Map<String,Object> disableEmployeeByAdmin(DisableEmployeeRequest disableEmployeeRequest);

    Map<String,Object> employeeFeedback(EmployeeFeedbackRequest employeeFeedbackRequest);

    Map<String,Object> pendingLeavesByAdmin();
//
//    Map<String,Object> updateRole(RoleUpdateRequest roleUpdateRequest);
//
//    Map<String,Object> deleteRole(RoleDeleteRequest roleDeleteRequest);

    @Transactional
    Map<String,Object> signUp(SignUpRequest signUpRequest);

    Map<String,Object> resetPassword(ResetPasswordRequest resetPassword);
//    Map<String,Object> forgotPassword(String email);
}
