package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.HolidayDeleteRequest;
import com.hashworks.lms.dto.request.HolidaySaveRequest;
import com.hashworks.lms.dto.request.HolidayUpdateRequest;

import java.util.Map;

public interface IHolidayListMasterService {
    Map<String,Object> addHoliday(HolidaySaveRequest holidaySaveRequest);

    Map<String,Object> updateHoliday(HolidayUpdateRequest holidayUpdateRequest);

    Map<String,Object> getAllHolidays();

    Map<String,Object> deleteHoliday(HolidayDeleteRequest holidayDeleteRequest);
}
