package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.PolicyDeleteRequest;
import com.hashworks.lms.dto.request.PolicySaveRequest;

import java.util.Map;

public interface IPolicyService {

    Map <String, Object> addPolicy(PolicySaveRequest policySaveRequest);

    Map <String, Object> getAllPolicies();

    Map<String,Object> deletePolicy(PolicyDeleteRequest policyDeleteRequest);
}
