//package com.hashworks.lms.service;
//
//import com.hashworks.lms.model.Role;
//import com.hashworks.lms.security.UserInfo;
//import org.springframework.security.core.GrantedAuthority;
//
//import javax.servlet.http.HttpSession;
//import java.util.Collection;
//
//
//public interface IAuthenticationService {
//    Collection<? extends GrantedAuthority> getAuthorities(Role role);
//    UserInfo login(String email, String password, HttpSession httpSession);
//}
