package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.RoleDeleteRequest;
import com.hashworks.lms.dto.request.RoleSaveRequest;
import com.hashworks.lms.dto.request.RoleUpdateRequest;
import com.hashworks.lms.model.Role;

import java.util.List;
import java.util.Map;

public interface IRoleService {


    Map<String, Object> getAllRoles();

    Map<String, Object> addRole(RoleSaveRequest roleSaveRequest);

    Map<String,Object> updateRole(RoleUpdateRequest roleUpdateRequest);

    Map<String,Object> deleteRole(RoleDeleteRequest roleDeleteRequest);
}
