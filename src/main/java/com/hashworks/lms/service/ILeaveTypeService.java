package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.LeaveTypeDeleteRequest;
import com.hashworks.lms.dto.request.LeaveTypeSaveRequest;
import com.hashworks.lms.dto.request.LeaveTypeUpdateRequest;
import com.hashworks.lms.model.LeaveType;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ILeaveTypeService {

  public Map<String,Object> getAllLeaveTypes();

  public  Map<String,Object> addLeaveType(LeaveTypeSaveRequest leaveTypeRequest);

//  public ResponseEntity<Map> addAllLeaveTypes(String requestString);

  public Map<String,Object> deleteLeaveType(LeaveTypeDeleteRequest leaveTypeRequest);

  public Map<String,Object> editLeaveType(LeaveTypeUpdateRequest leaveTypeRequest);
}
