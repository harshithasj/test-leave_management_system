package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.ReportingManagerMultiSaveRequest;
import com.hashworks.lms.dto.request.ReportingManagerSaveRequest;
import com.hashworks.lms.dto.request.ReportingManagerUpdateRequest;
import com.hashworks.lms.dto.request.RoleUpdateRequest;

import java.util.Map;

public interface IReportingManagerService {
    Map<String,Object> addRM(ReportingManagerSaveRequest reportingManagerSaveRequest);

    Map<String, Object> getAllRMs();

    Map<String,Object> addAllRMs(ReportingManagerMultiSaveRequest reportingManagerMultiSaveRequest);

    Map<String,Object> updateRM(ReportingManagerUpdateRequest reportingManagerUpdateRequest);

}
