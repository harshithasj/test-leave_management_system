package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.*;

import java.util.Map;

public interface ILeaveApplicationService {

    Map<String,Object> applyForLeave(LeaveApplicationSaveRequest leaveApplicationRequest);

    Map<String,Object> viewLeavesStatus(LeaveStatusRequest leaveStatusRequest);

    Map<String,Object> viewLeavesByManager(ManagerLeaveViewRequest managerLeaveViewRequest);

    Map<String,Object> approveLeaves(ApproveLeavesRequest approveLeavesRequest);

    Map<String,Object> rejectLeaves(RejectLeavesRequest rejectLeavesRequest);

//    Map<String,Object> rejectionLeavesApproval(ApproveRejectionLeavesRequest approveRejectionLeavesRequest);

    Map<String,Object> editAppliedLeave(EditAppliedLeaveRequest editAppliedLeaveRequest);

    Map<String,Object> managerRejectLeaves(ManagerRejectLeavesRequest managerRejectLeavesRequest);

    Map<String,Object> viewLeavesByManagerByDate(ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest);

    Map<String,Object> consumptionStartedLeaveApplicationsJob();

    Map<String,Object> withdrawLeaves(WithdrawLeavesRequest withdrawLeavesRequest);

 //   Map<String,Object> getEmployeeLOPInfo(EmployeeLOPInfoRequest employeeLOPInfoRequest);
}
