package com.hashworks.lms.service;

import com.hashworks.lms.dto.request.*;
import com.hashworks.lms.dto.response.UserLeaveDetailsResponse;

import java.util.Map;

public interface IUserLeavesAuditService {
//
//    Map<String,Object> applyForLeave(LeaveApplicationSaveRequest leaveApplicationRequest);
//
    Map<String,Object> getUserLeaveDetails(UserLeaveDetailsRequest userLeaveDetailsRequest);

    UserLeaveDetailsResponse getUserLeaveDetailsNew(UserLeaveDetailsRequest userLeaveDetailsRequest);

    UserLeaveDetailsResponse getUserLeaveDetailsNew2(UserLeaveDetailsRequest userLeaveDetailsRequest,GetEmployeeDetailsRequest getEmployeeDetailsRequest);
//
//    Map<String,Object> viewLeavesByManager(ManagerLeaveViewRequest managerLeaveViewRequest);
//
//    Map<String,Object> approveLeaves(ApproveLeavesRequest approveLeavesRequest);
//
//    Map<String,Object> rejectLeaves(RejectLeavesRequest rejectLeavesRequest);
//
////    Map<String,Object> rejectionLeavesApproval(ApproveRejectionLeavesRequest approveRejectionLeavesRequest);
//
//    Map<String,Object> editAppliedLeave(EditAppliedLeaveRequest editAppliedLeaveRequest);
//
//    Map<String,Object> managerRejectLeaves(ManagerRejectLeavesRequest managerRejectLeavesRequest);
//
//    Map<String,Object> viewLeavesByManagerByDate(ManagerLeaveViewByDateRequest managerLeaveViewByDateRequest);
//
//    Map<String,Object> consumptionStartedLeaveApplicationsJob();

}
