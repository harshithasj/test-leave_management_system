package com.hashworks.lms.utils;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class ResponseJsonUtilNew {

    public static HashMap<String,Object> getInvalidIncorrectParametersError(){
        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", false );
        map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
        map.put( "statusMessage", "Invalid/Incorrect Request Parameters" );
        return map;
    }

    public static Map<String,Object> genericExceptionMessage(String exceptionMessage) {
        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", false );
        //map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
        map.put( "statusMessage", exceptionMessage  );
        return map;
    }

    public static HashMap<String,Object> postSuccessResponseMessage(String successMessage){
        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", true );
        map.put( "statusCode", HttpStatus.OK.value() ); //200 OK
        map.put( "statusMessage", successMessage  );
        return map;
    }

    public static Map<String,Object> getSuccessResponseMessage(String successMessage,Object object){
        HashMap<String, Object> map = new HashMap<>();
        map.put("success", true);
        map.put("statusCode", HttpStatus.OK.value());
        map.put("statusMessage", successMessage);
        map.put("result", object);
        return map;
    }

    public static Map<String,Object> entityNotFoundMessage(String failureMessage) {
        HashMap<String, Object> map = new HashMap<>();
        map.put( "success", false );
        map.put( "statusCode", HttpStatus.BAD_REQUEST.value() ); //400 Bad Request
        map.put( "statusMessage", failureMessage );
        return map;
    }
}
