package com.hashworks.lms.utils;

import com.hashworks.lms.model.constant.Constant;
import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class ResponseJsonUtil {
	/**
	 * Returns success JSON format
	 *
	 * @return
	 *  {
            "status": "success"
        }
	 */
    public static Map<String, String> getSuccessResponseJson() {
        Map<String, String> responseBuilder = new HashMap<>();
        responseBuilder.put(Constant.STATUS,Constant.SUCCESS);
        return responseBuilder;
    }

	/**
	 * Returns success JSON format for given data
	 *
	 * @param data - value for 'data' key
	 * @return
	 * {
            "status": "success",
            "data": {
                "content": 101
            }
        }
	 */
    public static Map<String, Object> getSuccessResponseJson(Object data) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put(Constant.STATUS,Constant.SUCCESS);
        responseBuilder.put(Constant.DATA, data);
        return responseBuilder;
    }

    /**
     *
     * @param apiError
     *  {
            "status": "BAD_REQUEST",
            "timestamp": "20-09-2017 08:13:53 AM",
            "message": "Request JSON found Empty",
            "debugMessage": "Json was not found Null, it's Should not be Empty! "
        }
     * @return
     *  {
            "error": {
                "status": "BAD_REQUEST",
                "timestamp": "20-09-2017 08:13:53 AM",
                "message": "Request JSON found Empty",
                "debugMessage": "Json was not found Null, it's Should not be Empty! "
            },
            "status": "failure"
        }
     */
    public static Map<String, Object> getFailedResponseJson(Object apiError) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put(Constant.STATUS, Constant.FAILURE);
        responseBuilder.put(Constant.ERRORS, apiError);
        return responseBuilder;
    }
    /**
     * Returns success JSON with id in data
     *
     * @param id
     * @return
     * 		{
    "status": "success",
    "data": {
    "id": 101
    }
    }
     */
    public static Map getSuccessResponseJsonForId(Long id) {
        Map data = new HashMap();
        data.put(Constant.ID, id);
        return getSuccessResponseJson(data);
    }
    /**
     * Creates error JSON format
     *
     * @param errorCode
     * @param error
     * @return
     * {
    "status": "failure",
    "errors": {
    "INVALIDINPUTS": "Invalid values for products, email"
    }
    }
     */
    public static Map getFailedResponseJson(String errorCode, String error) {
        Map responseBuilder = new HashMap();
        responseBuilder.put(Constant.STATUS, Constant.FAILURE);
        Map errors = new HashMap();
        errors.put(errorCode, error);
        responseBuilder.put(Constant.ERRORS, errors);
        return responseBuilder;
    }

}
