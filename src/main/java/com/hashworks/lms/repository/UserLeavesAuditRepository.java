package com.hashworks.lms.repository;

import com.hashworks.lms.model.LeaveApplication;
import com.hashworks.lms.model.Role;
import com.hashworks.lms.model.UserLeavesAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserLeavesAuditRepository extends JpaRepository<UserLeavesAudit, Long>{

    @Query("select ula from UserLeavesAudit ula where ula.user.id = :id")
    List<UserLeavesAudit> findByUserId(@Param("id") long id);

    @Query("select ula from UserLeavesAudit ula where ula.user.id = :id and ula.policyMaster.id = :policyMasterId")
    List<UserLeavesAudit> findByUserIdAndPolicyMasterId(@Param("id") long id, @Param( "policyMasterId" ) long policyMasterId);


    @Query(value = "select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = :userId and policy_master_id = :policyMasterId and leave_type_id = :leaveTypeId ",nativeQuery = true)
    List<Object[]> getLeavesCreditedConsumedAvailable(@Param( "userId" ) long userId, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId);


//    @Query(value = "select sum(credit)-sum(debit) as leavesAvailableAtFYEnd from user_leaves_audit where policy_master_id = :policyMasterId and user_id = :userId and leave_type_id = :leaveTypeId",nativeQuery = true)
//    List<Object[]> getLeavesAvailableAtFyEnd(@Param( "userId" ) long userId, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId );


    @Query(value = "select sum(ula.credit)-sum(ula.debit) as leavesAvailableAtFYEnd from UserLeavesAudit ula where ula.policyMaster.id = :policyMasterId and ula.user.id = :userId and ula.leaveType.id = :leaveTypeId")
    Double getLeavesAvailableAtFyEnd(@Param( "userId" ) long userId, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId );

    @Query(value = "select sum(credit) as credited , sum(debit) as consumed , sum(credit-debit) as available from user_leaves_audit where user_id = :userId and policy_master_id = :policyMasterId and leave_type_id = :leaveTypeId and created_date between :startDate and :endDate",nativeQuery = true)
    List<Object[]> getLeavesCreditedConsumedAvailable2(@Param( "userId" ) Long userId,@Param( "policyMasterId" ) long policyMasterId ,  @Param( "leaveTypeId" ) long leaveTypeId, @Param( "startDate" ) Date startDate,@Param( "endDate" ) Date endDate);

    @Query("select sum(ula.debit) from UserLeavesAudit ula where user_id = :userId and policy_master_id = :policyMasterId and leave_type_id = :leaveTypeId and created_date between :startDate and :endDate ")
    int getSumOfWithDrawLeaves(@Param("userId")Long userId,@Param( "policyMasterId" ) long policyMasterId,  @Param( "leaveTypeId" ) long leaveTypeId, @Param( "startDate" ) Date startDate,@Param( "endDate" ) Date endDate);

    @Query(value = "select sum(credit) as credited  from user_leaves_audit where user_id = :userId and policy_master_id = :policyMasterId and leave_type_id = :leaveTypeId ",nativeQuery = true)
    Double[] getLeavesCreditedMoreThen30(@Param( "userId" ) long userId, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId);

}
