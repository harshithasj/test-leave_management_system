package com.hashworks.lms.repository;

import com.hashworks.lms.model.PolicyDetailMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface PolicyDetailMasterRepository extends JpaRepository<PolicyDetailMaster,Long> {
    List<PolicyDetailMaster> findByPolicyMasterId(Long id);

    List<PolicyDetailMaster> findAllByPolicyMasterId(Long id);

    List<PolicyDetailMaster> findAllByPolicyMasterIdAndCarryForwardAllowedTrue(Long id);

    @Transactional
    @Modifying
    @Query("delete from PolicyDetailMaster p where p.policyMaster.id =:policyId")
    void deleteByPolicyMasterId(@Param("policyId")Long policyId);
    // List <PolicyDetailMaster> findAll(Long policyId);
}
