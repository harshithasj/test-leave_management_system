package com.hashworks.lms.repository;

import com.hashworks.lms.model.HolidayListMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface HolidayListMasterRepository extends JpaRepository<HolidayListMaster,Long>{
    @Query( "select count(h) from HolidayListMaster h where h.date = :date ")
    int findByHoliday(@Param("date") Date date);

}
