package com.hashworks.lms.repository;

import com.hashworks.lms.model.Employee;
import com.hashworks.lms.model.EmployeeReportingManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeReportingManagerRepository extends JpaRepository<EmployeeReportingManager,Long> {

    EmployeeReportingManager findByEmployeeId(Long employeeId);

    List<EmployeeReportingManager> findByReportingManagerId(Long reportingManagerId);

    //  List<Employee> findByRManagerId(Long managerId);

//    @Query("select e.employee from EmployeeReportingManager e where e.manager.id = :managerId ")
//    List<Employee> findByManagerId(@Param("managerId") Long managerId);

}
