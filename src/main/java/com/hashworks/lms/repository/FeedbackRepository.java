package com.hashworks.lms.repository;

import com.hashworks.lms.model.EmployeeFeedback;
import com.hashworks.lms.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
}
