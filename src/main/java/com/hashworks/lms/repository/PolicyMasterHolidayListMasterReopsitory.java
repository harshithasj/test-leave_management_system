package com.hashworks.lms.repository;

import com.hashworks.lms.model.PolicyMasterHolidayListMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PolicyMasterHolidayListMasterReopsitory extends JpaRepository<PolicyMasterHolidayListMaster,Long> {
}
