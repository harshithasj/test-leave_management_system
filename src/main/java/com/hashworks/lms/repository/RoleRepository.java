package com.hashworks.lms.repository;

import com.hashworks.lms.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

    Role findByRoleType(String roleType);
    @Query("SELECT r FROM Role r where r.id=?1")
    Set<Role> findById(Long id);
}
