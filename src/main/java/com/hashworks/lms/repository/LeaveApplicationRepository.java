package com.hashworks.lms.repository;

import com.hashworks.lms.model.Employee;
import com.hashworks.lms.model.LeaveApplication;
import com.hashworks.lms.model.LeaveType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface LeaveApplicationRepository extends JpaRepository<LeaveApplication,Long> {

    List<LeaveApplication> findByUserId(long id);

    @Query("select l from LeaveApplication l where employee = :employees")
    List<LeaveApplication> findAllByEmployees(@Param("employees") List<Employee> employees);

    @Query("select l from LeaveApplication l where l.reportingManager.id = :id")
    List<LeaveApplication> findByReportingManagerId(@Param("id") long id);

    @Query("select l from LeaveApplication l where l.reportingManager.id = :managerId and l.leaveStartDate between :startDate and :endDate")
    List<LeaveApplication> findByReportingManagerIdByDate(@Param("managerId") Long managerId,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

    @Query("select l from LeaveApplication l where l.status = :status  and l.leaveEndDate = :currentDate")
    List<LeaveApplication> findByApprovedStatusAndCurrentDate(@Param( "status" ) String status,@Param( "currentDate")Date currentDate);

    @Query("select l from LeaveApplication l where l.status = :status  and l.leaveStartDate = :currentDate")
    List<LeaveApplication> findByApprovedStatusAndCurrentDate2(@Param( "status" ) String status,@Param( "currentDate")Date currentDate);

    @Query("select l from LeaveApplication l where l.status = :status and l.withdrawalStatus = :withdrawalStatus and :currentDate between l.leaveStartDate and l.leaveEndDate")
    List<LeaveApplication> findByConsumptionStartedStatusAndCurrentDate(@Param( "status" ) String status,@Param( "withdrawalStatus" ) String withdrawalStatus,@Param( "currentDate")Date currentDate);


//    @Query(value = "SELECT sum(la.appliedNoOfDays) as leaveCountBasedOnStatus FROM LeaveApplication la where la.user.id = :userId and la.status = :status and la.leaveType.id = :leaveTypeId and la.policyMaster.id = :policyMasterId ")
//    List<Object[]> getLeavesCountBasedOnStatus(@Param( "userId" ) long userId, @Param( "status" ) String status, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId );

    @Query(value = "SELECT sum(appliedNoOfDays) as leaveCountBasedOnStatus from LeaveApplication la where la.user.id = :userId and la.status = :status and la.leaveType.id = :leaveTypeId and la.policyMaster.id = :policyMasterId ")
    Double getLeavesCountBasedOnStatus(@Param( "userId" ) long userId, @Param( "status" ) String status, @Param( "policyMasterId" ) long policyMasterId, @Param( "leaveTypeId" ) long leaveTypeId );

    @Query("select l from LeaveApplication l where ((l.leaveEndDate between :startDate and :endDate) or (l.leaveStartDate between :startDate and :endDate)) and l.status in ('consumed fully','withdrawn') and l.leaveType.id = :leaveTypeId order by l.employee.name")
    List<LeaveApplication> findByDate(@Param("startDate") Date startDate,@Param("endDate") Date endDate,@Param("leaveTypeId") long leaveTypeId);


    @Query("select l from LeaveApplication l where l.reportingManager.id = :id and l.status = 'pending'")
    List<LeaveApplication> findByReportingManagerId1(@Param("id") long id);

    @Query("select l from LeaveApplication l where l.user.id = :id and l.leaveType.name = 'LOP' and l.leaveStartDate >= :startDate ")
    List<LeaveApplication> findByStatusAndEmployeeId(@Param("id") long id,@Param("startDate") Date startDate);

    @Modifying
    @Query(value ="update  leave_application  set reporting_manager_id = :newId where reporting_manager_id = :oldId",nativeQuery = true)
    void changeReportingManager(@Param("newId") long newId,@Param("oldId") long oldId);

    List<LeaveApplication> findByStatus(String pending);
}
