package com.hashworks.lms.repository;

import com.hashworks.lms.model.ReportingManager;
import com.hashworks.lms.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportingManagerRepository  extends JpaRepository<ReportingManager, Long> {

    ReportingManager findByEmployeeIdAndEnabledTrue(Long reportingManagerEmployeeId);

    @Query("select r from ReportingManager r where r.enabled = true and r.employee.enabled=true")
    List<ReportingManager> findAll();
}
