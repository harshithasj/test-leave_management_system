package com.hashworks.lms.repository;

import com.hashworks.lms.model.Employee;
import com.hashworks.lms.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u.employee from User u where u.id = :userId ")
    Employee findByUserId(@Param("userId") Long userId);

//    @Query("select u from User u where u.employee_id = :employee_id")
    User findByEmployeeId(Long employeeId);

    User findByEmployeeId(BigInteger employeeId);

    List<User> findByEnabledTrue();


    List<User> findByIdIn(List<Long> userIds);

    Optional<User> findById(Long userId);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    @Query(value = "select u.username FROM users u , user_role ur where u.id = ur.user_id and ur.role_id = (select role_id from role where role_type = 'Admin');",nativeQuery = true)
    Object[] findAdminEmailId();
}
