package com.hashworks.lms.repository;

import com.hashworks.lms.model.PolicyMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PolicyMasterRepository extends JpaRepository<PolicyMaster, Long> {
    PolicyMaster findByYear(String previousPolicyYear);

    @Query("select p from PolicyMaster p where enabled = true")
    PolicyMaster findEnabledTrue();
}
