package com.hashworks.lms.repository;

import com.hashworks.lms.model.Employee;
import com.hashworks.lms.model.EmployeeFeedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeFeedbackRepository  extends JpaRepository<EmployeeFeedback, Long> {
}
