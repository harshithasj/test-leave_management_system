package com.hashworks.lms.repository;

import com.hashworks.lms.dto.response.EmployeeDetailResponse;
import com.hashworks.lms.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {


    List<Employee> findAllByEnabled(boolean status);
//
//    @Query(value = "SELECT E.id AS employeeId, E.dob AS dob,\n" +
//            "\t(SELECT E.name FROM employee E where E.id = RM.employee_id) AS reportingManagersName\n" +
//            "    from employee E\n" +
//            "    LEFT JOIN users U ON E.id = U.employee_id \n" +
//            "    LEFT JOIN user_role UR ON U.id = UR.user_id\n" +
//            "    LEFT JOIN employee_reporting_manager ERM ON E.id = ERM.employee_id\n" +
//            "    LEFT JOIN reporting_manager RM ON ERM.reporting_manager_id = RM.id \n" +
//            "    WHERE E.enabled = true",nativeQuery = true)


    @Query(value = "SELECT E.id AS employeeId, E.dob AS dob, E.email_id AS emailId, E.mobile_number AS mobileNumber, E.name AS name, U.id AS userId, \n" +
            "    U.username AS username, U.password AS pasword, UR.role_id AS roleId,E.badge_number AS badgeNumber,\n" +
            "    (SELECT R.role_type FROM role R where R.role_id = roleId) as roleName,\n" +
            "    RM.id AS reportingManagerId,RM.employee_id AS reportingManagerEmployeeId,\n" +
            " \t(SELECT E.name FROM employee E where E.id = RM.employee_id) AS reportingManagerName\n" +
            "     from employee E\n" +
            "     LEFT JOIN users U ON E.id = U.employee_id \n" +
            "     LEFT JOIN user_role UR ON U.id = UR.user_id\n" +
            "     LEFT JOIN employee_reporting_manager ERM ON E.id = ERM.employee_id\n" +
            "     LEFT JOIN reporting_manager RM ON ERM.reporting_manager_id = RM.id \n" +
            "     WHERE E.enabled = true;\n" +
            "    ",nativeQuery = true)
    List<Object[]> findAllEmployeeDetails();
//    List<Map<Object,Object>> findAllEmployeeDetails();

    @Query(value="select e.id AS employeeId,\n" +
            " ulb.balance_leaves_available AS balanceLEaveAvailable,\n" +
            " ulb.balance_leaves_available_from_previous_year AS balanceLeaveAvailableFromPreviousYear,\n" +
            " l.name AS leaveType\n" +
            " from user_leave_balance ulb,users u,employee e,leave_type l\n" +
            " where ulb.user_id = u.id and u.employee_id = e.id and l.id=ulb.leave_type_id;",nativeQuery = true)
    List<Object[]> getAllEmployeeBalanceLeaveDetails();

//    @Query(value="select distinct e.name AS reportingManagerName ,e.id AS reportingManagerEmployeeId,er.employee_id AS employeeId\n" +
//            " from employee e, employee_reporting_manager er,reporting_manager r\n" +
//            " where e.id=r.employee_id and er.reporting_manager_id = r.id;",nativeQuery = true)
//    List<Object[]> getAllEmployeeReportingManagerDetails();

    @Query(value="SELECT E.id AS employeeId, E.dob AS dob, E.email_id AS emailId, E.mobile_number AS mobileNumber, E.name AS name, U.id AS userId, \n" +
            "    U.username AS username, U.password AS pasword, UR.role_id AS roleId,E.badge_number AS badgeNumber,\n" +
            "    (SELECT R.role_type FROM role R where R.role_id = roleId) as roleName,\n" +
            "    RM.id AS reportingManagerId,RM.employee_id AS reportingManagerEmployeeId,\n" +
            " \t(SELECT E.name FROM employee E where E.id = RM.employee_id) AS reportingManagerName\n" +
            "     from employee E\n" +
            "     LEFT JOIN users U ON E.id = U.employee_id \n" +
            "     LEFT JOIN user_role UR ON U.id = UR.user_id\n" +
            "     LEFT JOIN employee_reporting_manager ERM ON E.id = ERM.employee_id\n" +
            "     LEFT JOIN reporting_manager RM ON ERM.reporting_manager_id = RM.id \n" +
            "     WHERE E.enabled = true and E.id = :id",nativeQuery = true)
   List<Object[]> getEmployeeDetail(@Param("id") long id);

    @Query(value="select e.id AS employeeId,\n" +
            " ulb.balance_leaves_available AS balanceLEaveAvailable,\n" +
            " ulb.balance_leaves_available_from_previous_year AS balanceLeaveAvailableFromPreviousYear,\n" +
            " l.name AS leaveType\n" +
            " from user_leave_balance ulb,users u,employee e,leave_type l\n" +
            " where ulb.user_id = u.id and u.employee_id = e.id and l.id=ulb.leave_type_id and e.id = :id",nativeQuery = true)
    List<Object[]> getEmployeeBalanceLeaveDetails(@Param("id") long id);

    @Query(value="SELECT E.id AS employeeId, E.dob AS dob, E.email_id AS emailId, E.mobile_number AS mobileNumber, E.name AS name, U.id AS userId, \n" +
            "    U.username AS username, U.password AS pasword, UR.role_id AS roleId,E.badge_number AS badgeNumber,\n" +
            "    (SELECT R.role_type FROM role R where R.role_id = roleId) as roleName,\n" +
            "    RM.id AS reportingManagerId,RM.employee_id AS reportingManagerEmployeeId,\n" +
            " \t(SELECT E.name FROM employee E where E.id = RM.employee_id) AS reportingManagerName\n" +
            "     from employee E\n" +
            "     LEFT JOIN users U ON E.id = U.employee_id \n" +
            "     LEFT JOIN user_role UR ON U.id = UR.user_id\n" +
            "     LEFT JOIN employee_reporting_manager ERM ON E.id = ERM.employee_id\n" +
            "     LEFT JOIN reporting_manager RM ON ERM.reporting_manager_id = RM.id \n" +
            "     WHERE E.enabled = true and RM.id = :id",nativeQuery = true)
    List<Object[]> getEmployeeDetailByManager(@Param("id") long id);

    @Query(value="select e.id AS employeeId,\n" +
            "ulb.balance_leaves_available AS balanceLEaveAvailable,\n" +
            "ulb.balance_leaves_available_from_previous_year AS balanceLeaveAvailableFromPreviousYear,\n" +
            "l.name AS leaveType,reporting_manager_id AS reportingManagerId,rm.employee_id AS reportingManagerEmployeeId\n" +
            "from user_leave_balance ulb,users u,employee e,leave_type l,employee_reporting_manager erm,reporting_manager rm\n" +
            "where ulb.user_id = u.id and u.employee_id = e.id\n" +
            " and l.id=ulb.leave_type_id and\n" +
            " erm.employee_id =e.id  and erm.reporting_manager_id = rm.id and rm.id = :id",nativeQuery = true)
    List<Object[]> getEmployeeBalanceLeaveDetailsByManager(@Param("id") long id);

    Employee findByEmailId(String username);
    @Query(value = "SELECT max(id)+1 FROM leave_management.employee;",nativeQuery = true)
    Object[] getMaxId();
    @Query(value = "INSERT INTO `leave_management`.`user_role` (`user_id`, `role_id`) VALUES (?1, ?2);",nativeQuery = true)
    Object[] saveRole(Long userid,Long roleid);

    Employee findByBadgeNumber(String badgeNumber);

    Employee findByMobileNumber(String mobileNumber);
}
