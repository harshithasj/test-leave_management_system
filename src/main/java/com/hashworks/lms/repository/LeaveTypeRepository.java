package com.hashworks.lms.repository;

import com.hashworks.lms.model.LeaveType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface LeaveTypeRepository extends JpaRepository<LeaveType,Long> {

    @Transactional
    @Modifying
    @Query("delete from LeaveType l where l.id =:leaveTypeId")
    void deleteLeaveType(@Param("leaveTypeId")Long leaveTypeId);

   LeaveType findByName(String name);
@Query("SELECT l FROM LeaveType l where l.enabled=true")
    List<LeaveType> findAllByIsEnabled();
}
